namespace sound 
{
#define MAXCHANNELS 32
	IXAudio2* pXAudio2 = NULL;
	IXAudio2MasteringVoice* pMasterVoice = NULL;
	IXAudio2SourceVoice* pSourceVoice;
	XAUDIO2_BUFFER buffer;
	BYTE* channel[MAXCHANNELS];
	BYTE* fxBus[MAXCHANNELS];
	int len = 44100 * 60 * 10;
	int channelLen = 44100 ;
	int fxBusLen = 44100;
#ifdef EditMode
	bool channelVisible[MAXCHANNELS];
#endif

	void Init()
	{

		// ������� ������ IXAudio2
		HRESULT hr = XAudio2Create(&pXAudio2);

		// ������� ������������� �����
		hr = pXAudio2->CreateMasteringVoice(&pMasterVoice);

		for (int x = 0; x < MAXCHANNELS-1; x++)
		{
			channel[x] = new BYTE[channelLen * 2*2];
			ZeroMemory(channel[x], channelLen * 2*2);
			fxBus[x] = new BYTE[fxBusLen * 2 * 2];
			ZeroMemory(channel[x], fxBusLen * 2 * 2);

		}

		channel[MAXCHANNELS-1] = new BYTE[len * 2 * 2];
		ZeroMemory(channel[MAXCHANNELS - 1], len * 2 * 2);

		fxBus[MAXCHANNELS - 1] = new BYTE[len * 2 * 2];
		ZeroMemory(fxBus[MAXCHANNELS - 1], len * 2 * 2);


			// ������� ����� ���������
			WAVEFORMATEX waveformat;
			waveformat.wFormatTag = WAVE_FORMAT_PCM;
			waveformat.nChannels = 2;
			waveformat.nSamplesPerSec = 44100;
			waveformat.nAvgBytesPerSec = 44100 * 2*2;
			waveformat.nBlockAlign = 2*16/8;
			waveformat.wBitsPerSample = 16;
			waveformat.cbSize = 0;

			hr = pXAudio2->CreateSourceVoice(&pSourceVoice, &waveformat);

			ZeroMemory(&buffer, sizeof(buffer));
			buffer.AudioBytes = 2*2 * len;
			buffer.pAudioData = channel[MAXCHANNELS -1];
			buffer.Flags = XAUDIO2_END_OF_STREAM;
			buffer.PlayBegin = 0;
			buffer.PlayLength = 0;

			// �������� �����
			hr = pSourceVoice->SubmitSourceBuffer(&buffer);

			pSourceVoice->Stop(0, 0);
		
	}

	void Release()
	{
		pXAudio2->StopEngine();
		pXAudio2->Release();
	}







}
