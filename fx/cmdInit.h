
#define used_Project
#define used_EndDraw
#define used_MainLoop
#define used_Scene
#define used_EndScene
#define used_ShowScene
#define used_Track
#define used_Channel
#define used_Clip
#define used_Note
#define used_Envelope
#define used_Point
#define used_Oscillator
#define used_EndTrack
#define used_ParticleContainer
#define used_pEvo
#define used_pScroll
#define used_Mixer
#define used_Mute
#define used_Solo
#define used_CreateShader
#define used_CreateVB
#define used_CreateTexture
#define used_SetRT
#define used_ResetRT
#define used_Emitter
#define used_TimeLoop
#define used_Dynamics
#define used_Echo
#define used_Equalizer


#define used_Color
#define used_Size
#define used_Quad
#define used_Acceleration
#define used_Spawn
#define used_Velocity
#define used_Rotation
#define used_ColorPoint
#define used_fxPoint
#define used_ControlCurve
#define used_BlendMode
#define used_Orientation
#define used_SetTexture
#define used_SetZBuffer

#define CAT_SCENE 1
#define CAT_CMD 2
#define CAT_COPY 3
#define CAT_3D 4
#define CAT_PARTICLE 5
#define CAT_SOUND 6

using namespace Commands;

#ifdef EditMode

#define EditorInfoOffset 8
#define EditorInfoOffsetX 0
#define EditorInfoOffsety 2
#define EditorInfoOffsetSelected 4
#define EditorInfoOffsetMode 5

struct uiColor 
{
	XMVECTORF32 background = XMVECTORF32{ 0.25,0.25,0.25,1.0 };
	XMFLOAT4 boxcat[7] = 
	{
		XMFLOAT4 (0,0,0,1),//
		XMFLOAT4(1.0,0.0,0.0,1.0),//scene
		XMFLOAT4(1.0,1.0,0.0,1.0),//cmd
		XMFLOAT4(0.0,0.0,1.0,1.0),//copy
		XMFLOAT4(0.5,0.5,1.0,1.0),//3d
		XMFLOAT4(0.25f,0.25f,0.95f,1.0f),//particle
		XMFLOAT4(0.0f,0.5f,0.0f,1.0f),//sound
	};
	XMFLOAT4 font = XMFLOAT4 (0,0,0,1);
	XMFLOAT4 selectedfont = XMFLOAT4 (1,0,0,1);
	XMFLOAT4 selectedbox = XMFLOAT4 (1,1,1,1);
	XMFLOAT4 clipcopy = XMFLOAT4(0.0, 0.25f, 0.0, 1.0);
	XMFLOAT4 notestrong = XMFLOAT4(0, 0.75f, 0, 1);
	XMFLOAT4 notevolume = XMFLOAT4 (0,0.75f,0,.5f);
	XMFLOAT4 notepan = XMFLOAT4 (1,1,1,.5f);
	XMFLOAT4 peakmeter = XMFLOAT4 (1,1,1,.5f);
	XMFLOAT4 activechannel = XMFLOAT4(0.0, 0.5f, 0.0, 1.0);
	XMFLOAT4 mutedchannel = XMFLOAT4(0.0, 0.25f, 0.0, 1.0);
	XMFLOAT4 envelopeline = XMFLOAT4(0, 0, 0, 1);
	XMFLOAT4 point = XMFLOAT4(0, 0, 0, 1);
	XMFLOAT4 envelopeboundsline = XMFLOAT4(0, 0, 0, 1);
	XMFLOAT4 masterwave = XMFLOAT4(0.5f, 0.5f, 0.5f, .5f);
	XMFLOAT4 masterwaveline = XMFLOAT4(1.0f,1.0f,1.0f, .75f);
	XMFLOAT4 colorpoint = XMFLOAT4(1, 1, 0, 1);
	XMFLOAT4 fxpoint = XMFLOAT4(0, 0, 0, 1);
	XMFLOAT4 cursor = XMFLOAT4(0, 0, 0, .5f);
	XMFLOAT4 loopoff = XMFLOAT4(.5f, .5f, .5f, 1);
	XMFLOAT4 loopon = XMFLOAT4(1.0f, .5f, .5f, 1);
	XMFLOAT4 loadback = XMFLOAT4(.2f, .2f, .2f, .5f);
	XMFLOAT4 timelinelines = XMFLOAT4(0, 0, 0, 1.f);
} colorScheme;

#endif

void CalcOfsAndSize(int n)
{
	int ofs = 1; int c = 0;
	while ((BYTE*)*CmdDesc[n].ParamName[c] != 0)
	{
		CmdDesc[n].ParamOffset[c] = ofs;
		ofs += TypeSizeTable[CmdDesc[n].ParamType[c]];
		c++;
	}

CmdDesc[n].Size = ofs;
}
void cmdInit()
{
	ZeroMemory(CmdDesc, sizeof(CmdDesc));

	int n = 0;
	CmdDesc[n].Size = 0;
	CmdDesc[n].Routine = Release;
	n++;

#ifdef used_Project
	CmdDesc[n].Routine = Project;
	CmdDesc[n].Level = -1;
	CmdDesc[n].Size = 32;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Project");
		strcpy_s(CmdDesc[n].ParamName[0], "name");
		CmdDesc[n].Category = CAT_SCENE;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif
		CmdDesc[n].ParamType[0] = PT_TEXT;
		CmdDesc[n].ParamOffset[1] = 31;
#endif
	n++;

#ifdef used_EndDraw
	CmdDesc[n].Routine = EndDraw;
	CmdDesc[n].Level = -1;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "EndDraw");
		CmdDesc[n].Category = CAT_SCENE;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif
#endif
	n++;


#ifdef used_MainLoop
	CmdDesc[n].Routine = MainLoop;
	CmdDesc[n].Level = -1;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Mainloop");
		CmdDesc[n].Category = CAT_SCENE;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif
#endif
	n++;


#ifdef used_Scene
	CmdDesc[n].Routine = Scene;
	CmdDesc[n].Level = -1;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Scene");
		CmdDesc[n].Category = CAT_SCENE;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
		strcpy_s(CmdDesc[n].ParamName[0], "name");
	#endif
	CmdDesc[n].ParamType[0] = PT_LABEL;
#endif
	CalcOfsAndSize(n);
	n++;




#ifdef used_EndScene
	CmdDesc[n].Routine = EndScene;
	CmdDesc[n].Level = -1;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "EndScene");
		CmdDesc[n].Category = CAT_SCENE;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif
#endif
	n++;

#ifdef used_ShowScene
	CmdDesc[n].Routine = ShowScene;
	CmdDesc[n].Level = 0;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "ShowScene");
		CmdDesc[n].Category = CAT_SCENE;
		//CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
		CmdDesc[n].Color = XMFLOAT4(.5,.5,.5,.8);
		strcpy_s(CmdDesc[n].ParamName[0], "scene");
		strcpy_s(CmdDesc[n].ParamName[1], "timeline");
			strcpy_s(CmdDesc[n].ParamEnum[1][0], "off");
			strcpy_s(CmdDesc[n].ParamEnum[1][1], "on");
		strcpy_s(CmdDesc[n].ParamName[2], "x"); 
		strcpy_s(CmdDesc[n].ParamName[3], "y");
		strcpy_s(CmdDesc[n].ParamName[4], "length");
	#endif
	 CmdDesc[n].ParamType[0] = PT_BYTE;
	 CmdDesc[n].ParamType[1] = PT_ENUM;
	 CmdDesc[n].ParamType[2] = PT_SWORD;
	 CmdDesc[n].ParamType[3] = PT_SWORD;
	 CmdDesc[n].ParamType[4] = PT_SWORD;
	 CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_Track
	CmdDesc[n].Routine = Track;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Track");
		CmdDesc[n].Category = CAT_SOUND;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif

#endif
	n++;

#ifdef used_Channel
	CmdDesc[n].Routine = Channel;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Channel");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "max polyphony"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "name"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "volume"); CmdDesc[n].ParamEnvControlled[2] = true;
	strcpy_s(CmdDesc[n].ParamName[3], "pan"); CmdDesc[n].ParamEnvControlled[3] = true;
	strcpy_s(CmdDesc[n].ParamName[4], "mute"); CmdDesc[n].ParamEnvControlled[4] = false;
		strcpy_s(CmdDesc[n].ParamEnum[4][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[4][1], "on");
	strcpy_s(CmdDesc[n].ParamName[5], "solo"); CmdDesc[n].ParamEnvControlled[5] = false;
		strcpy_s(CmdDesc[n].ParamEnum[5][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[5][1], "on");

#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	CmdDesc[n].ParamType[1] = PT_LABEL;
	CmdDesc[n].ParamType[2] = PT_BYTE;
	CmdDesc[n].ParamType[3] = PT_SBYTE;
	CmdDesc[n].ParamType[4] = PT_ENUM;
	CmdDesc[n].ParamType[5] = PT_ENUM;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_Clip
	CmdDesc[n].Routine = Clip;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = 13;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Clip");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "x"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "y"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "length"); CmdDesc[n].ParamEnvControlled[2] = false;
	strcpy_s(CmdDesc[n].ParamName[3], "repeat"); CmdDesc[n].ParamEnvControlled[3] = false;
	strcpy_s(CmdDesc[n].ParamName[4], "volume"); CmdDesc[n].ParamEnvControlled[4] = false;
	strcpy_s(CmdDesc[n].ParamName[5], "grid"); CmdDesc[n].ParamEnvControlled[5] = false;
	strcpy_s(CmdDesc[n].ParamName[6], "npm"); CmdDesc[n].ParamEnvControlled[6] = false;
#endif

								   CmdDesc[n].ParamType[0] = PT_SWORD; 
	CmdDesc[n].ParamOffset[1] = 3; CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamOffset[2] = 5; CmdDesc[n].ParamType[2] = PT_SWORD; 
	CmdDesc[n].ParamOffset[3] = 7; CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamOffset[4] = 9; CmdDesc[n].ParamType[4] = PT_BYTE;
	CmdDesc[n].ParamOffset[5] = 10; CmdDesc[n].ParamType[5] = PT_BYTE;
	CmdDesc[n].ParamOffset[6] = 11; CmdDesc[n].ParamType[6] = PT_SWORD;

#endif
	n++;

#ifdef used_Note
	CmdDesc[n].Routine = Note;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = 5;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Note");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "Note"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "Velocity"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "Slide"); CmdDesc[n].ParamEnvControlled[2] = false;
	strcpy_s(CmdDesc[n].ParamName[3], "Pan"); CmdDesc[n].ParamEnvControlled[3] = false;
#endif
								   CmdDesc[n].ParamType[0] = PT_BYTE;
	CmdDesc[n].ParamOffset[1] = 2; CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamOffset[2] = 3; CmdDesc[n].ParamType[2] = PT_BYTE;
	CmdDesc[n].ParamOffset[3] = 4; CmdDesc[n].ParamType[3] = PT_SBYTE;

#endif
	n++;

#ifdef used_Envelope
	CmdDesc[n].Routine = Envelope;
	CmdDesc[n].Level = 0;
//	CmdDesc[n].Size = 14;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Envelope");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "x"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "y"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "retrigger"); CmdDesc[n].ParamEnvControlled[2] = false;
		strcpy_s(CmdDesc[n].ParamEnum[2][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[2][1], "on");
	strcpy_s(CmdDesc[n].ParamName[3], " "); CmdDesc[n].ParamEnvControlled[3] = false;
	strcpy_s(CmdDesc[n].ParamName[4], "assign to"); CmdDesc[n].ParamEnvControlled[4] = false;
	strcpy_s(CmdDesc[n].ParamName[5], "assign op"); CmdDesc[n].ParamEnvControlled[5] = false;
		strcpy_s(CmdDesc[n].ParamEnum[5][0], "+");
		strcpy_s(CmdDesc[n].ParamEnum[5][1], "-");
		strcpy_s(CmdDesc[n].ParamEnum[5][2], "=");
		strcpy_s(CmdDesc[n].ParamEnum[5][3], "*");
		strcpy_s(CmdDesc[n].ParamEnum[5][4], "/");
	strcpy_s(CmdDesc[n].ParamName[6], "min"); CmdDesc[n].ParamEnvControlled[6] = false;
	strcpy_s(CmdDesc[n].ParamName[7], "max"); CmdDesc[n].ParamEnvControlled[7] = false;
	strcpy_s(CmdDesc[n].ParamName[8], "link"); CmdDesc[n].ParamEnvControlled[8] = false;
		strcpy_s(CmdDesc[n].ParamEnum[8][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[8][1], "on");

#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_ENUM;
	CmdDesc[n].ParamType[3] = PT_ENUM;
	CmdDesc[n].ParamType[4] = PT_ENUMASSIGN;
	CmdDesc[n].ParamType[5] = PT_ENUM;
	CmdDesc[n].ParamType[6] = PT_SWORD;
	CmdDesc[n].ParamType[7] = PT_SWORD;
	CmdDesc[n].ParamType[8] = PT_ENUM;


	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_Point
	CmdDesc[n].Routine = Point;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = 10;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Point");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.point;
	strcpy_s(CmdDesc[n].ParamName[0], "x"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "y"); CmdDesc[n].ParamEnvControlled[1] = false;
#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
    CmdDesc[n].ParamType[1] = PT_SWORD;
	CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_Oscillator
	CmdDesc[n].Routine = Oscillator;
	CmdDesc[n].Level = 0;
	
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Oscillator");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "freq shift"); CmdDesc[n].ParamEnvControlled[0] = true;
	strcpy_s(CmdDesc[n].ParamName[1], "volume"); CmdDesc[n].ParamEnvControlled[1] = true;
	strcpy_s(CmdDesc[n].ParamName[2], "wave type"); CmdDesc[n].ParamEnvControlled[2] = false;
		strcpy_s(CmdDesc[n].ParamEnum[2][0], "sin");
		strcpy_s(CmdDesc[n].ParamEnum[2][1], "triangle");
		strcpy_s(CmdDesc[n].ParamEnum[2][2], "saw");
		strcpy_s(CmdDesc[n].ParamEnum[2][3], "square");
		strcpy_s(CmdDesc[n].ParamEnum[2][4], "noise");
	strcpy_s(CmdDesc[n].ParamName[3], "harmonics count"); CmdDesc[n].ParamEnvControlled[3] = false;
	strcpy_s(CmdDesc[n].ParamName[4], "harmonics detune"); CmdDesc[n].ParamEnvControlled[4] = true;
	strcpy_s(CmdDesc[n].ParamName[5], "harmonics amp"); CmdDesc[n].ParamEnvControlled[5] = true;

#endif
    CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamType[2] = PT_ENUM;
	CmdDesc[n].ParamType[3] = PT_BYTE;
	CmdDesc[n].ParamType[4] = PT_BYTE;
	CmdDesc[n].ParamType[5] = PT_BYTE;


	CalcOfsAndSize(n);

#endif
	n++;


#ifdef used_EndTrack
	CmdDesc[n].Routine = EndTrack;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "EndTrack");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif

#endif
	n++;


#ifdef used_Mixer
	CmdDesc[n].Routine = Mixer;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Mixer");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];

#endif
	CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_Mute
	CmdDesc[n].Routine = Mute;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Mute");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_Solo
	CmdDesc[n].Routine = Solo;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Solo");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];

#endif
	CmdDesc[n].ParamType[0] = PT_ENUM;
	CalcOfsAndSize(n);

#endif
	n++;


#ifdef used_ParticleContainer
	CmdDesc[n].Routine = ParticleContainer;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = 100;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "ParticleContainer");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "shader");
	strcpy_s(CmdDesc[n].ParamName[1], "y");
	strcpy_s(CmdDesc[n].ParamName[2], "z");
	strcpy_s(CmdDesc[n].ParamName[3], "x size");
	strcpy_s(CmdDesc[n].ParamName[4], "mode");


#endif
								   CmdDesc[n].ParamType[0] = PT_INT;
	CmdDesc[n].ParamOffset[1] = 5; CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamOffset[2] = 7; CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamOffset[3] = 9; CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamOffset[4] = 11; CmdDesc[n].ParamType[4] = PT_SWORD;

#endif
	n++;


#ifdef used_CreateShader
	CmdDesc[n].Routine = CreateShader;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateShader");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "name");
	strcpy_s(CmdDesc[n].ParamName[1], "target slot");//todo:: unused, remove
	strcpy_s(CmdDesc[n].ParamName[2], "data");
#endif
	CmdDesc[n].ParamType[0] = PT_LABEL;
	CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamType[2] = PT_TEXT;
	
	CalcOfsAndSize(n);
#endif
	n++;


#ifdef used_CreateVB
	CmdDesc[n].Routine = CreateVB;
	CmdDesc[n].Level = 0;
	
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateVB");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "name");
	strcpy_s(CmdDesc[n].ParamName[1], "target slot");
	strcpy_s(CmdDesc[n].ParamName[2], "type");
		strcpy_s(CmdDesc[n].ParamEnum[2][0], "type 1");
		strcpy_s(CmdDesc[n].ParamEnum[2][1], "type 2");
	strcpy_s(CmdDesc[n].ParamName[3], "size");

#endif
	CmdDesc[n].ParamType[0] = PT_LABEL;
	CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamType[2] = PT_ENUM;
	CmdDesc[n].ParamType[3] = PT_INT;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_TimeLoop
	CmdDesc[n].Routine = TimeLoop;
	CmdDesc[n].Level = 0;

#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "TimeLoop");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "active"); CmdDesc[n].ParamEnvControlled[0] = false;
		strcpy_s(CmdDesc[n].ParamEnum[0][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[0][1], "on");
	strcpy_s(CmdDesc[n].ParamName[1], "x"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "length"); CmdDesc[n].ParamEnvControlled[2] = false;

#endif
	CmdDesc[n].ParamType[0] = PT_ENUM;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_SWORD;

	CalcOfsAndSize(n);

#endif
	n++;



#ifdef used_Emitter
	CmdDesc[n].Routine = Emitter;
	CmdDesc[n].Level = 0;

#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Emitter");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "name");
	strcpy_s(CmdDesc[n].ParamName[1], "type");
		strcpy_s(CmdDesc[n].ParamEnum[1][0], "box");
		strcpy_s(CmdDesc[n].ParamEnum[1][1], "sphere");
	strcpy_s(CmdDesc[n].ParamName[2], "x");
	strcpy_s(CmdDesc[n].ParamName[3], "y");
	strcpy_s(CmdDesc[n].ParamName[4], "z");
	strcpy_s(CmdDesc[n].ParamName[5], "size x");
	strcpy_s(CmdDesc[n].ParamName[6], "size y");
	strcpy_s(CmdDesc[n].ParamName[7], "size z");
	strcpy_s(CmdDesc[n].ParamName[8], "motion blur");
	strcpy_s(CmdDesc[n].ParamName[9], "mute");
		strcpy_s(CmdDesc[n].ParamEnum[9][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[9][1], "on");
	strcpy_s(CmdDesc[n].ParamName[10], "solo");
		strcpy_s(CmdDesc[n].ParamEnum[10][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[10][1], "on");


#endif
	CmdDesc[n].ParamType[0] = PT_LABEL;
	CmdDesc[n].ParamType[1] = PT_ENUM;
	CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamType[4] = PT_SWORD;
	CmdDesc[n].ParamType[5] = PT_SWORD;
	CmdDesc[n].ParamType[6] = PT_SWORD;
	CmdDesc[n].ParamType[7] = PT_SWORD;
	CmdDesc[n].ParamType[8] = PT_BYTE;
	CmdDesc[n].ParamType[9] = PT_ENUM;
	CmdDesc[n].ParamType[10] = PT_ENUM;

	CalcOfsAndSize(n);

#endif
	n++;


#ifdef used_Dynamics
	CmdDesc[n].Routine = Dynamics;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Dynamics");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "preamp"); CmdDesc[n].ParamEnvControlled[0] = true;
	strcpy_s(CmdDesc[n].ParamName[1], "offset"); CmdDesc[n].ParamEnvControlled[1] = true;
	strcpy_s(CmdDesc[n].ParamName[2], "scale"); CmdDesc[n].ParamEnvControlled[2] = true;
	strcpy_s(CmdDesc[n].ParamName[3], "out volume"); CmdDesc[n].ParamEnvControlled[3] = true;
	strcpy_s(CmdDesc[n].ParamName[4], "mix"); CmdDesc[n].ParamEnvControlled[4] = true;

#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamType[3] = PT_BYTE;
	CmdDesc[n].ParamType[4] = PT_BYTE;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_Echo
	CmdDesc[n].Routine = Echo;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Echo");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "target channel"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "leftAmp"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "rightAmp"); CmdDesc[n].ParamEnvControlled[2] = false;
	strcpy_s(CmdDesc[n].ParamName[3], "left offset"); CmdDesc[n].ParamEnvControlled[3] = false;
	strcpy_s(CmdDesc[n].ParamName[4], "right offset"); CmdDesc[n].ParamEnvControlled[4] = false;
	strcpy_s(CmdDesc[n].ParamName[5], "rand amp"); CmdDesc[n].ParamEnvControlled[4] = false;
	strcpy_s(CmdDesc[n].ParamName[6], "rand speed"); CmdDesc[n].ParamEnvControlled[4] = false;

#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamType[2] = PT_BYTE;
	CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamType[4] = PT_SWORD;
	CmdDesc[n].ParamType[5] = PT_BYTE;
	CmdDesc[n].ParamType[6] = PT_SWORD;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_Equalizer
	CmdDesc[n].Routine = Equalizer;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Equalizer");
	CmdDesc[n].Category = CAT_SOUND;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "type"); CmdDesc[n].ParamEnvControlled[0] = false;
		strcpy_s(CmdDesc[n].ParamEnum[0][0], "lowpass");
		strcpy_s(CmdDesc[n].ParamEnum[0][1], "highpass");
		strcpy_s(CmdDesc[n].ParamEnum[0][2], "bandpass");
		strcpy_s(CmdDesc[n].ParamEnum[0][3], "notch");
		strcpy_s(CmdDesc[n].ParamEnum[0][4], "eq");
	strcpy_s(CmdDesc[n].ParamName[1], "freq"); CmdDesc[n].ParamEnvControlled[1] = true;
	strcpy_s(CmdDesc[n].ParamName[2], "gain"); CmdDesc[n].ParamEnvControlled[2] = true;
	strcpy_s(CmdDesc[n].ParamName[3], "bandwidth"); CmdDesc[n].ParamEnvControlled[3] = true;

#endif
	CmdDesc[n].ParamType[0] = PT_ENUM;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamType[3] = PT_SWORD;
	CalcOfsAndSize(n);
	CmdDesc[n].Size += 3*2*2;//buffer

#endif
	n++;


#ifdef used_Spawn
	CmdDesc[n].Routine = Spawn;
	CmdDesc[n].Level = 0;

#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Spawn");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "start");
	strcpy_s(CmdDesc[n].ParamName[1], "end");
	strcpy_s(CmdDesc[n].ParamName[2], "p.per sec");
	strcpy_s(CmdDesc[n].ParamName[3], "lifetime");
	strcpy_s(CmdDesc[n].ParamName[4], "mode");
		strcpy_s(CmdDesc[n].ParamEnum[4][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[4][1], "on");
	strcpy_s(CmdDesc[n].ParamName[5], "blp");

#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_INT;
	CmdDesc[n].ParamType[3] = PT_INT;
	CmdDesc[n].ParamType[4] = PT_ENUM;
	CmdDesc[n].ParamType[5] = PT_SWORD;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_Velocity
	CmdDesc[n].Routine = Velocity;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Velocity");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "x");
	strcpy_s(CmdDesc[n].ParamName[1], "y");
	strcpy_s(CmdDesc[n].ParamName[2], "z");
	strcpy_s(CmdDesc[n].ParamName[3], "range x");
	strcpy_s(CmdDesc[n].ParamName[4], "range y");
	strcpy_s(CmdDesc[n].ParamName[5], "range z");
	strcpy_s(CmdDesc[n].ParamName[6], "deaccel");
#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamType[4] = PT_SWORD;
	CmdDesc[n].ParamType[5] = PT_SWORD;
	CmdDesc[n].ParamType[6] = PT_SWORD;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_Acceleration
	CmdDesc[n].Routine = Acceleration;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Acceleration");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "x");
	strcpy_s(CmdDesc[n].ParamName[1], "y");
	strcpy_s(CmdDesc[n].ParamName[2], "z");
	strcpy_s(CmdDesc[n].ParamName[3], "range x");
	strcpy_s(CmdDesc[n].ParamName[4], "range y");
	strcpy_s(CmdDesc[n].ParamName[5], "range z");
#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamType[4] = PT_SWORD;
	CmdDesc[n].ParamType[5] = PT_SWORD;

	CalcOfsAndSize(n);

#endif
	n++;

	
#ifdef used_Quad
	CmdDesc[n].Routine = Quad;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Quad");
	CmdDesc[n].Category = CAT_3D;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "shader");
	for (int t = 1; t < 32; t++)
	{
		strcpy_s(CmdDesc[n].ParamName[t], "p");
	}
#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	for (int t = 1; t < 32; t++)
	{
		CmdDesc[n].ParamType[t] = PT_SWORD;
	}

	CalcOfsAndSize(n);

#endif
	n++;
	//------------------------

#ifdef used_Size
	CmdDesc[n].Routine = Size;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Size");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "size");
	strcpy_s(CmdDesc[n].ParamName[1], "size range");

#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_SWORD;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_Color
	CmdDesc[n].Routine = Color;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Color");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "r");
	strcpy_s(CmdDesc[n].ParamName[1], "g");
	strcpy_s(CmdDesc[n].ParamName[2], "b");
	strcpy_s(CmdDesc[n].ParamName[3], "a");
	strcpy_s(CmdDesc[n].ParamName[4], "brightness");
	strcpy_s(CmdDesc[n].ParamName[5], "contrast");
#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamType[2] = PT_BYTE;
	CmdDesc[n].ParamType[3] = PT_BYTE;
	CmdDesc[n].ParamType[4] = PT_BYTE;
	CmdDesc[n].ParamType[5] = PT_BYTE;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_BlendMode
	CmdDesc[n].Routine = BlendMode;
	CmdDesc[n].Level = 0;

#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "BlendMode");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "alpha");
		strcpy_s(CmdDesc[n].ParamEnum[0][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[0][1], "on");
		strcpy_s(CmdDesc[n].ParamEnum[0][2], "pm1");
		strcpy_s(CmdDesc[n].ParamEnum[0][3], "pm2");
	strcpy_s(CmdDesc[n].ParamName[1], "operation");
		strcpy_s(CmdDesc[n].ParamEnum[1][0], "add");
		strcpy_s(CmdDesc[n].ParamEnum[1][1], "reverse sub");
		strcpy_s(CmdDesc[n].ParamEnum[1][2], "sub");
		strcpy_s(CmdDesc[n].ParamEnum[1][3], "min");
		strcpy_s(CmdDesc[n].ParamEnum[1][4], "max");
#endif
		CmdDesc[n].ParamType[0] = PT_ENUM;
		CmdDesc[n].ParamType[1] = PT_ENUM;

		CalcOfsAndSize(n);
#endif
	n++;


#ifdef used_Orientation
	CmdDesc[n].Routine = Orientation;
	CmdDesc[n].Level = 0;

#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Orientation");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "mode");
	strcpy_s(CmdDesc[n].ParamEnum[0][0], "screen");
	strcpy_s(CmdDesc[n].ParamEnum[0][1], "p.motion");
	strcpy_s(CmdDesc[n].ParamEnum[0][2], "source");

#endif
	CmdDesc[n].ParamType[0] = PT_ENUM;

	CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_Rotation
	CmdDesc[n].Routine = Rotation;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Rotation");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "speed");
	strcpy_s(CmdDesc[n].ParamName[1], "phase");
	strcpy_s(CmdDesc[n].ParamName[2], "speed range");
	strcpy_s(CmdDesc[n].ParamName[3], "phase range");
	strcpy_s(CmdDesc[n].ParamName[4], "offset x");
	strcpy_s(CmdDesc[n].ParamName[5], "offset y");


#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamType[4] = PT_SWORD;
	CmdDesc[n].ParamType[5] = PT_SWORD;

	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_ControlCurve
	CmdDesc[n].Routine = ControlCurve;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "ControlCurve");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];

#endif
	CalcOfsAndSize(n);

#endif
	n++;

#ifdef used_ColorPoint
	CmdDesc[n].Routine = ColorPoint;
	CmdDesc[n].Level = 2;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "ColorPoint");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.colorpoint;
	strcpy_s(CmdDesc[n].ParamName[0], "x"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "r"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "g"); CmdDesc[n].ParamEnvControlled[2] = false;
	strcpy_s(CmdDesc[n].ParamName[3], "b"); CmdDesc[n].ParamEnvControlled[3] = false;
	strcpy_s(CmdDesc[n].ParamName[4], "a"); CmdDesc[n].ParamEnvControlled[4] = false;
#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamType[2] = PT_BYTE;
	CmdDesc[n].ParamType[3] = PT_BYTE;
	CmdDesc[n].ParamType[4] = PT_BYTE;

	CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_fxPoint
	CmdDesc[n].Routine = fxPoint;
	CmdDesc[n].Level = 2;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "fxPoint");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.fxpoint;
	strcpy_s(CmdDesc[n].ParamName[0], "x"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "y"); CmdDesc[n].ParamEnvControlled[1] = false;
#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	CmdDesc[n].ParamType[1] = PT_BYTE;

	CalcOfsAndSize(n);
#endif
	n++;


#ifdef used_CreateTexture
	CmdDesc[n].Routine = CreateTexture;
	CmdDesc[n].Level = 2;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateTexture");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "name"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "size"); CmdDesc[n].ParamEnvControlled[1] = false;
		strcpy_s(CmdDesc[n].ParamEnum[1][0], "32");
		strcpy_s(CmdDesc[n].ParamEnum[1][1], "64");
		strcpy_s(CmdDesc[n].ParamEnum[1][2], "128");
		strcpy_s(CmdDesc[n].ParamEnum[1][3], "256");
		strcpy_s(CmdDesc[n].ParamEnum[1][4], "512");
		strcpy_s(CmdDesc[n].ParamEnum[1][5], "1024");
		strcpy_s(CmdDesc[n].ParamEnum[1][6], "2048");
	strcpy_s(CmdDesc[n].ParamName[2], "format"); CmdDesc[n].ParamEnvControlled[2] = false;
		strcpy_s(CmdDesc[n].ParamEnum[2][0], "rgba8");
		strcpy_s(CmdDesc[n].ParamEnum[2][1], "rgba16");
		strcpy_s(CmdDesc[n].ParamEnum[2][2], "rgba32");
#endif
	CmdDesc[n].ParamType[0] = PT_LABEL;
	CmdDesc[n].ParamType[1] = PT_ENUM;
	CmdDesc[n].ParamType[2] = PT_ENUM;

	CalcOfsAndSize(n);
#endif
	n++;


#ifdef used_SetRT
	CmdDesc[n].Routine = SetRT;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetRT");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "texture"); CmdDesc[n].ParamEnvControlled[0] = false;
#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_ResetRT
	CmdDesc[n].Routine = ResetRT;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "ResetRT");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
	CalcOfsAndSize(n);
#endif
	n++;


#ifdef used_pEvo
	CmdDesc[n].Routine = pEvo;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "pEvo");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "startSpd"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "endSpd"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "startSpdA"); CmdDesc[n].ParamEnvControlled[2] = false;
	strcpy_s(CmdDesc[n].ParamName[3], "endSpdA"); CmdDesc[n].ParamEnvControlled[3] = false;
	strcpy_s(CmdDesc[n].ParamName[4], "fold"); CmdDesc[n].ParamEnvControlled[4] = false;
	strcpy_s(CmdDesc[n].ParamName[5], "foldA"); CmdDesc[n].ParamEnvControlled[5] = false;
#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamType[4] = PT_SWORD;
	CmdDesc[n].ParamType[5] = PT_SWORD;
	CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_pScroll
	CmdDesc[n].Routine = pScroll;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "pScroll");
	CmdDesc[n].Category = CAT_PARTICLE;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "startSpd"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "endSpd"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "startSpdA"); CmdDesc[n].ParamEnvControlled[2] = false;
	strcpy_s(CmdDesc[n].ParamName[3], "endSpdA"); CmdDesc[n].ParamEnvControlled[3] = false;
	strcpy_s(CmdDesc[n].ParamName[4], "diection"); CmdDesc[n].ParamEnvControlled[4] = false;
#endif
	CmdDesc[n].ParamType[0] = PT_SWORD;
	CmdDesc[n].ParamType[1] = PT_SWORD;
	CmdDesc[n].ParamType[2] = PT_SWORD;
	CmdDesc[n].ParamType[3] = PT_SWORD;
	CmdDesc[n].ParamType[4] = PT_SWORD;
	CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_SetTexture
	CmdDesc[n].Routine = SetTexture;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetTexture");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "slot"); CmdDesc[n].ParamEnvControlled[0] = false;
	strcpy_s(CmdDesc[n].ParamName[1], "texture"); CmdDesc[n].ParamEnvControlled[1] = false;
	strcpy_s(CmdDesc[n].ParamName[2], "sampler"); CmdDesc[n].ParamEnvControlled[2] = false;
		strcpy_s(CmdDesc[n].ParamEnum[2][0], "nearest");
		strcpy_s(CmdDesc[n].ParamEnum[2][1], "linear");
#endif
	CmdDesc[n].ParamType[0] = PT_BYTE;
	CmdDesc[n].ParamType[1] = PT_BYTE;
	CmdDesc[n].ParamType[2] = PT_ENUM;
	CalcOfsAndSize(n);
#endif
	n++;

#ifdef used_SetZBuffer
	CmdDesc[n].Routine = SetZBuffer;
	CmdDesc[n].Level = 0;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetZBuffer");
	CmdDesc[n].Category = CAT_CMD;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	strcpy_s(CmdDesc[n].ParamName[0], "mode"); CmdDesc[n].ParamEnvControlled[0] = false;
		strcpy_s(CmdDesc[n].ParamEnum[0][0], "off");
		strcpy_s(CmdDesc[n].ParamEnum[0][1], "read&write");
		strcpy_s(CmdDesc[n].ParamEnum[0][2], "readonly");
		strcpy_s(CmdDesc[n].ParamEnum[0][3], "writeonly");

#endif
	CmdDesc[n].ParamType[0] = PT_ENUM;
	CalcOfsAndSize(n);
#endif
	n++;

	stack::CommandsCount = n;

	for (int x = 1; x < n; x++)
	{
		CmdDesc[x].ParamOffset[0] = 1;
	}

#ifdef EditMode
	//last M bytes for editor. todo:remove in runtime
	//int16 x,y bool selected bool mode
	for (int x = 1; x < n; x++)
	{
		CmdDesc[x].Size += EditorInfoOffset;
	}
#endif

}
