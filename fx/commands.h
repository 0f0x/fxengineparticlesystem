namespace Commands
{
	int playmode = 0;
	int prerender = 0;
	int cursor = 0;
	int cursorM = 0;
	double cursorF = 0;
	double StartPosition = 0;
	BYTE* currentPos;
	BYTE currentCmd;

	float inputFreq[MAXCHANNELS];
	float nextFreq[MAXCHANNELS];
	int clipLeft[MAXCHANNELS];
	short int noteLeft[MAXCHANNELS];
	short int activeNoteLeft[MAXCHANNELS];
	bool activeNoteFound[MAXCHANNELS] ;
	BYTE noteVelocity[MAXCHANNELS] ;
	signed char notePan[MAXCHANNELS];
	BYTE noteSlide[MAXCHANNELS] ;
	BYTE nextNoteVelocity[MAXCHANNELS] ;
	BYTE nextNoteSlide[MAXCHANNELS] ;
	signed char nextNotePan[MAXCHANNELS];

	short int loopedCursor[MAXCHANNELS];
	short int nextNotePos[MAXCHANNELS];
	short int noteSize[MAXCHANNELS];
	int currentChannel = 0;
	BYTE channel_amp[MAXCHANNELS];
	bool master = false;
	BYTE* loopPoint = NULL;
	#define PREBUFFER 4


	BYTE shaderSlotCounter = 0;

	void Release()
	{

	}

	void Project()
	{

	}

	void MainLoop()
	{
		loopPoint = currentPos;
	}

	void EndDraw()
	{

	}

	BYTE* CallStack[256];
	BYTE CallStackPointer = 0;
	bool SceneCallFlag = false;

	void Scene()
	{
	//	if (!SceneCallFlag)	SceneCallFlag = true; else SceneCallFlag = false;
	//	SceneCallFlag = true;
		if (CallStackPointer > 0) CallStackPointer--; else return;
		currentPos = CallStack[CallStackPointer];
	}

	void EndScene()
	{
	//	if (SceneCallFlag)	SceneCallFlag = false; else SceneCallFlag = true;
		
	//	SceneCallFlag = false;
		if (CallStackPointer > 0) CallStackPointer--; else return;
		currentPos = CallStack[CallStackPointer];
		
	}

	bool SceneLocateFlag = false;
	BYTE* ScenePointer[256];
	
	void ShowScene()
	{
		BYTE* ptr = stack::data;
		int c = 0;
		if (!SceneLocateFlag)
		{
			while (*ptr != 0)
			{
				if (CmdDesc[*ptr].Routine == Scene)
				{
					ScenePointer[c] = ptr;
					c++;
				}

				ptr += CmdDesc[*ptr].Size;
			}
		}

		#ifndef EditMode
			SceneLocateFlag = true;
		#endif	

			BYTE scene = *(BYTE*)(currentPos + CmdDesc[*(BYTE*)currentPos].ParamOffset[0]);

			CallStack[CallStackPointer] = currentPos;
			CallStackPointer++;

			currentPos = ScenePointer[scene];
	}

	//#define p_count 100000

	bool timeLoopEnable = false;
	signed short timeLoopStart = 0;
	signed short timeLoopLen = 0;

	void StopWave()
	{
		sound::pSourceVoice->Stop(0, 0);

	}

	void PlayWave(int start);

	void TimeLoop()
	{
	#ifdef EditMode
		bool _timeLoopEnable;
		signed short _timeLoopStart;
		signed short _timeLoopLen;

		_timeLoopEnable = *(BYTE*)(currentPos + 1);		
		_timeLoopStart = *(signed short*)(currentPos + 2);
		_timeLoopLen = *(signed short*)(currentPos + 4);

		//restart sound if loop settings changed
		if ((_timeLoopEnable != timeLoopEnable || 
			_timeLoopStart != timeLoopStart || 
			_timeLoopLen != timeLoopLen)
			&&(playmode==1||prerender==1))
		{
			timeLoopEnable = _timeLoopEnable;
			timeLoopStart = _timeLoopStart;
			timeLoopLen = _timeLoopLen;

			StartPosition = timeLoopStart;
			PlayWave(timeLoopStart);
		}

		

		timeLoopEnable = _timeLoopEnable;
		timeLoopStart = _timeLoopStart;
		timeLoopLen = _timeLoopLen;

		if (!timeLoopEnable) return;

		if (playmode == 1||prerender==1)
		{
			for (int x = 0; x < MAXCHANNELS - 1; x++) activeNoteFound[x] = false;
		}
	#endif
	}
/*
	struct ParticleParams
	{
		XMFLOAT4 EmitterPos;
		XMFLOAT4 EmitterSize;
		XMFLOAT4 EmitterType;
	};

	struct ParticleParamsP
	{
		XMFLOAT4 Color;
	};*/

	dx::ParticleParams  particleCB_v1_v;
	dx::ParticleParamsP particleCB_v1_p;

	void Emitter()
	{
		particleCB_v1_v.EmitterType.x = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);

		particleCB_v1_v.EmitterPos.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		particleCB_v1_v.EmitterPos.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		particleCB_v1_v.EmitterPos.z = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);
		particleCB_v1_v.EmitterPos.w = 0;

		particleCB_v1_v.EmitterSize.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[5]);
		particleCB_v1_v.EmitterSize.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[6]);
		particleCB_v1_v.EmitterSize.z = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[7]);

		particleCB_v1_v.MotionBlur.x = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[8]);

	}

	void Color()
	{
		//particleCB_v1_v.Color.r = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);

		particleCB_v1_v.Color.x = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		particleCB_v1_v.Color.y = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		particleCB_v1_v.Color.z = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		particleCB_v1_v.Color.w = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		
	}

	void Size()
	{
		particleCB_v1_v.Size.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		particleCB_v1_v.SizeRange.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
	}


	void Acceleration()
	{
		particleCB_v1_v.Acceleration.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		particleCB_v1_v.Acceleration.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		particleCB_v1_v.Acceleration.z = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);

		particleCB_v1_v.AccelerationRange.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		particleCB_v1_v.AccelerationRange.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);
		particleCB_v1_v.AccelerationRange.z = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[5]);

	}

	void Rotation()
	{
		particleCB_v1_v.Rotation.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		particleCB_v1_v.RotationPhase.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		particleCB_v1_v.RotationRange.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		particleCB_v1_v.RotationPhaseRange.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);

		particleCB_v1_v.RotationOffset.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);
		particleCB_v1_v.RotationOffset.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[5]);

	}

	void Spawn()
	{
		particleCB_v1_v.Start.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		particleCB_v1_v.End.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		particleCB_v1_v.PPS.x = (float)*(INT32*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		particleCB_v1_v.LifeTime.x = (float)*(INT32*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
	}

	void Velocity()
	{
		particleCB_v1_v.Velocity.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		particleCB_v1_v.Velocity.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		particleCB_v1_v.Velocity.z = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);

		particleCB_v1_v.VelocityRange.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		particleCB_v1_v.VelocityRange.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);
		particleCB_v1_v.VelocityRange.z = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[5]);

		particleCB_v1_v.Deaccel.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[6]);
	}

	void Orientation()
	{
		particleCB_v1_v.Basis.x = 0;//source basis!
		particleCB_v1_v.Basis.y = 1;
		particleCB_v1_v.Basis.z = 0;
		particleCB_v1_v.Basis.w = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
	}

	void pEvo()
	{
		particleCB_v1_p.evoSpeed.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		particleCB_v1_p.evoSpeed.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		particleCB_v1_p.evoSpeed.z = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		particleCB_v1_p.evoSpeed.w = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		particleCB_v1_p.evoFold.x  = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);
	}

	void pScroll()
	{
		particleCB_v1_p.scrollSpeed.x = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		particleCB_v1_p.scrollSpeed.y = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		particleCB_v1_p.scrollSpeed.z = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		particleCB_v1_p.scrollSpeed.w = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		particleCB_v1_p.scrollDir.x   = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);
	}

	float clamp(float a, float _min, float _max);
	float frac(float a);
	double frac_d(double a);
	
	void UpdateVB()
	{
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		
		int prtPerSec = (int)particleCB_v1_v.PPS.x;
		int lifetime = (int)particleCB_v1_v.LifeTime.x;
		int pCount = prtPerSec*lifetime;

		float stime = (float)(cursorF / 60.0f);
		float effectend = particleCB_v1_v.End.x;//in sec
		float end = (float)floor(effectend);

		pCount = min(4000, pCount);
		int i = (int)(frac((float)(stime * prtPerSec / pCount))*pCount);

		int len = (int)floor(prtPerSec / 60.0) + 1;

		dx::g_pImmediateContext->Map(dx::pVertexBuffer[0], 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &mappedResource);

		for (int c = 0; c < len; c++)
		{
			//int d = frac((float)(i + c)/(float)pCount)*pCount;
			int d = (i + c);

			if (d >= pCount) d -= pCount;

			for (int v = 0; v < 6; v++)
			{
				dx::vertices[d * 6 + v].Pos.x = (float)sin(stime / 4.0f);

			}
		}

		memcpy(mappedResource.pData, dx::vertices, sizeof(dx::Vertex)*pCount*6);
		dx::g_pImmediateContext->Unmap(dx::pVertexBuffer[0], 0);

	}

	
	INT32 lastpoint = 0;
	INT32 lastframe = 0;
	POINT mp;
	float _old_x = 0;
	float _old_y = 0;

	float lerp(float a, float b, float v);

	void UpdateVBpart()
	{

		INT32 prtPerSec = (INT32)particleCB_v1_v.PPS.x;
		int lifetime = (int)particleCB_v1_v.LifeTime.x;
		INT32 pCount = (INT32)round(prtPerSec*lifetime / 60.0);

		INT32 pCountT = (INT32)(prtPerSec * particleCB_v1_v.End.x / 60.0);
	//	if (pCountT<pCount) pCount = pCountT;

		float stime = (float)(cursorF / 60.0);
		float effectend = particleCB_v1_v.End.x;//in sec
		float end = (float)floor(effectend);

//----
		double prtPerFrame = prtPerSec / 60.;

		int posbufferSize = min(60,lifetime);
		//int posbufferSize = 60;
		//int posbufferSize = 60;

		//if (posbufferSize % 60 != 0) return;

		particleCB_v1_v.PosBufferSize.x = posbufferSize;

		//INT32 i = frac_d(cursor / (double)lifetime)*posbufferSize;
		//INT32 i = posbufferSize*(cursor / prtPerFrame)/ pCount;
		INT32 i = posbufferSize*cursor / (double)lifetime;
		//INT32 i = frac(stime * prtPerSec / pCount) * posbufferSize;


		if (GetAsyncKeyState(VK_LBUTTON))
		{
			GetCursorPos(&mp);
			ScreenToClient(hWnd, &mp);
		}
		
		float _x, _y;
		_x = mp.x / float(dx::width) * 2.f - 1;
		_y = -(mp.y / float(dx::height) * 2.f - 1);

	//	_x = sin(stime*.4)*.25;
	//	_y = cos(stime*.5)*.25;

	//	_x *= 1.f / dx::aspect;
	
	//	_x *= sin(stime * .5);
	//	_y*= sin(stime * .6);

		INT32 endpoint = i + 1;

		
		if (lastframe!=cursor)
		{
		//	lastpoint = frac(lastpoint / (float)pCount)*pCount;
		//	endpoint = frac(endpoint / (float)pCount)*pCount;
			for (INT32 c = lastpoint; c < endpoint; c++)
			{
				INT32 d = c;
				//d = frac_d(d / (double)(posbufferSize))*posbufferSize;
				//while (d >= posbufferSize) d -= posbufferSize;
				d = d % posbufferSize;
				
				float p = (c-lastpoint) / (double)(endpoint - lastpoint);

				particleCB_v1_v.PosTable[d].x = lerp(_old_x,_x,p);
				particleCB_v1_v.PosTable[d].y = lerp(_old_y,_y,p);
				//particleCB_v1_v.PosTable[d].z = particleCB_v1_v.PosTable[d].y-particleCB_v1_v.PosTable[d].x;
				particleCB_v1_v.PosTable[d].z = 0;
				particleCB_v1_v.PosTable[d].w = 1;
			}
			_old_x = _x;
			_old_y = _y;
			lastpoint = endpoint;
			lastframe = cursor;
			//if (lastpoint >= pCount) lastpoint -= pCount;
		//	lastpoint = frac(endpoint / (float)pCount)*pCount;


		}
		


	}

	BYTE blendIndex = 0;

	void BlendMode()
	{
		BYTE alfa = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		BYTE op = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		blendIndex = alfa * 5 + op;
	}

	

	

	void ParticleContainer()
	{

		INT32 prtPerSec = (INT32)particleCB_v1_v.PPS.x;

		if (prtPerSec < 1) return;

		int lifetime = (int)particleCB_v1_v.LifeTime.x;

		BYTE op = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);

		if (op==0)
		{
			if (lifetime < 1) return;
		}

		INT32 pCount = (INT32)round(prtPerSec*lifetime/60.0);

		INT32 pCountT = (INT32)(prtPerSec * particleCB_v1_v.End.x / 60.0);
		if (pCountT<pCount) pCount=pCountT;

		float stime = (float)(cursor / 60.0);
		float effectend = particleCB_v1_v.End.x;//in sec
		float end = (float)floor(effectend);

		//if (end < 1) return;
		if (op==1)
		{
			pCount = prtPerSec;
			pCount = sqrt(pCount);

			pCount *= pCount;
		}


/*
		{

			if (GetAsyncKeyState(VK_LBUTTON))
			{
				GetCursorPos(&mp);
				ScreenToClient(hWnd, &mp);
			}

			float _x, _y;
			_x = mp.x / float(dx::width) * 2.f - 1;
			_y = -(mp.y / float(dx::height) * 2.f - 1);
			_x *= 1.f / dx::aspect;


			INT32 n = frac(stime * prtPerSec / pCount) * posbufferSize;
			if (n >= pCount) n -= pCount;

			particleCB_v1_v.PosTable[n].x = _x;
			particleCB_v1_v.PosTable[n].y = _y;
			particleCB_v1_v.PosTable[n].z = 0;
			particleCB_v1_v.PosTable[n].w = 1;
		}

*/


	//	pCount = min(p_count, pCount);
		INT32 i = (INT32)(frac(stime * prtPerSec/pCount )*pCount);
		int len = (int)floor(prtPerSec / 60.0) + 1;

		if (end>cursor&&op==0) UpdateVBpart();

		particleCB_v1_v.MaxParticleCount.x = (float)pCount;

		INT32 n = *(INT32*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		UINT stride = sizeof(dx::Vertex);
		UINT offset = 0;
	//	dx::g_pImmediateContext->IASetVertexBuffers(0, 1, &dx::pVertexBuffer[n], &stride, &offset);
		dx::g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		
		dx::g_pImmediateContext->VSSetConstantBuffers(0, 1, &dx::pConstantBufferV);
		dx::g_pImmediateContext->PSSetConstantBuffers(0, 1, &dx::pConstantBufferP);

		dx::g_pImmediateContext->VSSetConstantBuffers(1, 1, &dx::pParticleCB_t1V);
		dx::g_pImmediateContext->PSSetConstantBuffers(1, 1, &dx::pParticleCB_t1V);
		dx::g_pImmediateContext->PSSetConstantBuffers(2, 1, &dx::pParticleCB_t1P);


		dx::g_pImmediateContext->VSSetShader(dx::Shader[n].pVs, NULL, 0);
		dx::g_pImmediateContext->PSSetShader(dx::Shader[n].pPs, NULL, 0);

		dx::g_pImmediateContext->VSSetShaderResources(0, 1, &dx::TextureResView[0]);
		dx::g_pImmediateContext->VSSetSamplers(0, 1, &dx::pSamplerLinear);
		dx::g_pImmediateContext->VSSetSamplers(1, 1, &dx::pSamplerNone);
		dx::g_pImmediateContext->VSSetShaderResources(2, 1, &dx::TextureResView[1]);

		dx::g_pImmediateContext->PSSetShaderResources(0, 1, &dx::TextureResView[0]);
		dx::g_pImmediateContext->PSSetSamplers(0, 1, &dx::pSamplerLinear);
		dx::g_pImmediateContext->PSSetShaderResources(1, 1, &dx::g_pDepthStencilSRView);
		dx::g_pImmediateContext->PSSetSamplers(1, 1, &dx::pSamplerNone);

		float blendFactor[4];
		blendFactor[0] = 0.0f;
		blendFactor[1] = 0.0f;
		blendFactor[2] = 0.0f;
		blendFactor[3] = 0.0f;
	//	dx::g_pImmediateContext->IASetInputLayout(dx::pVertexLayout[n]);


		dx::ConstantBuffer cb;
		cb.time.x = (float)cursor;
		cb.time.y = (cursor-timeLoopStart)/(float)timeLoopLen;
		cb.aspectRatio.x = dx::aspect;

		cb.View = dx::View;
		cb.Proj = dx::Projection;

		dx::g_pImmediateContext->UpdateSubresource(dx::pConstantBufferV, 0, NULL, &cb, 0, 0);
		dx::g_pImmediateContext->UpdateSubresource(dx::pConstantBufferP, 0, NULL, &cb, 0, 0);

		dx::g_pImmediateContext->UpdateSubresource(dx::pParticleCB_t1V, 0, NULL, &particleCB_v1_v, 0, 0);
		dx::g_pImmediateContext->UpdateSubresource(dx::pParticleCB_t1P, 0, NULL, &particleCB_v1_p, 0, 0);


		float blendFactor2[4];
		blendFactor2[0] = 0.0f;
		blendFactor2[1] = 0.0f;
		blendFactor2[2] = 0.0f;
		blendFactor2[3] = 0.0f;

		dx::g_pImmediateContext->OMSetBlendState(dx::bs[blendIndex], blendFactor2,0xffffffff);
		
		INT32 count = pCount;
		//count = min(count, p_count);

		INT32 launched = (INT32)clamp((float)(cursor * prtPerSec / 60),0,(float)count);// -max(cursor - lifetime, 0)*prtPerSec / 60;
		INT32 waiting = count - launched;
		
		INT32 tail = (INT32)min(waiting + max(cursor - effectend, 0)*prtPerSec / 60, count);

		//debug output
		*(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1])=(signed short)count;

		dx::g_pImmediateContext->IASetInputLayout(NULL);
		dx::g_pImmediateContext->IASetVertexBuffers(0, 0, NULL, NULL, NULL);

		dx::g_pImmediateContext->Draw((count)*6, 0);

	}

	//
	void SetTexture()
	{
		BYTE slot = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		BYTE texture = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		BYTE sampler = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		dx::g_pImmediateContext->PSSetShaderResources(slot, 1, &dx::TextureResView[texture]);
		dx::g_pImmediateContext->PSSetSamplers(0, 1, &dx::pSamplerLinear);
		dx::g_pImmediateContext->PSSetSamplers(1, 1, &dx::pSamplerNone);
		

		dx::g_pImmediateContext->PSSetShaderResources(slot+1, 1, &dx::g_pDepthStencilSRView);

	}

	void SetZTexture()
	{
		BYTE slot = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		BYTE texture = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		BYTE sampler = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);


		dx::g_pImmediateContext->PSSetShaderResources(slot, 1, &dx::g_pDepthStencilSRView);
		dx::g_pImmediateContext->PSSetSamplers(0, 1, &dx::pSamplerLinear);
	}

	void Quad()
	{
		int tn = 0;

		if (dx::currentRTn == -1)
		{
			tn = tempTexCount-1;
			ID3D11Resource* pResource = NULL;
			dx::g_pRenderTargetView->GetResource(&pResource);

			dx::g_pImmediateContext->CopyResource(dx::temptexture[tn], pResource);
		}
		else
		{
			int size = (int)dx::Tsize[dx::currentRTn].x;
			switch (size)
			{
				case 32: tn = 0; break;
				case 64: tn = 1; break;
				case 128: tn = 2; break;
				case 256: tn = 3; break;
				case 512: tn = 4; break;
				case 1024: tn = 5; break;
				case 2048: tn = 6; break;
			}

		//	if (dx::TFormat[dx::currentRTn] == 1) tn += 7;

		//	if (dx::TFormat[dx::currentRTn] == 2) tn += 7*2;

			dx::g_pImmediateContext->CopyResource(dx::temptexture[tn], dx::pTexture[dx::currentRTn]);
		}

		

		dx::ConstantBuffer cb;
		cb.time.x = (float)cursor;
		cb.time.y = (cursor - timeLoopStart) / (float)timeLoopLen;
		cb.aspectRatio.x = dx::aspect;

		cb.View = dx::View;
		cb.Proj = dx::Projection;

		for (int x = 1; x < MAXPARAM; x++)
		{
		//	switch (CmdDesc[currentCmd].ParamType[x])
			{
		//	case PT_ENUM:
		//	case PT_BYTE:
		//		cb.params[x].x = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[x]);
		//		break;
		//	case PT_SWORD:
				cb.params[x].x = *(short int*)(currentPos + CmdDesc[currentCmd].ParamOffset[x]);
		//		break;
			}
		}

		dx::g_pImmediateContext->UpdateSubresource(dx::pConstantBufferV, 0, NULL, &cb, 0, 0);
		dx::g_pImmediateContext->UpdateSubresource(dx::pConstantBufferP, 0, NULL, &cb, 0, 0);

		UINT stride = sizeof(dx::Vertex);
		UINT offset = 0;
		dx::g_pImmediateContext->IASetVertexBuffers(0, 1, &dx::ppVB, &stride, &offset);
		dx::g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		int n = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		dx::g_pImmediateContext->VSSetShader(dx::Shader[n].pVs, NULL, 0);
		dx::g_pImmediateContext->PSSetShader(dx::Shader[n].pPs, NULL, 0);
		dx::g_pImmediateContext->IASetInputLayout(dx::pVertexLayout[0]);

			dx::g_pImmediateContext->PSSetShaderResources(0, 1, &dx::TempTextureResView[tn]);
			dx::g_pImmediateContext->PSSetSamplers(0, 1, &dx::pSamplerLinear);
			
			float blendFactor2[4];
			blendFactor2[0] = 0.0f;
			blendFactor2[1] = 0.0f;
			blendFactor2[2] = 0.0f;
			blendFactor2[3] = 0.0f;

			dx::g_pImmediateContext->OMSetBlendState(dx::bs[blendIndex], blendFactor2, 0xffffffff);

		dx::g_pImmediateContext->Draw( 6, 0);
	
	}


	void Track()
	{

	}

	void EndTrack()
	{

	}


	bool channelOn[MAXCHANNELS];

	BYTE channelVolume[MAXCHANNELS];
	signed char channelPan[MAXCHANNELS];
	bool channelSolo[MAXCHANNELS];
	bool inClip[MAXCHANNELS];

	void Channel()
	{
		//currentChannel = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		currentChannel++;
		inClip[currentChannel] = false;
		bool solomode = false;
		for (int x = 0; x<MAXCHANNELS;x++)
		{
			if (channelSolo[x] == true) solomode = true;
		}

		if (currentChannel >= 0 && currentChannel < MAXCHANNELS-1)
		{
			channelOn[currentChannel] = false;

			channelVolume[currentChannel] = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
			channelPan[currentChannel] = *(signed char*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);

			BYTE mute = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);
			BYTE solo = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[5]);
			channelSolo[currentChannel] = solo;

			if (solomode)
			{
				channelVolume[currentChannel] *= solo;
			}
			else
			{
				channelVolume[currentChannel] *= (1 - mute);
			}
		}
	}

	void Note()
	{

	}

#define clip_x 1
#define clip_lenght 5
#define clip_repeat 7
#define clip_npm 11

	float lerp(float a, float b, float v)
	{
		return (float)(a*(1.0 - v) + b*v);
	}

	float clamp(float a, float _min, float _max)
	{
		return min(_max, max(a, _min));
	}
	
	double clamp_d(double a, double _min, double _max)
	{
		return min(_max, max(a, _min));
	}

	float frac(float a)
	{
		return a - (float)floor(a);
	}

	double frac_d(double a)
	{
		return a - (double)floor(a);
	}
	//				char n[13] = "cCdDefFgGaAB";

	/*				note = note%12;
	char a[2];
	a[0]= n[note];
	a[1] = 0;
	SetWindowText(hWnd, a);*/
	
	
	int clipLen = 0;

	void Clip()
	{
		if (currentChannel < 0) return;
		signed short left = *(signed short*)(currentPos + clip_x);
		signed short len = *(signed short*)(currentPos + clip_lenght);

		if (len == 0) {
	//		inClip = false;
			return;
		}

		signed short repeat = *(signed short*)(currentPos + clip_repeat);
		signed short npm = (*(signed short*)(currentPos + clip_npm))*4;

		if (npm == 0) return;

		signed short right = left + (repeat + 1)*len*(int)(3600 / (int)npm);

		

		if (cursorM<left || cursorM>right)
		{
		//	inClip = false;
			return;
		}

		clipLen = len * (int)(3600 / (int)npm);

		inClip[currentChannel] = true;

		channelOn[currentChannel] = true;

		clipLeft[currentChannel] = left;

		noteLeft[currentChannel] = left;
		noteSize[currentChannel] = 3600 / (int)npm;
		loopedCursor[currentChannel] = (cursorM - left) % ((int)(len*noteSize[currentChannel])) + left;

		BYTE* i = currentPos + CmdDesc[currentCmd].Size;

		BYTE note = 0; int noteCounter = 0;

	/*	if (cursorM > loopedCursor[currentChannel])
		{
			
			nextFreq[currentChannel] = inputFreq[currentChannel];
		}*/

		//first note for looping
		if (*i != 0 && CmdDesc[*i].Routine == Note)
		{
			BYTE n = *(i + 1);
			if (n > 0)
			{
				note = n - 1;
				nextFreq[currentChannel] = 16.3515987f *(float)pow(2.0f, note / 12.0f);
			}
			//nextNotePos[currentChannel] = noteLeft[currentChannel];
			nextNoteVelocity[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[1]);
			nextNoteSlide[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[2]);
			nextNotePan[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[3]);

		}
		

		//activeNoteLeft[currentChannel] = noteLeft[currentChannel];
		//activeNoteFound[currentChannel] = false;
		while (*i != 0 && CmdDesc[*i].Routine == Note)
		{
			noteLeft[currentChannel] = left + noteCounter* (int) (3600 / (int)npm);

			BYTE n = *(i + 1);

			if (n > 0 && *(BYTE*)(i+CmdDesc[*i].ParamOffset[2])==0)
			{
				activeNoteLeft[currentChannel] = noteLeft[currentChannel];
				
			}

			if (n > 0)
			{
				note = n - 1;
				activeNoteFound[currentChannel] = true;
				inputFreq[currentChannel] = 16.3515987f *(float)pow(2.0f, note / 12.0f);
			}



			noteVelocity[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[1]);
			noteSlide[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[2]);
			notePan[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[3]);



			if (loopedCursor[currentChannel] >= noteLeft[currentChannel] && loopedCursor[currentChannel] < noteLeft[currentChannel] + noteSize[currentChannel])
			{

				if (noteCounter < len - 1)
				{
					i += CmdDesc[*i].Size;
					n = *(i + 1);
					if (n > 0)
					{
						note = n - 1;
						nextFreq[currentChannel] = 16.3515987f *(float)pow(2.0f, note / 12.0f);
					}
					else
					{
						nextFreq[currentChannel] = inputFreq[currentChannel];
					}


						nextNoteVelocity[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[1]);
						nextNoteSlide[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[2]);
						nextNotePan[currentChannel] = *(BYTE*)(i + CmdDesc[*i].ParamOffset[3]);
					
						//nextNotePos[currentChannel] = noteLeft[currentChannel] + noteSize[currentChannel];
				}

				return;
			}

		
			i += CmdDesc[*i].Size;
			noteCounter++;
		}

	}

	float temporalParams[MAXCHANNELS][MAXPARAM];
	float temporalParams2[MAXCHANNELS][MAXPARAM];
	int temporalParamsOp[MAXCHANNELS][MAXPARAM];

	short int envLoopedCursor = 0;
	short int envLoopedCursor2 = 0;

#define Envelope_x 1
#define Envelope_y 3
#define Envelope_repeat 5
#define Envelope_assign 7
#define Envelope_assignOp 8
#define Envelope_min 9
#define Envelope_max 11

#define Point_x 1
#define Point_y 3

#define envOp_plus 0
#define envOp_minus 1
#define envOp_cpy 2
#define envOp_mul 3
#define envOp_div 4

	float EnvOp(float value, float env, int op)
	{
		switch (op)
		{
		case envOp_plus:
			return value + env;
		case envOp_minus:
			return value - env;
		case envOp_cpy:
			return env;
		case envOp_mul:
			return value * env;
		case envOp_div:
			return value / env;
		}
		return value;
	}

	void ResetEnv()
	{
		if (currentChannel < 0) return;
		for (int x = 0; x < MAXPARAM; x++)
		{
			temporalParams[currentChannel][x] = 0;
			temporalParams2[currentChannel][x] = 0;
			temporalParamsOp[currentChannel][x] = 0;//+op whith 0 arg for no affects
		}
	}

	void Point()
	{

	}

	float initial_phase[MAXCHANNELS];
	double sum[MAXCHANNELS];
	int framelen = 44100 / 60;
	int preCount = 0;
	
	
	void Envelope()
	{
		if (currentChannel < 0) return;
		if (inClip[currentChannel] == false) return;

		BYTE assign = *(BYTE*)(currentPos + Envelope_assign);
		BYTE assignOp = *(BYTE*)(currentPos + Envelope_assignOp);

		temporalParams[currentChannel][assign] = 0;
		temporalParams2[currentChannel][assign] = 0;
		temporalParamsOp[currentChannel][assign] = 0;//+op whith 0 arg for no affects

		BYTE link = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[8]);
		BYTE retrigger = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);

		short int firstX,firstY;
		if (link == 0)
		{
			firstX = *(signed short*)(currentPos + Envelope_x);// +clipLeft[currentChannel];
		}
		else
		{
			firstX = clipLeft[currentChannel];
		}
		firstY = *(signed short*)(currentPos + Envelope_y);

		if (retrigger == 1)
		{
			firstX = activeNoteLeft[currentChannel];
		}

		if (cursorM < firstX) return;//left margin

		


		//search max x, period, loopedcursor
		BYTE* i = currentPos + CmdDesc[currentCmd].Size;
		short int maxpoint = firstX;
	

		while (*i != 0 && CmdDesc[*i].Routine == Point)
		{
			short int pointX = *(signed short*)(i + Point_x) + firstX;

			maxpoint = max(pointX, maxpoint);
			i += CmdDesc[*i].Size;
		}
		short int period = maxpoint - firstX;

		if (period <= 0) return;

		int _cursorM;

		if (retrigger == 0)
		{
			_cursorM = cursorM;

			envLoopedCursor = (_cursorM - firstX) % (period)+firstX;
			envLoopedCursor2 = (1 + _cursorM - firstX) % (period)+firstX;
		}
		else
		{
			
			_cursorM = loopedCursor[currentChannel];
			//_cursorM = cursorM;
			int fofs = 0;
			if (_cursorM < firstX)
			{
				fofs= clipLen;
			}

			envLoopedCursor = (_cursorM -firstX+fofs) % (period)+firstX;
			envLoopedCursor2 = (1 + _cursorM -firstX+fofs) % (period)+firstX;

		}


		//search left and right points
		i = currentPos + CmdDesc[currentCmd].Size;

		short int leftX = firstX;
		short int leftY = firstY;

		short int rightX = firstX;
		short int rightY = firstY;

		short int leftdelta = 32767;
		short int rightdelta = 32767;

		while (*i != 0 && CmdDesc[*i].Routine == Point)
		{
			short int x = *(signed short*)(i + Point_x) + firstX;
			short int y = *(signed short*)(i + Point_y);

			if (x <= envLoopedCursor)
			{
				short int td = envLoopedCursor - x;
				if (td < leftdelta)
				{
					leftdelta = td;
					leftX = x;
					leftY = y;
				}
			}

			if (x > envLoopedCursor)
			{
				short int td = x - envLoopedCursor;
				if (td < rightdelta)
				{
					rightdelta = td;
					rightX = x;
					rightY = y;
				}
			}

			i += CmdDesc[*i].Size;
		}

		float n = 0; float n2 = 0;
		if (rightX != leftX)
		{
			n = (float)(envLoopedCursor - leftX) / (float)(rightX - leftX);
			n2 = (float)(envLoopedCursor2 - leftX) / (float)(rightX - leftX);

 			if (envLoopedCursor2 < envLoopedCursor)
			{
				n2 = 1.0;
			}
		}
		else
		{
			n = (float)(envLoopedCursor - leftX-1) / (float)(rightX - leftX+1);
			n2 = (float)(envLoopedCursor2 - leftX-1) / (float)(rightX - leftX+1);

			if (envLoopedCursor2 < envLoopedCursor)
			{
				n2 = 1.0;
			}
		}


		float y = lerp(leftY, rightY, n);
		float y2 = lerp(leftY, rightY, n2);

		temporalParams[currentChannel][assign] = y;
		temporalParams2[currentChannel][assign] = y2;

		temporalParamsOp[currentChannel][assign] = assignOp;

	}

	int colorPointCounter = 0;
	int fxPointCounter = 0;

	void ColorPoint()
	{
		if (colorPointCounter > 4) return;

		BYTE r, g, b, a;
		BYTE x;

		r = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		g = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		b = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		a = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);

		x = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);

		particleCB_v1_v.ColorOverLife[colorPointCounter] = XMFLOAT4(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
		particleCB_v1_v.ColorOverLifePos[colorPointCounter].x = (float)(x / 100.0f);

		colorPointCounter++;
		particleCB_v1_v.cpCount.x = (float)colorPointCounter;

		
	}

	void fxPoint()
	{
		if (fxPointCounter > 4) return;

		BYTE x, y;

		x = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		y = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);

		particleCB_v1_v.SizeOverLife[fxPointCounter].x = (float)(x / 100.0);
		particleCB_v1_v.SizeOverLife[fxPointCounter].y = (float)(y / 100.0);

		fxPointCounter++;
		particleCB_v1_v.cpCount.y = (float)fxPointCounter;
	}


	void ControlCurve()
	{
		colorPointCounter = 0;
		fxPointCounter = 0;

	}


	void ClearAllChannels()
	{

		for (int x = 0; x < MAXCHANNELS-1; x++)
		{
			ZeroMemory(sound::channel[x], sound::channelLen * 2);
			initial_phase[x] = 0;
			sum[x] = 0;
		}
		ZeroMemory(sound::channel[MAXCHANNELS - 1], sound::len * 2);
		initial_phase[MAXCHANNELS - 1] = 0;
		sum[MAXCHANNELS - 1] = 0;


		for (int x = 0; x < MAXCHANNELS; x++)
		{
			ZeroMemory(sound::fxBus[x], sound::fxBusLen * 2);
		}
	}


	void Render();



	void PlayWave(int start)
	{
		sound::pSourceVoice->Stop(0, 0);

		if (StartPosition < 0) StartPosition = 0;
		if (start < 0) start = 0;

		for (int x = 0; x < MAXCHANNELS; x++)
		{
			channel_amp[x] = *(currentPos + x + 1);
			activeNoteFound[x] = false;
		}

		ClearAllChannels();
		ResetEnv();

		Commands::playmode = 0;
		Commands::prerender = 1;

		for (int z = 0; z < PREBUFFER; z++)
		{		
			preCount = z;
			Render();
		}
		//preCount++;
		Commands::prerender = 0;
		Commands::playmode = 1;



		int startS = start * 44100 / 60;

		int x = MAXCHANNELS - 1;

		//ZeroMemory(&buffer, sizeof(buffer));
		sound::buffer.AudioBytes = 2 * 2 * sound::len;
		//buffer.pAudioData = channel[x];
			sound::buffer.Flags = XAUDIO2_END_OF_STREAM;
		//buffer.PlayBegin = 0;
		//	buffer.PlayLength = 2*(len - start * 44100 / 60);
		sound::buffer.PlayBegin = start * 44100 / 60;

		if (timeLoopEnable&&cursorF>=timeLoopStart&&cursorF<timeLoopStart+timeLoopLen)
		{
			sound::buffer.LoopBegin = timeLoopStart * 44100 / 60;
			sound::buffer.LoopCount = XAUDIO2_LOOP_INFINITE;
			sound::buffer.LoopLength = timeLoopLen * 44100 / 60;
		}
		else
		{
			sound::buffer.LoopBegin = 0;
			sound::buffer.LoopLength = sound::len;
			sound::buffer.LoopCount = 1;
		}


		sound::pSourceVoice->FlushSourceBuffers();
		sound::pSourceVoice->SubmitSourceBuffer(&sound::buffer);
		sound::pSourceVoice->Start();

		timer::resetStartFlag = true;
		//timer::StartTime = timer::GetCounter();
	}

#ifdef EditMode
	void PlayOneNote(int cp)
	{
		//search position
		BYTE* i = stack::data;
		bool found = false;
		signed short x = 0;
		int notecounter = 0;
		signed short npm = 0;

		while (*i != 0 && found==false)
		{
			if (CmdDesc[*i].Routine == Clip)
			{
				x = *(signed short*)(i + CmdDesc[*i].ParamOffset[0]);
				npm = *(signed short*)(i + CmdDesc[*i].ParamOffset[6])*4;
				notecounter = -1;
			}

			if (CmdDesc[*i].Routine == Note)
			{
				notecounter++;
			}

			if (i >= stack::data + cp)
			{
				found = true;
			}

			i += CmdDesc[*i].Size;
		}

		//calc stop position
		//play
		if (npm == 0) npm = 1;
		int intNPM = (int)(3600 / (int)npm);

		signed short start = x + notecounter * intNPM;
		StartPosition = start;


		//------------------

		sound::pSourceVoice->Stop(0, 0);

		if (StartPosition < 0) StartPosition = 0;
		if (start < 0) start = 0;

		for (int x = 0; x < MAXCHANNELS; x++)
		{
			channel_amp[x] = *(currentPos + x + 1);
			activeNoteFound[x] = false;
		}

		ClearAllChannels();
		ResetEnv();

		Commands::playmode = 0;
		prerender = 1;
		for (int z = 0; z < 4; z++)
		{
			preCount = z;
			Render();
		}
		//preCount++;

		prerender = 0;
		int ch = MAXCHANNELS - 1;

		int declick = 735;
		int start_ = start * 44100 / 60;
	/*		for (int t = 0; t < declick; t++)
			{
				signed short valueL = *(signed short*)(sound::channel[ch] + (t + start_) * 4);
				signed short valueR = *(signed short*)(sound::channel[ch] + (t + start_) * 4 + 2);

				float v= t / (float)declick;
				valueL *= v;
				valueR *= v;

				*(signed short*)(sound::channel[ch] + (t + start_ ) * 4) = valueL;
				*(signed short*)(sound::channel[ch] + (t + start_ ) * 4 + 2) = valueR;
			}*/

		int startS = start * 44100 / 60;



		//ZeroMemory(&buffer, sizeof(buffer));
		sound::buffer.AudioBytes = 2 * 2 * sound::len;
		//buffer.pAudioData = channel[x];
		//	sound::buffer.Flags = XAUDIO2_NO_LOOP_REGION;
		//buffer.PlayBegin = 0;
		//	buffer.PlayLength = 2*(len - start * 44100 / 60);
		sound::buffer.PlayBegin = start * 44100 / 60;
		sound::pSourceVoice->FlushSourceBuffers();
		sound::pSourceVoice->SubmitSourceBuffer(&sound::buffer);
		sound::pSourceVoice->Start();
		timer::StartTime = timer::GetCounter();
		Commands::playmode = 1;
		prerender = 0;
		

	}
#endif

#define Oscillator_freqshift 1
#define Oscillator_volume 3
#define Oscillator_freqshiftN 0
#define Oscillator_volumeN 1

#define Oscillator_type 4
#define Oscillator_harmCount 5
#define Oscillator_harmDetune 6
#define Oscillator_harmAmp 7
#define Oscillator_harmAmpN 5
	
	float prev_amp[MAXCHANNELS];

	void Oscillator()
	{

		if (currentChannel <0) return;
		if (inClip[currentChannel] == false) return;

		//if (channelOn[currentChannel]==false) return;
		
		if (cursorM < 0) return;

		if (activeNoteFound[currentChannel] == false)
		{
			for (int i = 0; i < framelen * 2; i += 2)
			{
				*(signed short*)(sound::channel[currentChannel] + (i ) * 2) = 0;
					* (signed short*)(sound::channel[currentChannel] + (i ) * 2 + 2) = 0;
			}
			return;
		}



		short int _freqShift = *(signed short*)(currentPos + Oscillator_freqshift);
		BYTE _amp = *(BYTE*)(currentPos + Oscillator_volume);

		float freqShift  = EnvOp(_freqShift, temporalParams[currentChannel][Oscillator_freqshiftN],  temporalParamsOp[currentChannel][Oscillator_freqshiftN]);
		float freqShift2 = EnvOp(_freqShift, temporalParams2[currentChannel][Oscillator_freqshiftN], temporalParamsOp[currentChannel][Oscillator_freqshiftN]);

		float amp  = EnvOp(_amp / 255.0f, temporalParams[currentChannel][Oscillator_volumeN] / 255.0f,  temporalParamsOp[currentChannel][Oscillator_volumeN]);
		float amp2 = EnvOp(_amp / 255.0f, temporalParams2[currentChannel][Oscillator_volumeN] / 255.0f, temporalParamsOp[currentChannel][Oscillator_volumeN]);
		

		int startI = 0;
		int start = cursorM*44100 / 60;
		 
	
		 int wavetype = *(BYTE*)(currentPos + Oscillator_type);
		 BYTE h_count = *(BYTE*)(currentPos + Oscillator_harmCount);

		 BYTE h_detune = *(BYTE*)(currentPos + Oscillator_harmDetune);
		 float h_detune1 = EnvOp(h_detune / 255.0f, temporalParams[currentChannel][4] / 255.0f, temporalParamsOp[currentChannel][4]);
		 float h_detune2 = EnvOp(h_detune / 255.0f, temporalParams2[currentChannel][4] / 255.0f, temporalParamsOp[currentChannel][4]);


		 BYTE h_amp = *(BYTE*)(currentPos + Oscillator_harmAmp);
		 float h_amp1 = EnvOp(h_amp / 255.0f, temporalParams[currentChannel][Oscillator_harmAmpN] / 255.0f, temporalParamsOp[currentChannel][Oscillator_harmAmpN]);
		 float h_amp2 = EnvOp(h_amp / 255.0f, temporalParams2[currentChannel][Oscillator_harmAmpN] / 255.0f, temporalParamsOp[currentChannel][Oscillator_harmAmpN]);

		 switch (wavetype)
		 {
			case 0:
				h_count = 1;
				break;
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;

		 }

		 float hl = 1.f / (float)h_count;
		 float ch = 0;

		 double fStart = sum[currentChannel];
		 double f = fStart;

			for (int i = 0; i < framelen*2; i+=2 )
			{
				float s = float(i / 2) / (float)framelen;
				float slide = (float)((i/2 + (loopedCursor[currentChannel] -noteLeft[currentChannel])*44100.0/ 60.0))/(float)((noteSize[currentChannel])*44100.0 / 60.0);
				float slidespeed = (255.0f - noteSlide[currentChannel])/64.0f;
				slidespeed = (float) pow(slidespeed, slidespeed);
				slide = (float)pow(slide, slidespeed + 1.f);
				slide = clamp(slide, 0, 1);
				float Freq = lerp(inputFreq[currentChannel], nextFreq[currentChannel], slide)+lerp(freqShift, freqShift2,s);

				float la = 0;

				float h_detuneI = lerp(h_detune1, h_detune2, s);
				
					switch (wavetype)
					{
						case 0://sin
						{
							for (int h = 0; h < h_count; h++)
							{
								float ofs = 0;
								float _hf = h + 1.f;
								float hf = h + 1.f + (float)sin(h*3123122.0)*h_detuneI;
								float ha = 1;

								if (h > 0) ha -= lerp(h_amp1, h_amp2, s);
								ha = clamp(ha, 0, 1);
								la += (float)sin(f*hf + ofs)*ha;
								ch += hl;
							}
							break;
						}
						case 1://triangle
						{
							for (int h = 0; h < h_count; h++)
							{
								float ofs = 0;
								float _hf = h + 1.f;
								float hf = h + 1.f + (float)sin(h*3123122.0)*h_detuneI;
								float ha = 1;

									ha = 1.f / (_hf*_hf);
									float m = 1.0f - h % 2;
									ha *= m;
									ofs = m*3.14159265359f / 2.0f;

								if (h > 0) ha -= lerp(h_amp1, h_amp2, s);
								ha = clamp(ha, 0, 1);
								la += (float)sin(f*hf + ofs)*ha;
								ch += hl;
							}
							break;
						}
						case 2://saw
						{
							for (int h = 0; h < h_count; h++)
							{
								float ofs = 0;
								float _hf = h + 1.f;
								float hf = h + 1.f + (float)sin(h*3123122.0)*h_detuneI;
								float ha = 1;

									ha = 1.f / _hf;

								if (h > 0) ha -= lerp(h_amp1, h_amp2, s);
								ha = clamp(ha, 0, 1);
								la += (float)sin(f*hf + ofs)*ha;
								ch += hl;
							}
							break;
						}
						case 3://square
						{
							for (int h = 0; h < h_count; h++)
							{
								float ofs = 0;
								float _hf = h + 1.f;
								float hf = h + 1.f + (float)sin(h*3123122.0)*h_detuneI;
								float ha = 1;

									ha = 1.f / _hf;
									float m = 1.0f - h % 2;
									ha *= m;

								if (h > 0) ha -= lerp(h_amp1, h_amp2, s);
								ha = clamp(ha, 0, 1);
								la += (float)sin(f*hf + ofs)*ha;
								ch += hl;
							}
							break;
						}
						case 4://noise
						{
							break;
						}
					}

					float cf_amp = amp;
					if (prev_amp[currentChannel]!=amp)
					{
						cf_amp = lerp(prev_amp[currentChannel], amp, clamp(s*20, 0, 1));
					}
				
					la *= 32767.0f*lerp(cf_amp, amp2, s) ;
				la = clamp(la, -32767.0, 32767.0);
				short value = (short)la;

				f+= 2.0 * 3.14159265359 * Freq / 44100.0;

			//	signed short Tleft  = *(signed short*)(sound::channel[currentChannel] + (i + start * 2) * 2);
			//	signed short Tright = *(signed short*)(sound::channel[currentChannel] + (i + start * 2) * 2 + 2);
				//Tleft = 0; Tright = 0;

				*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2) = value;
					*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2) = value;
			}

			if (playmode == 1|| prerender == 1 )
				sum[currentChannel] = f;

			prev_amp[currentChannel] = amp2;

	ResetEnv();
	}

	void Dynamics()
	{
		if (currentChannel <0) return;
		if (inClip[currentChannel] == false) return;

	//	if (channelOn[currentChannel] == false) return;

		if (cursorM < 0) return;
		int startofs = 0;
		if (master == true) startofs = framelen*cursorM * 4;

		int start = cursorM * 44100 / 60;
		int startI = 0;

		BYTE preamp = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		float preamp1 = EnvOp(preamp / 255.0f, temporalParams[currentChannel][0] / 255.0f, temporalParamsOp[currentChannel][0]);
		float preamp2 = EnvOp(preamp / 255.0f, temporalParams2[currentChannel][0] / 255.0f, temporalParamsOp[currentChannel][0]);

		BYTE offset = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		float offset1 = EnvOp(offset / 255.0f, temporalParams[currentChannel][1] / 255.0f, temporalParamsOp[currentChannel][1]);
		float offset2 = EnvOp(offset / 255.0f, temporalParams2[currentChannel][1] / 255.0f, temporalParamsOp[currentChannel][1]);

		signed short hardness = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		float hardness1 = EnvOp(hardness / 255.0f, temporalParams[currentChannel][2] / 255.0f, temporalParamsOp[currentChannel][2]);
		float hardness2 = EnvOp(hardness / 255.0f, temporalParams2[currentChannel][2] / 255.0f, temporalParamsOp[currentChannel][2]);

		BYTE out = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		float out1 = EnvOp(out / 255.0f, temporalParams[currentChannel][3] / 255.0f, temporalParamsOp[currentChannel][3]);
		float out2 = EnvOp(out / 255.0f, temporalParams2[currentChannel][3] / 255.0f, temporalParamsOp[currentChannel][3]);

		BYTE mix = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);
		float mix1 = EnvOp(mix / 255.0f, temporalParams[currentChannel][4] / 255.0f, temporalParamsOp[currentChannel][4]);
		float mix2 = EnvOp(mix / 255.0f, temporalParams2[currentChannel][4] / 255.0f, temporalParamsOp[currentChannel][4]);

		for (int i = 0; i < framelen * 2; i += 2)
		{
			float s = float(i / 2) / (float)framelen;
			float preampI = lerp(preamp1, preamp2, s);
			float offsetI = lerp(offset1, offset2, s);
			float hardnessI = lerp(hardness1, hardness2, s);
			float outI = lerp(out1, out2, s);
			float mixI = lerp(mix1, mix2, s);

			signed short left = *(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + startofs);
			signed short right = *(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2 + startofs);

			float _left = (float)(left*(float)(1.f+8.f*preampI))/32767.0f;
			_left += offsetI;
			float lp =(float) pow(fabs(_left), 1.0f + hardnessI);
			if (left < 0) lp *= -1.0f;
			_left -= offsetI;
			_left = lerp((float)sin(lp*3.14f / 2.0f),lp,mixI);
			_left = clamp(_left, -1, 1);
			_left*= 32767.0f*outI;
			
			float _right = (float)(right*(float)(1. + 8.*preampI)) / 32767.0f;
			_right += offsetI;
			lp = (float)pow(fabs(_right), 1.0 + hardnessI);
			if (right < 0) lp *= -1.0;
			_right -= offsetI;
			_right = lerp((float)sin(lp*3.14f / 2.0f), lp, mixI);
			_right = clamp(_right, -1, 1);
			_right *= 32767.0f*outI;

			left = (signed short)_left;
			right = (signed short)_right;

			*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + startofs) = left;
			*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2 + startofs) = right;
		}

		ResetEnv();
	}

	void Echo()
	{
		if (currentChannel <0) return;
		if (inClip[currentChannel] == false) return;

		if (cursorM < 0) return;
		int startofs = 0;
		if (master == true) startofs = framelen*cursorM * 4;


	//

	//	int start = cursorM * 44100 / 60;
		int startI = 0;

	//	BYTE target = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);

	//	if (target < 0|| target >=MAXCHANNELS) return;

		BYTE leftamp = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		BYTE rightamp = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		short int _leftofs = *(short int*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		short int _rightofs = *(short int*)(currentPos + CmdDesc[currentCmd].ParamOffset[4]);

		BYTE _rAmp = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[5]);
		short int _rSpeed = *(short int*)(currentPos + CmdDesc[currentCmd].ParamOffset[6]);
		float rAmp = _rAmp / 255.0f;
		float rSpeed = _rSpeed*100.f;

		int leftofs = _leftofs;// *framelen;
		int rightofs = _rightofs;// *framelen;

		int fl = framelen * 2 * 2;


		for (int i = 0; i < leftofs * 4; i += 2)
		{
			signed short left = *(signed short*)(sound::channel[currentChannel] + i + startofs);
			signed short left2 = *(signed short*)(sound::channel[currentChannel] + i+fl + startofs);
			int _left = (int)clamp((float)(left + left2), -32767.f, 32767.f);
			*(signed short*)(sound::channel[currentChannel] + i + startofs) = (signed short)_left;;
			*(signed short*)(sound::channel[currentChannel] + i+fl + startofs) = (signed short)0;
		}

		for (int i = 0; i < fl; i += 4)
		{
			int ofs = (int)(leftofs + leftofs*sin(i / (float)fl*3.1415f*2.0f * rSpeed)*rAmp);
			ofs *= 4;

			signed short left = *(signed short*)(sound::channel[currentChannel] + i + startofs);
			left = (signed short)((left*(float)leftamp) / 255.0f);
			signed short left2 = *(signed short*)(sound::channel[currentChannel] + i + ofs + startofs);
			int _left =(int) clamp((float)(left + left2), -32767, 32767);
			*(signed short*)(sound::channel[currentChannel] + i+ofs + startofs) = _left;


			ofs = rightofs + (int)(rightofs*sin(i / (float)fl*3.1415f*2.0f * rSpeed)*rAmp);
			ofs *= 4;

			signed short right = *(signed short*)(sound::channel[currentChannel] + i+2 + startofs);
			right = (signed short)((right*(float)rightamp) / 255.0f);
			signed short right2 = *(signed short*)(sound::channel[currentChannel] + i + ofs+2 + startofs);
			int _right = (int)(clamp((float)(right + right2), -32767.f, 32767.f));
			*(signed short*)(sound::channel[currentChannel] + i + ofs+2 + startofs) = _right;

		}



	//	ResetEnv();*/
	}

#define M_PI		3.14159265358979323846f
#define M_LN2	   0.69314718055994530942f
	enum {
		LPF, /* low pass filter */
		HPF, /* High pass filter */
		BPF, /* band pass filter */
		NOTCH, /* Notch Filter */
		PEQ, /* Peaking band EQ filter */
		LSH, /* Low shelf filter */
		HSH /* High shelf filter */
	};

	void Equalizer()
	{
		if (currentChannel <0) return;
		if (inClip[currentChannel] == false) return;
		if (cursorM < 0) return;

		int startofs = 0;
		if (master==true) startofs = framelen*cursorM * 4;

		BYTE type = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[0]);

		signed short freq = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		float freqI = EnvOp(freq , temporalParams[currentChannel][1] , temporalParamsOp[currentChannel][1]);

		signed short gain= *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		float gainI = EnvOp(gain, temporalParams[currentChannel][2], temporalParamsOp[currentChannel][2]);

		signed short width = *(signed short*)(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		float widthI = EnvOp(width, temporalParams[currentChannel][3], temporalParamsOp[currentChannel][3]);

		int loopBufferOfs = CmdDesc[currentCmd].ParamOffset[3] + 2;
		int fl = framelen * 4;// +freq * 4;

		float _width = (widthI+255.0f)/255.0f;
		if (_width < .01f) _width = .01f;

		float a0, a1, a2, b0, b1, b2;

		float Fs = 44100;      							   
		float A = (float)pow(10.0, gainI / 40.0);
		float omega = 2 * M_PI * freqI / Fs;
		float sn = (float)sin(omega);
		float cs = (float)cos(omega);
		float alpha = (float)(sn * sinh(M_LN2 / 2.f * _width * omega / sn));
		float beta = (float)sqrt(A + A);

		switch (type) {
		case LPF:
			b0 = (1 - cs) / 2;
			b1 = 1 - cs;
			b2 = (1 - cs) / 2;
			a0 = 1 + alpha;
			a1 = -2 * cs;
			a2 = 1 - alpha;
			break;
		case HPF:
			b0 = (1 + cs) / 2;
			b1 = -(1 + cs);
			b2 = (1 + cs) / 2;
			a0 = 1 + alpha;
			a1 = -2 * cs;
			a2 = 1 - alpha;
			break;
		case BPF:
			b0 = alpha;
			b1 = 0;
			b2 = -alpha;
			a0 = 1 + alpha;
			a1 = -2 * cs;
			a2 = 1 - alpha;
			break;
		case NOTCH:
			b0 = 1;
			b1 = -2 * cs;
			b2 = 1;
			a0 = 1 + alpha;
			a1 = -2 * cs;
			a2 = 1 - alpha;
			break;
		case PEQ:
			b0 = 1 + (alpha * A);
			b1 = -2 * cs;
			b2 = 1 - (alpha * A);
			a0 = 1 + (alpha / A);
			a1 = -2 * cs;
			a2 = 1 - (alpha / A);
			break;
		}

		float BufferL[44100/60+4*2];     
		float BufferR[44100/60+4*2];

		int pre = 2;

		BufferL[0] = *(signed short*)(currentPos + loopBufferOfs + 0);
		BufferL[1] = *(signed short*)(currentPos + loopBufferOfs + 2);

		BufferR[0] = *(signed short*)(currentPos + loopBufferOfs + 4);
		BufferR[1] = *(signed short*)(currentPos + loopBufferOfs + 6);

		//copy 
		{
			for (int i = 0; i < fl; i += 4)
			{
				signed short left = *(signed short*)(sound::channel[currentChannel] + i +startofs);
				BufferL[i/4+pre] = left;
				signed short right = *(signed short*)(sound::channel[currentChannel] + i+2+startofs);
				BufferR[i / 4 + pre] = right;

			}
		}

		int i = pre;
		int n = 44100/60+pre;

		float PrevSampleL[3];        /* this array holds 3 elements numbered 0 through 2 */
		float PrevSampleR[3];        /* this array holds 3 elements numbered 0 through 2 */
		PrevSampleL[0] = BufferL[1];
		PrevSampleL[1] = BufferL[0];

		PrevSampleR[0] = BufferR[1];
		PrevSampleR[1] = BufferR[0];

		while (i < n) {       

			PrevSampleL[2] = PrevSampleL[1];  
			PrevSampleL[1] = PrevSampleL[0];
			PrevSampleL[0] = BufferL[i];

			BufferL[i] = clamp((b0 / a0 * PrevSampleL[0]) +
				(b1 / a0 * PrevSampleL[1]) +
				(b2 / a0 * PrevSampleL[2]) -
				(a1 / a0 * BufferL[i - 1]) -
				(a2 / a0 * BufferL[i - 2]),-32767,32767);
				
			PrevSampleR[2] = PrevSampleR[1];
			PrevSampleR[1] = PrevSampleR[0];
			PrevSampleR[0] = BufferR[i];

			BufferR[i] = clamp((b0 / a0 * PrevSampleR[0]) +
				(b1 / a0 * PrevSampleR[1]) +
				(b2 / a0 * PrevSampleR[2]) -
				(a1 / a0 * BufferR[i - 1]) -
				(a2 / a0 * BufferR[i - 2]), -32767, 32767);
			i++;
		}

		//paste 
		{
			for (int i = 0; i < fl; i += 4)
			{
				signed short left = (signed short)BufferL[i/4+pre];
				signed short right = (signed short)BufferR[i / 4 + pre];
				*(signed short*)(sound::channel[currentChannel] + i+startofs) = left;
				*(signed short*)(sound::channel[currentChannel] + i+2+startofs) = right;
			}
		}

		*(signed short*)(currentPos + loopBufferOfs) = (signed short)BufferL[44100 / 60];
		*(signed short*)(currentPos + loopBufferOfs+2) = (signed short)BufferL[44100 / 60+1];
		*(signed short*)(currentPos + loopBufferOfs+4) = (signed short)BufferR[44100 / 60];
		*(signed short*)(currentPos + loopBufferOfs + 6) = (signed short)BufferR[44100 / 60 + 1];


			ResetEnv();
	}

	

	void Mixer()
	{

		int start = cursorM * 44100 / 60;
		int startI = 0;

		if (start < 0) return;

		master = true;

		int channelcount = currentChannel+1;

		int outchannel = MAXCHANNELS - 1;
		currentChannel = outchannel;
		inClip[currentChannel] = true;
		for (int i = 0; i < framelen * 2; i += 2)
		{
			double a = 0;
			double b = 0;
			for (int x = 0; x < min(channelcount,MAXCHANNELS-1); x++)
			{
				signed short _a = *(signed short*)(sound::channel[x] + (i + startI * 2) * 2);
				signed short _b = *(signed short*)(sound::channel[x] + (i + startI * 2) * 2+2);

				float slide = (float)(((i / 2 + (loopedCursor[x] - noteLeft[x])*44100 / 60)) / ((noteSize[x])*44100 / 60));
				float slidespeed = (255.0f - noteSlide[x]);
				slidespeed = clamp(slidespeed, 0, 1);
				//slide *= slidespeed;
				//slide = slide * slidespeed - slidespeed + 1;
				slide = clamp(slide, 0, 1);

				float pan = lerp(notePan[x], nextNotePan[x], slide) ;
				//pan = notePan[x] / 255.0;

				float scf = (-channelPan[x]-pan+127.0f) / 255.0f;
				float vlm = channelVolume[x] / 255.0f;

				float velocity = lerp(noteVelocity[x], nextNoteVelocity[x], slide) / 255.0f;
				//velocity = noteVelocity[x] / 255.0;

				vlm *= velocity;

				if (inClip[x] == false) vlm = 0;

				a += (double)_a * scf*vlm;
				b += (double)_b * (1.-scf)*vlm;


			}
			a = clamp_d(a,-32767, 32767);
			b = clamp_d(b, -32767, 32767);
			*(signed short*)(sound::channel[outchannel] + (i + start * 2)*2)= (signed short)a;
			*(signed short*)(sound::channel[outchannel] + (i + start * 2) * 2+2) = (signed short)b;
		}
	}

	void Solo()
	{

	}

	void Mute()
	{

	}

	//-----------------------------------------------
	//-----------------------------------------------
	//-----------------------------------------------

	void CreateShader()
	{
		BYTE slot = shaderSlotCounter;
		shaderSlotCounter++;
	//	BYTE slot = *(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		BYTE* shaderText = currentPos + CmdDesc[currentCmd].ParamOffset[2];
		dx::CompileShader(slot, (char*)shaderText, (char*)shaderText);
	}

	void CreateVB()
	{
		BYTE slot = *(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		INT32 size = *(currentPos + CmdDesc[currentCmd].ParamOffset[3]);
		dx::CreateVB(slot,size);
	}


	void SetRT()
	{
		BYTE n= *(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		//g_pImmediateContext->OMSetRenderTargets(1, &g_pRenderTargetView, g_pDepthStencilView);
		dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::RenderTargetView[n], 0);
		dx::currentRTn = n;

		D3D11_VIEWPORT vp;
		vp.Width = (FLOAT)dx::Tsize[n].x;
		vp.Height = (FLOAT)dx::Tsize[n].y;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		dx::g_pImmediateContext->RSSetViewports(1, &vp);
	}


	void ResetRT()
	{
		

		D3D11_VIEWPORT vp;
		vp.Width = (FLOAT)dx::width;
		vp.Height = (FLOAT)dx::height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		dx::g_pImmediateContext->RSSetViewports(1, &vp);

		//dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::g_pRenderTargetView, dx::g_pDepthStencilView);
		dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::g_pRenderTargetView, 0);

		dx::g_pImmediateContext->GenerateMips(dx::TextureResView[dx::currentRTn]);

		dx::currentRTn = -1;

	}

	void SetZBuffer()
	{
		BYTE mode = *(currentPos + CmdDesc[currentCmd].ParamOffset[0]);
		dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::g_pRenderTargetView, dx::g_pDepthStencilView);

		dx::g_pImmediateContext->OMSetDepthStencilState(dx::pDSState[mode], 1);

		if (mode==0) dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::g_pRenderTargetView, 0);

	}

	int tempTexCounter = 0;

	void CreateTexture()
	{
		if (dx::pTexture[tempTexCounter]) dx::pTexture[tempTexCounter]->Release();
		if (dx::TextureResView[tempTexCounter]) dx::TextureResView[tempTexCounter]->Release();
		if (dx::RenderTargetView[tempTexCounter]) dx::RenderTargetView[tempTexCounter]->Release();

		int mip = 0;
		BYTE ensize = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[1]);
		int size = (int)pow(2, ensize + 5);
		dx::Tsize[tempTexCounter].x = size;
		dx::Tsize[tempTexCounter].y = size;

		//ID3D11Texture2D *tex;
		D3D11_TEXTURE2D_DESC tdesc;
		D3D11_SUBRESOURCE_DATA tbsd;

		tbsd.pSysMem = (void *)dx::TextureTempMem;
		tbsd.SysMemPitch = size * 4;
		tbsd.SysMemSlicePitch = size * size * 4; 

		tdesc.Width = size;
		tdesc.Height = size;
		tdesc.MipLevels = 0;
		tdesc.ArraySize = 1;
		tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		tdesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

		tdesc.CPUAccessFlags = 0;

		tdesc.SampleDesc.Count = 1;
		tdesc.SampleDesc.Quality = 0;
		tdesc.Usage = D3D11_USAGE_DEFAULT;

		BYTE format = *(BYTE*)(currentPos + CmdDesc[currentCmd].ParamOffset[2]);
		switch (format)
		{
			case 0:
				tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				break;
			case 1:
				tdesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
				break;
			case 2:
				tdesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
				break;

		}

		dx::TFormat[tempTexCounter] = format;


		

		HRESULT h=dx::g_pd3dDevice->CreateTexture2D(&tdesc, NULL, &dx::pTexture[tempTexCounter]);


		D3D11_SHADER_RESOURCE_VIEW_DESC svDesc;

		svDesc.Format = tdesc.Format;
		svDesc.Texture2D.MipLevels = -1;
		svDesc.Texture2D.MostDetailedMip = 0;
		svDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

		h = dx::g_pd3dDevice->CreateShaderResourceView(dx::pTexture[tempTexCounter], &svDesc, &dx::TextureResView[tempTexCounter]);

		D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;

		renderTargetViewDesc.Format = tdesc.Format;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;

		// Create the render target view.
		HRESULT result = dx::g_pd3dDevice->CreateRenderTargetView(dx::pTexture[tempTexCounter], &renderTargetViewDesc, &dx::RenderTargetView[tempTexCounter]);
	
		tempTexCounter++;

	}

	//-------------------------------------
	//       execute command list
 
	int ccounter;
	void Render()
	{
		ccounter = 0; int precmds = 0;
		if (loopPoint == stack::data)
		{
			currentPos = loopPoint;
			while (*currentPos != (BYTE)0 && CmdDesc[*currentPos].Routine != EndDraw && CmdDesc[*currentPos].Routine != MainLoop)
			{
				currentCmd = *currentPos;
				if (CmdDesc[currentCmd].Routine == ShowScene||
					CmdDesc[currentCmd].Routine == Scene ||
					CmdDesc[currentCmd].Routine == EndScene )
					CmdDesc[currentCmd].Routine();

				//if (CmdDesc[currentCmd].Routine==CreateShader) SimpleCommand.Render();

				currentPos += CmdDesc[*currentPos].Size;
				precmds++;
			}
		}

		dx::currentRTn = -1;
		tempTexCounter = 0;
		shaderSlotCounter = 0;

		if (timer::resetStartFlag) 
		{
			timer::StartTime = timer::GetCounter();
			timer::resetStartFlag = false;
		}

		currentChannel = -1;
		master = false;
		cursorF = playmode*(timer::prevtick - timer::StartTime) / (1.f / 60.f * 1000.f) + StartPosition;
		cursor = (int)floor(cursorF);
		cursorM = (int)(cursorF+preCount);

		if (timeLoopEnable)
		{
			cursorM = (cursorM - timeLoopStart) % (timeLoopLen)+timeLoopStart;
			cursorF = (int(cursorF) - timeLoopStart) % (timeLoopLen)+timeLoopStart;
			cursor = (int)floor(cursorF);
		}

		currentPos = loopPoint;

		//int counter = 0;
		

		while (*currentPos != (BYTE)0 &&CmdDesc[*currentPos].Routine!=EndDraw)
		{
			currentCmd = *currentPos;
			CmdDesc[currentCmd].Routine();

			//if (CmdDesc[currentCmd].Routine==CreateShader) SimpleCommand.Render();

			currentPos += CmdDesc[*currentPos].Size;
			
			//pbar;
			if (loopPoint == stack::data)
			{
				ccounter++;
				char s_progress[65];
				char status[65];
				strcpy(status, "Precalcing ");
				_itoa(100*ccounter / precmds, s_progress, 10);
				strcat(s_progress, "%");
				strcat(status, s_progress);
				//_itoa(ccounter , s_progress, 10);
				SetWindowText(hWnd, status);
				//Sleep(16);
			}

		}

		ResetRT();

		dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::g_pRenderTargetView, dx::g_pDepthStencilView);
	}

}