

namespace io
{
	char name[1000];	
	char startDir[1000];
	char undopath[1000];

	int UndoNum = -1;
	int UndoCursor = 0;
	int maxUndo = 0;

	#include <shellapi.h>

	void silently_remove_directory(LPCTSTR dir) // Fully qualified name of the directory being deleted, without trailing backslash
	{
		SHFILEOPSTRUCT file_op = {
			NULL,
			FO_DELETE,
			dir,
			"",
			FOF_NOCONFIRMATION |
			FOF_NOERRORUI |
			FOF_SILENT,
			false,
			0,
			"" };
		SHFileOperation(&file_op);
	}

	void SelfLocate()
	{
		char *i;
		char tmpstr[1000];
		
		strcpy(tmpstr, GetCommandLine());
		//strcpy(cdir2, GetCommandLine());//������ ��� ����������� � �������
		i = strstr(tmpstr, ".exe"); while (*(i) != '\\') i--; *(i + 1) = 0;//�������� ������
		strcpy(startDir, tmpstr + 1);

		strcpy(undopath, tmpstr + 1);
		strcat(undopath, "undo\\");
		SetCurrentDirectory(startDir);
		silently_remove_directory(undopath);
		CreateDirectory(undopath, NULL);

	}


	void ShowFilename()
	{
		int j;
		for (j = strlen(name) - 1; j >= 0 && name[j] != '\\'; j--) {}
		SetWindowText(hWnd, name + j + 1);
	}

	void Load()
	{
		stack::PrecalcStatus = 0; stack::LoopPoint = 0;
		
		FILE *fp;

		OPENFILENAME ofn;
		char szFile[1000] = "\0";
		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "fx project\0*.fxp\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		// Display the Open dialog box. 
		if (GetOpenFileName(&ofn) == TRUE)
		{
			ZeroMemory(stack::data, MAXDATASIZE);

			strcpy(name, ofn.lpstrFile);
			fp = fopen(ofn.lpstrFile, "rb");
			fread(stack::data, 1, MAXDATASIZE, fp);
			fclose(fp);
			strcpy(name, ofn.lpstrFile);
			ShowFilename();	
		}

		Commands::loopPoint = stack::data;
		
	}

	void LoadTemplate()
	{
		if (*startDir!=0) SetCurrentDirectory(startDir);
		FILE *fp;
		fp = fopen("template.fxp", "rb");
		if (fp)
		{
			fread(stack::data, 1, MAXDATASIZE, fp);
			fclose(fp);
			strcpy(name, "untitled.fxp");
			ShowFilename();
		}
	}

	int SearchStackEnd(int cnt)
	{
		while (*(stack::data + cnt) != 0) 
		{
			cnt += CmdDesc[*(stack::data + cnt)].Size;
		}
		return cnt;
	}



	void SaveAs()
	{
		int cnt = 0;
		cnt = SearchStackEnd(cnt)+1;

		FILE *fp;
		OPENFILENAME ofn;
		char szFile[1000] = "\0";

		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "fx project\0*.fxp\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;

		if (GetSaveFileName(&ofn) == TRUE)
		{
			strcpy(name, ofn.lpstrFile);
			if (NULL == strstr(ofn.lpstrFile, ".fxp")) strcat(name, ".fxp");

			fp = fopen(name, "wb");
			fwrite(stack::data, cnt, 1, fp);
			fclose(fp);
			ShowFilename();
		}
	
	}

	void Save()
	{
		if (strlen(name)<1) 
		{
			SaveAs(); 
			return; 
		}

		int cnt = 0;
		cnt = SearchStackEnd(cnt)+1;

		FILE *fp;
		fp = fopen(name, "wb");
		fwrite(stack::data, cnt, 1, fp);
		fclose(fp);
	}

	void WriteUndoStage()
	{
		int cnt = SearchStackEnd(0)+1;
		FILE *fp;
		char name[100];
		char aa[100];
		UndoNum++;
		_itoa(UndoNum, aa, 10);
		strcpy(name, undopath);
		strcat(name, aa);
		strcat(name, ".fxp");

		fp = fopen(name, "wb");
		fwrite(stack::data, cnt, 1, fp);
		fclose(fp);

		maxUndo = UndoNum;
	}

	void Undo()
	{
		char name[1000];

		if (UndoNum > 0)
		{
			UndoNum--;
			FILE *fp;

			char aa[100];
			_itoa(UndoNum, aa, 10);
			strcpy(name, undopath);
			strcat(name, aa);
			strcat(name, ".fxp");

			fp = fopen(name, "rb");
			fread(stack::data, 1, MAXDATASIZE, fp);
			fclose(fp);
		}
		
	}

	void Redo()
	{
		if (UndoNum >= maxUndo) return;

		UndoNum++;
		char name[1000];

		FILE *fp;
		char aa[100];
		_itoa(UndoNum, aa, 10);
		strcpy(name, undopath);
		strcat(name, aa);
		strcat(name, ".fxp");

		fp = fopen(name, "rb");
		fread(stack::data, 1, MAXDATASIZE, fp);
		fclose(fp);

	}
}