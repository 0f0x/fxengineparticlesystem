using namespace dx;

namespace dxui {
	//
	typedef struct {
		ID3D11VertexShader*     pVs;
		ID3D11PixelShader*      pPs;
		ID3DBlob*				pVSBlob;
		ID3DBlob*				pPSBlob;
	} uishader;

	uishader BoxShader;
	uishader TextShader;
	uishader TimelineShader;
	uishader TexViewShader;

	char base_vs[] = \
		"Texture2D tex0 : register(t0);"
		"cbuffer ConstantBuffer : register(b0)"
		"{"
		"	float4 View;"
		"	float4 Pos;"
		"	float4 sSize;"
		"	float4 uvScale;"
		"	float4 uvOffset;"
		"};"
		"struct VS_INPUT"
		"{"
		"	float4 Pos : POSITION;"
		"	float2 uv : TEXCOORD0;"
		"};"
		"struct VS_OUTPUT"
		"{"
		"	float4 Pos : SV_POSITION;"
		"	float2 uv : TEXCOORD0;"
		"};"
		"VS_OUTPUT VS(VS_INPUT input)"
		"{"
		"	VS_OUTPUT output= (VS_OUTPUT)0;"
		"float2 p=input.Pos.xy*sSize.xy+Pos.xy;"
		"	output.Pos =float4(p,0,1);"
		"	output.uv = input.uv*uvScale.x+uvOffset.xy;"
		"	return output;"
		"}";

	char base_ps[] = \
		"cbuffer ConstantBuffer : register(b1)"
		"{"
		"	float4 Color;"
		"};"
		"Texture2D tex0 : register(t0);"
		"SamplerState samLinear : register(s0);"
		"struct VS_OUTPUT"
		"{"
		"	float4 Pos : SV_POSITION;"
		"	float2 uv : TEXCOORD0;"
		"};"
		"float4 PS(VS_OUTPUT input) : SV_Target"
		"{"
		"	return float4(Color.rgb,tex0.Sample( samLinear, input.uv ).a)*Color;"
		"}";

	char texview_ps[] = \
		"cbuffer ConstantBuffer : register(b1)"
		"{"
		"	float4 Color;"
		"};"
		"Texture2D tex0 : register(t0);"
		"SamplerState samLinear : register(s0);"
		"struct VS_OUTPUT"
		"{"
		"	float4 Pos : SV_POSITION;"
		"	float2 uv : TEXCOORD0;"
		"};"
		"float4 PS(VS_OUTPUT input) : SV_Target"
		"{"
		"	return float4(tex0.Sample( samLinear, input.uv ).rgb,1);"
		"}";


	char box_ps[] = \
		"cbuffer ConstantBuffer : register(b1)"
		"{"
		"	float4 Color;"
		"	float4 Aspect;"
		"	float4 GradientColor[5];"
		"	float4 GradientPos[5];"
		"	float4 PointCount;"
		"};"
		"Texture2D tex0 : register(t0);"
		"SamplerState samLinear : register(s0);"
		"float4 interpolator4(in float4 color[5], in float4 coords[5], in float i, in float points)"
		"{"
		"int leftindex = 0;"
		"float w = 0;"
		"for (int n = 0; n<points; n++)"
		"{"
		"if (i>coords[n].x && i - coords[n].x>0)"
		"{"
		"leftindex = n;"
		"w = coords[min(n + 1, points - 1)].x - coords[n].x;"
		"}"
		"}"
		"float4 c = lerp(color[leftindex], color[min(leftindex + 1, points - 1)], (i - coords[leftindex].x) / w);"
		"return c;"
		"}"
		"struct VS_OUTPUT"
		"{"
		"	float4 Pos : SV_POSITION;"
		"	float2 uv : TEXCOORD0;"
		"};"
		"float4 PS(VS_OUTPUT input) : SV_Target"
		"{"
		//"	return float4(Color.xyz,212.*(1.-2.*abs(input.uv.x-.5))*(1.-2.*abs(input.uv.y-.5)));"
		 //"	return float4(Color.xyz-input.uv.y*.1,min(Color.w,saturate(cos((input.uv.y-.5)*3.14)*5.)));"
		"float r=Aspect.w;"
		"float aspect=Aspect.x;"
		"float2 uv2=input.uv-.5;"
		"uv2.x *= aspect.x;"
		"float l = length(max(abs(uv2) - .5*float2(aspect, 1.) + r*2., 0.0)) - r;"
		"float cr=0.0;"
		"if (l<r) cr = 1.;"
		"	float4 fcolor =saturate(float4(Color.xyz+4.*pow(-uv2.y,5.),Color.w*cr));"
		" float4 gradcolor = interpolator4(GradientColor,GradientPos,input.uv.x,PointCount.x);"
		" fcolor*=gradcolor;"
		//" fcolor*=lerp(gradcolor,float4(1,1,1,1),1-clamp(PointCount.x,0,1));"
		//" fcolor*=lerp(gradcolor,float4(1,1,1,1),1-clamp(PointCount.x,0,1));"
		//"float t=clamp(PointCount.x, 0.0, 1.0);"
		//"if (t==1)"
		//" float4 fcolor2=gradcolor;"
		"return fcolor;"
		"}";

	char tl_vs[] = \
		"cbuffer ConstantBuffer : register(b0)"
		"{"
		"	float4 View;"
		"	float4 Pos;"
		"	float4 sSize;"
		"};"
		"struct VS_INPUT"
		"{"
		"	float4 Pos : POSITION;"
		"};"
		"struct VS_OUTPUT"
		"{"
		"	float4 Pos : SV_POSITION;"
		"};"
		"VS_OUTPUT VS(VS_INPUT input)"
		"{"
		"	VS_OUTPUT output= (VS_OUTPUT)0;"
		//"float2 p=input.Pos.xy*sSize.xy+Pos.xy-View.xy;"
		"float2 p=input.Pos.xy*sSize+Pos.xy;"
		"	output.Pos =float4(p,0,1);"
		"	return output;"
		"}";

	char tl_ps[] = \
		"cbuffer ConstantBuffer : register(b1)"
		"{"
		"	float4 Color;"
		"};"
		"struct VS_OUTPUT"
		"{"
		"	float4 Pos : SV_POSITION;"
		"};"
		"float4 PS(VS_OUTPUT input) : SV_Target"
		"{"
		"	return float4(Color.rgb,1.0);"
		"}";


	struct SimpleVertex
	{
		XMFLOAT3 Pos;
		XMFLOAT2 Tex;
	};

	struct TimelineVertex
	{
		XMFLOAT3 Pos;
	};

	SimpleVertex vertices[6] =
	{
		XMFLOAT3(0,0,0),XMFLOAT2(0,0),
		XMFLOAT3(1,0,0),XMFLOAT2(1,0),
		XMFLOAT3(1,-1,0),XMFLOAT2(1,1),

		XMFLOAT3(0,0,0),XMFLOAT2(0,0),
		XMFLOAT3(1,-1,0),XMFLOAT2(1,1),
		XMFLOAT3(0,-1,0),XMFLOAT2(0,1)
	};

#define timedividercount 9*60
#define maxlines 60*60*9
	TimelineVertex TimelineVertices[timedividercount * 2 + 2];
	TimelineVertex lineVertices[maxlines * 2];
	TimelineVertex envelopeVertices[maxlines * 2];

	struct ConstantBufferV
	{
		XMFLOAT4 View;
		XMFLOAT4 Pos;
		XMFLOAT4 Size;
		XMFLOAT4 uvScale;
		XMFLOAT4 uvOffset;
	};


	struct ConstantBufferP
	{
		XMFLOAT4 Color;
		XMFLOAT4 Aspect;
		XMFLOAT4 GradientColor[5];
		XMFLOAT4 GradientPos[5];
		XMFLOAT4 GradientPointCount;
	};

	struct ConstantBufferVTL
	{
		XMFLOAT4 View;
		XMFLOAT4 Pos;
		XMFLOAT4 Size;
	};

	struct ConstantBufferPTL
	{
		XMFLOAT4 Color;
	};

	struct ConstantBufferVL
	{
		XMFLOAT4 View;
		XMFLOAT4 Pos;
		XMFLOAT4 Size;
	};

	struct ConstantBufferPL
	{
		XMFLOAT4 Color;
	};

	ID3D11InputLayout*      g_pVertexLayout = NULL;
	ID3D11Buffer*			g_pVertexBuffer = NULL;
	ID3D11Buffer*           g_pConstantBufferV = NULL;
	ID3D11Buffer*           g_pConstantBufferP = NULL;

	ID3D11InputLayout*      g_pVertexLayoutTL = NULL;
	ID3D11Buffer*			g_pVertexBufferTL = NULL;

	ID3D11InputLayout*      g_pVertexLayoutL = NULL;
	ID3D11Buffer*			g_pVertexBufferL = NULL;
	ID3D11Buffer*           g_pConstantBufferVTL = NULL;
	ID3D11Buffer*           g_pConstantBufferPTL = NULL;
	ID3D11Buffer*           g_pConstantBufferVL = NULL;
	ID3D11Buffer*           g_pConstantBufferPL = NULL;

	ID3D11Texture2D *tex;
	ID3D11ShaderResourceView* Texture;
	ID3D11SamplerState*       g_pSamplerLinear = NULL;
	char* TX;


	// system font init
	BYTE* font;
	BYTE* UnpackedFont;
	char FontPTable[256];


	void SetTextureOptions()
	{
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = 0;// D3D11_FLOAT32_MAX;
		// ������� ��������� ������ ���������������
		dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &g_pSamplerLinear);

	}

	void VBSet()
	{
		UINT stride = sizeof(SimpleVertex);
		UINT offset = 0;
		dx::g_pImmediateContext->IASetVertexBuffers(0, 1, &g_pVertexBuffer, &stride, &offset);
		dx::g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}

	void TimelineVBSet()
	{
		UINT stride = sizeof(TimelineVertex);
		UINT offset = 0;
		dx::g_pImmediateContext->IASetVertexBuffers(0, 1, &g_pVertexBufferTL, &stride, &offset);
		dx::g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	}

	void lineVBSet()
	{
		UINT stride = sizeof(TimelineVertex);
		UINT offset = 0;
		dx::g_pImmediateContext->IASetVertexBuffers(0, 1, &g_pVertexBufferL, &stride, &offset);
		dx::g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	}

	void UpdateLineVb(int count)
	{
		D3D11_MAPPED_SUBRESOURCE resource;
		HRESULT h = dx::g_pImmediateContext->Map(g_pVertexBufferL, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
		memcpy(resource.pData, lineVertices, sizeof(TimelineVertex)*count * 2);
		dx::g_pImmediateContext->Unmap(g_pVertexBufferL, 0);

/*		D3D11_BOX box{};
		box.left = 0;
		box.right = sizeof(TimelineVertex)*count * 2;
		box.top = 0;
		box.bottom = 1;
		box.front = 0;
		box.back = 1;

		dx::g_pImmediateContext->UpdateSubresource(g_pVertexBufferL, 0, &box, (uint8_t*)(lineVertices), 0, 0);*/

	}

	void UpdateLineEnvelopeVb(int count)
	{
		D3D11_MAPPED_SUBRESOURCE resource;
		HRESULT h = dx::g_pImmediateContext->Map(g_pVertexBufferL, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
		memcpy(resource.pData, envelopeVertices, sizeof(TimelineVertex)*count * 2);
		dx::g_pImmediateContext->Unmap(g_pVertexBufferL, 0);

	/*	D3D11_BOX box{};
		box.left = 0;
		box.right = sizeof(TimelineVertex)*count * 2;
		box.top = 0;
		box.bottom = 1;
		box.front = 0;
		box.back = 1;

		dx::g_pImmediateContext->UpdateSubresource(g_pVertexBufferL, 0, &box, (uint8_t*)(envelopeVertices), 0, 0);*/
	}

	ID3D11BlendState* m_alphaEnableBlendingState;
	ID3D11BlendState* m_alphaDisableBlendingState;

	void SetBlendStates()
	{
		D3D11_BLEND_DESC bSDesc;
		ZeroMemory(&bSDesc, sizeof(D3D11_BLEND_DESC));
		bSDesc.RenderTarget[0].BlendEnable = TRUE;
		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		bSDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
		bSDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		g_pd3dDevice->CreateBlendState(&bSDesc, &m_alphaEnableBlendingState);

		bSDesc.RenderTarget[0].BlendEnable = FALSE;
		g_pd3dDevice->CreateBlendState(&bSDesc, &m_alphaDisableBlendingState);
	}

	void SetupDrawer()
	{
		VBSet();
		dx::g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pConstantBufferV);
		dx::g_pImmediateContext->PSSetConstantBuffers(1, 1, &g_pConstantBufferP);
		dx::g_pImmediateContext->VSSetShader(TextShader.pVs, NULL, 0);
		dx::g_pImmediateContext->PSSetShader(TextShader.pPs, NULL, 0);

		dx::g_pImmediateContext->PSSetShaderResources(0, 1, &Texture);
		dx::g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);

		float blendFactor[4];
		blendFactor[0] = 0.0f;
		blendFactor[1] = 0.0f;
		blendFactor[2] = 0.0f;
		blendFactor[3] = 0.0f;
		g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

		g_pImmediateContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
	}

	void SetupDrawerBox()
	{
		VBSet();
		dx::g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pConstantBufferV);
		dx::g_pImmediateContext->PSSetConstantBuffers(1, 1, &g_pConstantBufferP);
		dx::g_pImmediateContext->VSSetShader(BoxShader.pVs, NULL, 0);
		dx::g_pImmediateContext->PSSetShader(BoxShader.pPs, NULL, 0);

		dx::g_pImmediateContext->PSSetShaderResources(0, 1, &Texture);
		dx::g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);

		float blendFactor[4];
		blendFactor[0] = 0.0f;
		blendFactor[1] = 0.0f;
		blendFactor[2] = 0.0f;
		blendFactor[3] = 0.0f;

		g_pImmediateContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
		g_pImmediateContext->IASetInputLayout(g_pVertexLayout);
		//	g_pImmediateContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
	}

	void SetupDrawerTextureView()
	{
		VBSet();
		dx::g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pConstantBufferV);
		dx::g_pImmediateContext->PSSetConstantBuffers(1, 1, &g_pConstantBufferP);
		dx::g_pImmediateContext->VSSetShader(BoxShader.pVs, NULL, 0);
		dx::g_pImmediateContext->PSSetShader(TexViewShader.pPs, NULL, 0);

		dx::g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);

		float blendFactor[4];
		blendFactor[0] = 0.0f;
		blendFactor[1] = 0.0f;
		blendFactor[2] = 0.0f;
		blendFactor[3] = 0.0f;

		g_pImmediateContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
		g_pImmediateContext->IASetInputLayout(g_pVertexLayout);
		//	g_pImmediateContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
	}

	void SetupDrawerTimeline()
	{
		TimelineVBSet();
		dx::g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pConstantBufferVTL);
		dx::g_pImmediateContext->PSSetConstantBuffers(1, 1, &g_pConstantBufferPTL);
		dx::g_pImmediateContext->VSSetShader(TimelineShader.pVs, NULL, 0);
		dx::g_pImmediateContext->PSSetShader(TimelineShader.pPs, NULL, 0);

		//		dx::g_pImmediateContext->PSSetShaderResources(0, 1, &Texture[0]);
			//	dx::g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);

		float blendFactor[4];
		blendFactor[0] = 0.0f;
		blendFactor[1] = 0.0f;
		blendFactor[2] = 0.0f;
		blendFactor[3] = 0.0f;

		g_pImmediateContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
		g_pImmediateContext->IASetInputLayout(g_pVertexLayoutTL);
		//	g_pImmediateContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
	}

	void SetupDrawerline()
	{
		dx::g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pConstantBufferVL);
		dx::g_pImmediateContext->PSSetConstantBuffers(1, 1, &g_pConstantBufferPL);
		dx::g_pImmediateContext->VSSetShader(TimelineShader.pVs, NULL, 0);
		dx::g_pImmediateContext->PSSetShader(TimelineShader.pPs, NULL, 0);

		float blendFactor[4];
		blendFactor[0] = 0.0f;
		blendFactor[1] = 0.0f;
		blendFactor[2] = 0.0f;
		blendFactor[3] = 0.0f;

		g_pImmediateContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
		g_pImmediateContext->IASetInputLayout(g_pVertexLayoutL);

	}

	void VBInit()
	{
		HRESULT hr = S_OK;

		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		UINT numElements = ARRAYSIZE(layout);

		hr = dx::g_pd3dDevice->CreateInputLayout(layout, numElements, TextShader.pVSBlob->GetBufferPointer(), TextShader.pVSBlob->GetBufferSize(), &g_pVertexLayout);
		TextShader.pVSBlob->Release();

		g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(SimpleVertex) * 6;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = vertices;
		hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &g_pVertexBuffer);

	}

	void lineVBInit()
	{
		float step = .01f;
		float h = -.99f;


		HRESULT hr = S_OK;

		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		UINT numElements = ARRAYSIZE(layout);

		hr = dx::g_pd3dDevice->CreateInputLayout(layout, numElements, TimelineShader.pVSBlob->GetBufferPointer(), TimelineShader.pVSBlob->GetBufferSize(), &g_pVertexLayoutL);
		//	TimelineShader.pVSBlob->Release();

		g_pImmediateContext->IASetInputLayout(g_pVertexLayoutL);

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(TimelineVertex) * maxlines * 2;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = lineVertices;
		hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &g_pVertexBufferL);

	}

	void TimelineVBInit()
	{
		float step = .01f;
		float h = -.99f;

		for (int t = 0; t < timedividercount; t++)
		{

			TimelineVertices[t * 2].Pos.x = t*step;
			TimelineVertices[t * 2].Pos.y = h;
			TimelineVertices[t * 2 + 1].Pos.x = t*step;
			TimelineVertices[t * 2 + 1].Pos.y = h + .007f;

			if (t % 10 == 0)
				TimelineVertices[t * 2 + 1].Pos.y = h + .02f;

			if (t % 60 == 0)
				TimelineVertices[t * 2 + 1].Pos.y = h + .03f;

		}

		TimelineVertices[timedividercount * 2 - 2].Pos.x = 0;
		TimelineVertices[timedividercount * 2 - 2].Pos.y = h;
		TimelineVertices[timedividercount * 2 - 1].Pos.x = 10;
		TimelineVertices[timedividercount * 2 - 1].Pos.y = h;

		HRESULT hr = S_OK;

		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		UINT numElements = ARRAYSIZE(layout);

		hr = dx::g_pd3dDevice->CreateInputLayout(layout, numElements, TimelineShader.pVSBlob->GetBufferPointer(), TimelineShader.pVSBlob->GetBufferSize(), &g_pVertexLayoutTL);
		//	TimelineShader.pVSBlob->Release();

		g_pImmediateContext->IASetInputLayout(g_pVertexLayoutTL);

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(TimelineVertex) * timedividercount * 2;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = TimelineVertices;
		hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &g_pVertexBufferTL);

	}

	void CreateTexture(ID3D11ShaderResourceView** resView, char* src, int size)
	{

		//g_pd3dDevice->CreateTexture2D(

		//	ID3D11Texture2D *MakeCheckerboard(ID3D11Device *myDevice, int w, int h)
		//	{
		ID3D11Texture2D *tex;
		D3D11_TEXTURE2D_DESC tdesc;
		D3D11_SUBRESOURCE_DATA tbsd;


		tbsd.pSysMem = (void *)src;
		tbsd.SysMemPitch = size * 4;
		tbsd.SysMemSlicePitch = size*size * 4; // Not needed since this is a 2d texture

		tdesc.Width = size;
		tdesc.Height = size;
		tdesc.MipLevels = 1;
		tdesc.ArraySize = 1;

		tdesc.SampleDesc.Count = 1;
		tdesc.SampleDesc.Quality = 0;
		tdesc.Usage = D3D11_USAGE_DEFAULT;
		tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

		tdesc.CPUAccessFlags = 0;
		tdesc.MiscFlags = 0;

		g_pd3dDevice->CreateTexture2D(&tdesc, &tbsd, &tex);


		D3D11_SHADER_RESOURCE_VIEW_DESC svDesc;

		svDesc.Format = tdesc.Format;
		svDesc.Texture2D.MipLevels = 1;
		svDesc.Texture2D.MostDetailedMip = 0;
		svDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

		g_pd3dDevice->CreateShaderResourceView(tex, &svDesc, resView);

		tex->Release();
	}

	int roundUp(int n, int r)
	{
		return 	n - (n % r) + r;
	}

	void CreateConstBuf()
	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = roundUp(sizeof(ConstantBufferV), 16);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.StructureByteStride = 16;

		HRESULT hr = g_pd3dDevice->CreateBuffer(&bd, NULL, &g_pConstantBufferV);

		bd.ByteWidth = roundUp(sizeof(ConstantBufferP), 16);
		HRESULT hr2 = g_pd3dDevice->CreateBuffer(&bd, NULL, &g_pConstantBufferP);
	}

	void CreateConstBufTL()
	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = roundUp(sizeof(ConstantBufferVTL), 16);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.StructureByteStride = 16;

		HRESULT hr = g_pd3dDevice->CreateBuffer(&bd, NULL, &g_pConstantBufferVTL);

		bd.ByteWidth = roundUp(sizeof(ConstantBufferPTL), 16);
		HRESULT hr2 = g_pd3dDevice->CreateBuffer(&bd, NULL, &g_pConstantBufferPTL);
	}

	void CreateConstBufL()
	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = roundUp(sizeof(ConstantBufferVL), 16);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.StructureByteStride = 16;

		HRESULT hr = g_pd3dDevice->CreateBuffer(&bd, NULL, &g_pConstantBufferVL);

		bd.ByteWidth = roundUp(sizeof(ConstantBufferPL), 16);
		HRESULT hr2 = g_pd3dDevice->CreateBuffer(&bd, NULL, &g_pConstantBufferPL);
	}

	void Linelist(float x, float y, float width, float height, XMFLOAT4 Color)
	{
		ConstantBufferVTL cb;
		ConstantBufferPTL cb2;
		cb.View = XMFLOAT4(0, 0, 0, 0);
		cb.Pos = XMFLOAT4(x*dx::aspect, y, 0, 0);
		cb.Size = XMFLOAT4(width*dx::aspect, height, 0, 0);
		cb2.Color = Color;
		g_pImmediateContext->UpdateSubresource(g_pConstantBufferVTL, 0, NULL, &cb, 0, 0);
		g_pImmediateContext->UpdateSubresource(g_pConstantBufferPTL, 0, NULL, &cb2, 0, 0);
		g_pImmediateContext->Draw(timedividercount * 2 + 2, 0);
	}

	void Line(float x, float y, float width, float height, XMFLOAT4 Color, int count)
	{
		ConstantBufferVL cb;
		ConstantBufferPL cb2;
		cb.View = XMFLOAT4(0, 0, 0, 0);
		cb.Pos = XMFLOAT4(x*dx::aspect, y, 0, 0);
		cb.Size = XMFLOAT4(width*dx::aspect, height, 0, 0);
		cb2.Color = Color;
		g_pImmediateContext->UpdateSubresource(g_pConstantBufferVL, 0, NULL, &cb, 0, 0);
		g_pImmediateContext->UpdateSubresource(g_pConstantBufferPL, 0, NULL, &cb2, 0, 0);
		g_pImmediateContext->Draw(count * 2, 0);
	}

	float StringLen(float width, char* s)
	{
		float x = 0;
		float LetterSizeUVScale = 1.0 / 16.0;
		int i = 0;
		while (*(s + i) != 0)
		{
			x += FontPTable[s[i] - 32] * width*dx::aspect / 32.f/1.2f;
			i++;
		}
		return x;
	}

#define GradientMaxPoints 5
	typedef struct {
		XMFLOAT4 GradientColor[GradientMaxPoints];
		XMFLOAT4 GradientPos[GradientMaxPoints];
		XMFLOAT4 fxPos[GradientMaxPoints];
		float fxPosCount;
		float GradientPosCount;
	} gradient;

	void Box(float x, float y, float width, float height, XMFLOAT4 color, float roundness = .1, gradient* grad = NULL)
	{
		
		if (x + width < -1 / dx::aspect) return;
		if (x > 1 / dx::aspect) return;
		if (y < -1) return;
		if (y - height > 1) return;

		ConstantBufferV cb;
		ConstantBufferP cb2;
		cb.View = XMFLOAT4(0, 0, 0, 0);
		cb.Pos = XMFLOAT4(x*dx::aspect, y, 0, 0);
		cb.Size = XMFLOAT4(width*dx::aspect, height, 0, 0);
		//cb.Size = XMFLOAT4(width, height, 0, 0);
		cb.uvScale = XMFLOAT4(1, 1, 0, 0);
		cb.uvOffset = XMFLOAT4(0, 0, 0, 0);
		cb2.Color = color;
		cb2.Aspect.x = width / height;
		cb2.Aspect.y = dx::aspect;
		cb2.Aspect.z = dx::iaspect;
		cb2.Aspect.w = roundness;

		if (grad != NULL)
		{
			cb2.GradientPointCount = XMFLOAT4(grad->GradientPosCount,0,0,0);
			for (int n = 0; n < GradientMaxPoints; n++)
			{
				cb2.GradientColor[n] = grad->GradientColor[n];
				cb2.GradientPos[n] = grad->GradientPos[n];
			}
		}
		else
		{
			cb2.GradientPointCount = XMFLOAT4(GradientMaxPoints,0,0,0);
			for (int n = 0; n < GradientMaxPoints; n++)
			{
				cb2.GradientColor[n] = XMFLOAT4(1,1,1,1);
				cb2.GradientPos[n] = XMFLOAT4(n/(float)(GradientMaxPoints-1),0,0,0);
			}
		}

		g_pImmediateContext->UpdateSubresource(g_pConstantBufferV, 0, NULL, &cb, 0, 0);
		g_pImmediateContext->UpdateSubresource(g_pConstantBufferP, 0, NULL, &cb2, 0, 0);
		g_pImmediateContext->Draw(6, 0);
	}

	void Letter(float x, float y, float width, float height, XMFLOAT4 color,char s)
	{
		if (x +width < -1/dx::aspect) return;
		if (x > 1 / dx::aspect) return;
		if (y < -1) return;
		if (y - height > 1 ) return;

		ConstantBufferV cb;
		ConstantBufferP cb2;
		cb.View = XMFLOAT4(0, 0, 0, 0);
		cb.Pos = XMFLOAT4(x*dx::aspect, y, 0, 0);
		cb.Size = XMFLOAT4(width*dx::aspect, height, 0, 0);

		float LetterSizeUVScale = 1.0 / 16.0;
		int lcode = s - 32;
		cb.uvScale = XMFLOAT4(LetterSizeUVScale, LetterSizeUVScale, 0, 0);
		cb.uvOffset = XMFLOAT4(LetterSizeUVScale*(int)(lcode/16), LetterSizeUVScale*(s-16*(int)(lcode/16)), 0, 0);
		cb2.Color = color;
		g_pImmediateContext->UpdateSubresource(g_pConstantBufferV, 0, NULL, &cb, 0, 0);
		g_pImmediateContext->UpdateSubresource(g_pConstantBufferP, 0, NULL, &cb2, 0, 0);
		g_pImmediateContext->Draw(6, 0);
	}

	float TextX, TextY;
	void String(float x, float y, float width, float height, XMFLOAT4 color, char* s)
	{
		TextX = x; TextY = y;
		float LetterSizeUVScale = 1.0f / 16.0f/1.5f;
		int i = 0;
		while (*(s + i) != 0)
		{
			Letter(TextX, TextY, width*.9f, height, color, s[i]);
			TextX += FontPTable[s[i] - 32]* width*dx::aspect/32.f*2.f/1.2f;
			i++;
		}
	}



	void CompileShader(uishader* shader, char* source_vs, char* source_ps)
	{
		HRESULT hr = S_OK;
		ID3DBlob* pErrorBlob;
	
		hr = D3DCompile(source_vs, strlen(source_vs)+1, NULL, NULL, NULL, "VS", "vs_4_0", NULL, NULL,  &shader->pVSBlob, &pErrorBlob);
		if (FAILED(hr)) { MessageBox(hWnd, (char*)pErrorBlob->GetBufferPointer(), NULL, 0); }
		hr = D3DCompile(source_ps, strlen(source_ps)+1, NULL, NULL, NULL, "PS", "ps_4_0", NULL, NULL, &shader->pPSBlob, &pErrorBlob);
		if (FAILED(hr)) { MessageBox(hWnd, (char*)pErrorBlob->GetBufferPointer(), NULL, 0); }
		hr = g_pd3dDevice->CreateVertexShader(shader->pVSBlob->GetBufferPointer(), shader->pVSBlob->GetBufferSize(), NULL, &shader->pVs);
		if (FAILED(hr)) { MessageBox(hWnd, "vs fail", NULL, 0); }
		hr = g_pd3dDevice->CreatePixelShader(shader->pPSBlob->GetBufferPointer(), shader->pPSBlob->GetBufferSize(), NULL, &shader->pPs);
		if (FAILED(hr)) { MessageBox(hWnd, "ps fail", NULL, 0); }

	}

	void ShadersInit()
	{
	
		CompileShader(&BoxShader, base_vs, box_ps);
		CompileShader(&TextShader, base_vs, base_ps);
		CompileShader(&TimelineShader, tl_vs, tl_ps);
		CompileShader(&TexViewShader, base_vs, texview_ps);

	}

	

	void UnpackFrom1Bit(BYTE* src, BYTE* dst)
	{
		_asm {
			mov esi, src
			mov edi, dst

			mov edx, 0xffffffff
			mov ebx, 0

			mov ecx, 16 * 32 //512 pix for heigth
			lp3:

			push ecx
				push edi

				mov ecx, 4 * 16 //64 PIXELS 8 BYTES 1 letter
				lp2 :
				push ecx
				mov ecx, 8//unpack 1 byte to 8 pix
				mov al, [esi]
				lp :
				shl al, 1
				jc pixel
				mov[edi], ebx
				jmp no_pixel
				pixel :
			mov[edi], edx
				no_pixel :
			add edi, 4
				loop lp
				inc esi
				pop ecx
				loop lp2


				pop edi
				add edi, 512 * 4// next string
				pop ecx
				loop lp3

		}
	}

	void LoadFont()
	{
		BYTE *tmp;
		tmp = new BYTE[512 * 512 * 4];

		font = new BYTE[512 * 512];
		UnpackedFont = new BYTE[512 * 512 * 4];

		FILE* ff;
		ff = fopen("basefont32.wbm", "rb");
		if (ff == NULL)
		{
			MessageBox(hWnd, "font not loaded", 0, 0);
			return;
		}
		fread(font, 1, 32774, ff);
		fclose(ff);


		UnpackFrom1Bit(font + 6, tmp);

		//calc widths
		int cw, ofsx, ofsy, t;
		t = 0;

		for (ofsx = 0; ofsx<16; ofsx++)
		{
			for (ofsy = 0; ofsy<16; ofsy++)
			{
				cw = 0;
				for (int y = 0; y<32; y++)
				{
					for (int x = 31; x >= 0; x--)
					{
						if (*(BYTE*)(tmp + (y + ofsy * 32) * 512 * 4 + x * 4 + ofsx * 4 * 32) != 0 && x>cw) { cw = x; goto k; }
					}
				k:;
				}
				FontPTable[t] = cw;
				if (cw == 0) FontPTable[t] = 16;

				//char* s;
				//s = new char[11];
				//_itoa(FontPTable[t], s, 10);
				//MessageBox (Navi,s,0,0);

				t++;
			}

		}

		CreateTexture(&Texture, (char*)tmp, 512);

	}

	void Clear()
	{
		g_pImmediateContext->ClearRenderTargetView(g_pRenderTargetView, colorScheme.background);
		g_pImmediateContext->ClearDepthStencilView(g_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	}
}