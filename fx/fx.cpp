#define EditMode

#define _CRT_SECURE_NO_WARNINGS

#ifdef EditMode
	#include "resource.h"
#endif

#include "windows.h"


HINSTANCE hInst;                               
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
HWND hWnd;

#include <d3d11.h>
#include <d3dcompiler.h>
#include "DirectXMath.h"
#include <DirectXPackedVector.h>
#include <Xaudio2.h>

#ifdef EditMode
	#include <stdio.h>
#endif

#define PT_BYTE 0
#define PT_SWORD 1
#define PT_ENUM 2
#define PT_TEXT 3
#define PT_LABEL 4
#define PT_INT 5
#define PT_ENUMASSIGN 6
#define PT_SBYTE 7

int TypeSizeTable[8] = { 1,2,1,64000,32,4,1,1 };

#include "timer.h"
#include "stack.h"

#define MAXPARAM 32
#define MAXPARAMNAMELEN 32
#define MAXPARAMENUM 64
#define MAXSWITCHERS 8

#include "sound.h"

#include "dxInit.h"
#include "format.h"
#include "commands.h"
#include "cmdInit.h"




#ifdef EditMode

	#include "io.h"

	#include "ui_dx.h"
	#include "ui.h"

#endif

int prev_c=0;
float AverageLoad = 0;
float ŅurrentLoad = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInstance = (HINSTANCE)GetModuleHandle(0);

    MyRegisterClass(hInstance);

    if (!InitInstance (hInstance, nCmdShow)) return FALSE;

	auto hr=dx::InitDevice();
	cmdInit();
	stack::init();
	Commands::loopPoint = stack::data;

#ifdef EditMode
	io::SelfLocate();
	io::LoadTemplate();
	io::WriteUndoStage();
	ui::init();

	ui::removeSelection();
	*(stack::data + CmdDesc[*stack::data].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
	ui::lastSelectedI = 0;
	ui::CurrentPos = 0;

	ui::ShowParam();
	SetFocus(hWnd);

#endif

	MSG msg = { 0 };

	timer::StartCounter();
	sound::Init();


	if (hr == S_OK)
	{

		// Main message loop
		while (WM_QUIT != msg.message)
		{
			int FRAMES_PER_SECOND = 60;
			DWORD currentTick = GetTickCount();
			DWORD endTick = currentTick + 1000 / FRAMES_PER_SECOND;

				while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)&& msg.message != WM_QUIT)
				{

					if (!IsDialogMessage(ui::ToolBox, &msg) && !IsDialogMessage(ui::PEditor, &msg))
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}

			{
				double a = timer::GetCounter();
				timer::fp_delta = a - timer::prevtick;
				
				int c = (int)(playmode*(a - timer::StartTime) / (1.f / 60.f * 1000.f) + Commands::StartPosition);



				if ((playmode == 0 && (timer::fp_delta) >= (1000.0 / 60.0)) ||
					(
						playmode==1&&(prev_c!=c)
					))
				//if ((timer::fp_delta) >= (1000.0 / 20.0))
				{
					prev_c = c;

					timer::prevtick = a;
				
					#ifdef EditMode
						dxui::Clear();
						dx::g_pImmediateContext->OMSetDepthStencilState(pDSState[0], 1);
						
					#endif		

					Commands::Render();

					#ifdef EditMode
					dx::g_pImmediateContext->OMSetDepthStencilState(pDSState[0], 1);
						ui::ShowStack();

						if (a - ui::logTimer > 1500 && ui::logTimer >= 0)
						{
							io::ShowFilename();
							ui::logTimer = -1;
						}
						//ui::NLog(Commands::cursor);
						//ui::NLog(ui::cursorpos);

						
						char t[22];
						AverageLoad = lerp(AverageLoad , ŅurrentLoad,.051f) ;
						_itoa((int)(100.0f*AverageLoad/16666.0f), t, 10);
						float sx = -.99f / dx::aspect;
						float sy = -.95f;

						dxui::SetupDrawerBox();
						dxui::Box(sx, sy, .2f, .045f, colorScheme.loadback);
						dxui::Box(sx, sy, .2f*AverageLoad / 16666.0f, .045f, XMFLOAT4(AverageLoad / 16666.0f, 1.0f-AverageLoad / 16666.0f, 0,.5f+.5f*AverageLoad / 16666.0f));

						dxui::SetupDrawer();
						strcat(t, "%load");
						dxui::String(sx, sy, .045f, .045f, colorScheme.font, t);
						//dxui::String(sx+.05, sy, .045, .045, 1, 1, 1, "load%");
						

					#endif
						dx::g_pSwapChain->Present(0, 0);
						ŅurrentLoad = (float)(timer::GetCounter() - a)*1000.0f;
				}
				
			}
		}


	}

    return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = NULL;
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+2);
	wcex.lpszMenuName	= NULL;
    wcex.lpszClassName  = "fx";
    wcex.hIconSm        = NULL;

    return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow("fx", "fx", WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }
   
   ShowWindow(hWnd, SW_MAXIMIZE);
   UpdateWindow(hWnd);

   return TRUE;
}

bool playonenoteFlag = false;
WPARAM playnoteKey = 0;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		/*	case WM_PAINT:
				{
				//	PAINTSTRUCT ps;
				//	HDC hdc = BeginPaint(hWnd, &ps);
				//	Commands::Render();
				//	EndPaint(hWnd, &ps);
				}
				break;

			case WM_ERASEBKGND:
				{
					//return 1;
				}
				break;*/

	case WM_MOUSEWHEEL:
	{
		WORD fwKeys = GET_KEYSTATE_WPARAM(wParam);
		int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);

		if (zDelta != 0 && ui::isKeyDown(VK_MENU))
		{
			if (zDelta > 0 && ui::timelineScale < 600.0)
			{
				ui::timelinePos.x *= 1.2f;
				ui::timelineScale *= 1.2f;
				//ui::NLog(ui::timelineScale);
			}
			if (zDelta < 0 && ui::timelineScale>0.1)
			{
				ui::timelinePos.x *= .8f;
				ui::timelineScale *= .8f;
			}
		}

		if (zDelta != 0 && !ui::isKeyDown(VK_MENU))
		{
			if (zDelta > 0 && ui::timelineScale < 600.0)
			{
				ui::channelYstart += .025f;
			}
			if (zDelta < 0 && ui::timelineScale>0.1)
			{
				ui::channelYstart -= .025f;
			}
		}

		break;
	}



	case WM_LBUTTONDBLCLK:
	{
		ui::removeSelection();

		int i = 0;
		if (wParam == MK_CONTROL + 1) i = 1;
		if (wParam == MK_SHIFT + 1) i = 2;
		ui::Select(i);
		if (CmdDesc[*(stack::data + ui::CurrentPos)].Routine == Commands::ColorPoint)
		{
			CHOOSECOLOR cc;                 // common dialog box structure 
			COLORREF acrCustClr[16]; // array of custom colors 
			HBRUSH hbrush;                  // brush handle
			DWORD rgbCurrent;        // initial color selection
			rgbCurrent = *(DWORD*)(stack::data + ui::CurrentPos + 2);
											// Initialize CHOOSECOLOR 
			ZeroMemory(&cc, sizeof(cc));
			cc.lStructSize = sizeof(cc);
			cc.hwndOwner = hWnd;
			cc.lpCustColors = (LPDWORD)acrCustClr;
			cc.rgbResult = rgbCurrent;
			cc.Flags = CC_FULLOPEN | CC_RGBINIT;

			if (ChooseColor(&cc) == TRUE)
			{
				hbrush = CreateSolidBrush(cc.rgbResult);
				rgbCurrent = cc.rgbResult;
				*(BYTE*)(stack::data + ui::CurrentPos + 2)= (BYTE)(rgbCurrent);
				*(BYTE*)(stack::data + ui::CurrentPos + 3) = (BYTE)(rgbCurrent>>8);
				*(BYTE*)(stack::data + ui::CurrentPos + 4) = (BYTE)(rgbCurrent>>16);
			}
		}

		if ((CmdDesc[*(stack::data + ui::CurrentPos)].Routine == Commands::Channel)||
			(CmdDesc[*(stack::data + ui::CurrentPos)].Routine == Commands::Mixer)||
			(CmdDesc[*(stack::data + ui::CurrentPos)].Routine == Commands::ControlCurve)||
			(CmdDesc[*(stack::data + ui::CurrentPos)].Routine == Commands::Envelope)||
			CmdDesc[*(stack::data + ui::CurrentPos)].Routine == Commands::Scene)
		{
			BYTE cmd = *(stack::data + ui::CurrentPos);
			BYTE c = *(BYTE*)(stack::data + ui::CurrentPos + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
			c = !c;
			*(BYTE*)(stack::data + ui::CurrentPos + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode) = c;
		}

		if (CmdDesc[*(stack::data + ui::CurrentPos)].Routine == ShowScene)
		{
			BYTE targetScene = *(stack::data + ui::CurrentPos+CmdDesc[*(stack::data + ui::CurrentPos)].ParamOffset[0]);
			ui::removeSelection();
			ui::CurrentPos = ui::GetScenePosById(targetScene);
			*(BYTE*)(stack::data + ui::CurrentPos + CmdDesc[*(stack::data + ui::CurrentPos)].Size - EditorInfoOffset + EditorInfoOffsetSelected) = 1;
			*(BYTE*)(stack::data + ui::CurrentPos + CmdDesc[*(stack::data + ui::CurrentPos)].Size - EditorInfoOffset + EditorInfoOffsetMode) = 1;

		}

	/*	if (CmdDesc[*(stack::data + ui::CurrentPos)].Routine == Commands::Envelope)
		{
			BYTE cmd = *(stack::data + ui::CurrentPos);
			BYTE c = *(BYTE*)(stack::data + ui::CurrentPos + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
			c = !c;
			*(BYTE*)(stack::data + ui::CurrentPos + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode)=c;
		}*/

		if (!ui::LButtonDown)
		{
			ui::ShowParam();
		//	SetFocus(hWnd);
		}
		ui::LButtonDown = true;

		break;
	}

	case WM_LBUTTONUP:
		ui::LButtonDown = false;
		break;

	case WM_LBUTTONDOWN:
	{

		int i = 0;
		if (wParam == MK_CONTROL + 1) i = 1;
		if (wParam == MK_SHIFT + 1) i = 2;
		ui::Select(i);

		if (!ui::LButtonDown)
		{


			io::WriteUndoStage();
			ui::ShowParam();
			//ui::ShowParam();//wtf? text articacts
			
		//	SetFocus(hWnd);
		}
		ui::LButtonDown = true;

		break;
	}

	case WM_SYSKEYDOWN:
	{
		break;
	}

	case WM_KEYDOWN:
	{
		BYTE cmd = *(stack::data + ui::CurrentPos);

		if (wParam == 'L')
		{
			BYTE* i = stack::data;
			int minX, maxX;
			minX = 32767; maxX = 0;
			while (*i != 0)
			{
				//if (ui::GetTimeLineStatus(i) == true&&*(i+CmdDesc[*i].Size-EditorInfoOffset+EditorInfoOffsetSelected)==1)
				if (*(i + CmdDesc[*i].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
				{
					signed short left, right;
					if (CmdDesc[*i].Routine == Clip)
					{
						left = *(signed short*)(i + CmdDesc[*i].ParamOffset[0]);
						signed short len = *(signed short*)(i + CmdDesc[*i].ParamOffset[2]);
						signed short npm = *(signed short*)(i + CmdDesc[*i].ParamOffset[6])*4;
						if (npm == 0) npm = 1;
						int intNPM = (int)(3600 / (int)npm);

						len *= intNPM;
						right = left + len;

						if (left < minX) minX = left;
						if (right> maxX) maxX = right;
					}

				}

				i += CmdDesc[*i].Size;
			}

			i = stack::data;
			while (*i != 0)
			{
				if (CmdDesc[*i].Routine == TimeLoop)
				{
					signed short oldMin = *(signed short*)(i + CmdDesc[*i].ParamOffset[1]);
					signed short oldMax = *(signed short*)(i + CmdDesc[*i].ParamOffset[2]);

					if (oldMin != minX || oldMax+oldMin != maxX)
					{
						*(BYTE*)(i + CmdDesc[*i].ParamOffset[0]) = 1;
					}
					else
					{
						*(BYTE*)(i + CmdDesc[*i].ParamOffset[0]) = 1-*(BYTE*)(i + CmdDesc[*i].ParamOffset[0]);
					}

					if (minX < maxX)
					{
						*(signed short*)(i + CmdDesc[*i].ParamOffset[1]) = minX;
						*(signed short*)(i + CmdDesc[*i].ParamOffset[2]) = maxX - minX;
					}
					
				}

				i += CmdDesc[*i].Size;
			}

		}

		if (ui::isKeyDown(VK_SHIFT))
		{
			if (wParam == VK_DOWN || wParam == VK_UP)
			{
				int dir = 0;
				if (wParam == VK_UP) dir = 1;
				if (wParam == VK_DOWN) dir = -1;

				int i = 0;
				while (*(BYTE*)(stack::data + i) != 0)
				{

					BYTE ncmd = *(stack::data + i);
					if (CmdDesc[ncmd].Routine == Note && (*(BYTE*)(stack::data + i + CmdDesc[ncmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1))
					{
						BYTE _n = *(stack::data + i + 3);
						int n = _n;
						n += dir;
						if (n >= 0 && n <= 255)
						{
							*(BYTE*)(stack::data + i + 3) = (BYTE)n;
						}

					}
					i += CmdDesc[ncmd].Size;
				}
				ui::ShowParam();
			}
			
		}

		if (ui::isKeyDown(VK_CONTROL))
		{
			if (wParam == VK_DOWN|| wParam == VK_UP)
			{
				int dir = 0;
				if (wParam == VK_UP) dir = 10;
				if (wParam == VK_DOWN) dir = -10;

				int i = 0;
				while (*(BYTE*)(stack::data + i) != 0)
				{

					BYTE ncmd = *(stack::data + i);
					if (CmdDesc[ncmd].Routine == Note && (*(BYTE*)(stack::data + i + CmdDesc[ncmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1))
					{
						BYTE _n = *(stack::data + i + 2);
						int n = _n;
							n += dir;
							if (n >= 0 && n <= 255)
							{
								*(BYTE*)(stack::data + i + 2) = (BYTE)n;
							}

					}
					i += CmdDesc[ncmd].Size;
				}
				ui::ShowParam();

			}

			if (wParam == VK_LEFT || wParam == VK_RIGHT)
			{
				int dir = 0;
				if (wParam == VK_LEFT) dir = -10;
				if (wParam == VK_RIGHT) dir = 10;

				int i = 0;
				while (*(BYTE*)(stack::data + i) != 0)
				{

					BYTE ncmd = *(stack::data + i);
					if (CmdDesc[ncmd].Routine == Note && (*(BYTE*)(stack::data + i + CmdDesc[ncmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1))
					{
						signed char _n = *(stack::data + i + 4);
						int n = _n;
						n += dir;
						if (n >= -127 && n <= 127)
						{
							*(signed char*)(stack::data + i + 4) = (signed char)n;
						}

					}
					i += CmdDesc[ncmd].Size;
				}
				ui::ShowParam();

			}

			if (wParam >= '0'&& wParam < '9')
			{
				ui::autoNextNote = wParam - '0';
				char octx[10];
				_itoa(ui::autoNextNote, octx, 10);
				char octxt[32];
				strcpy(octxt, "step changed to ");
				strcat(octxt, octx);
				ui::Log(octxt);
	
			}

			if ((wParam == 'Q') || (wParam == 'A') || (wParam == VK_PRIOR) || (wParam == VK_NEXT))
			{
				int dir = 0;
				if (wParam == 'Q') dir = 1;
				if (wParam == 'A') dir = -1;
				if (wParam == VK_NEXT) dir = -12;
				if (wParam == VK_PRIOR) dir = 12;

				int i = 0;
				while (*(BYTE*)(stack::data + i) != 0)
				{

					BYTE ncmd = *(stack::data + i);
					if (CmdDesc[ncmd].Routine == Note && (*(BYTE*)(stack::data + i + CmdDesc[ncmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1))
					{
						BYTE _n = *(stack::data + i + 1);
						int n = _n;
						if (n != 0)
						{
							n += dir;
							if (n > 0 && n < 97)
							{
								*(BYTE*)(stack::data + i + 1) = (BYTE)n;
							}
						}
					}
					i += CmdDesc[ncmd].Size;
				}
				ui::ShowParam();
				io::WriteUndoStage();
	
			}
			
		}

		//if (isKeyDown(VK_CONTROL))

		if (CmdDesc[cmd].Routine == Note)
		{
			if (wParam >= '\\')
			{
				ui::VolumeOverwrite = !ui::VolumeOverwrite;
				if (ui::VolumeOverwrite) ui::Log("Volume overwrite on"); else ui::Log("Volume overwrite off");
		
			}

			if (wParam >= '1'&&wParam <= '9'&&ui::isKeyDown(VK_SHIFT))
			{
				ui::cVolume =(int) ((wParam - '0')*255.0/10.0);
				char octx[10];
				_itoa(ui::cVolume, octx, 10);
				char octxt[32];
				strcpy(octxt, "Volume changed to ");
				strcat(octxt, octx);
				ui::Log(octxt);
	
			}
			if (wParam == '0'&&ui::isKeyDown(VK_SHIFT))
			{
				ui::cVolume = 255;
				char octx[10];
				_itoa(ui::cVolume, octx, 10);
				char octxt[32];
				strcpy(octxt, "Volume changed to ");
				strcat(octxt, octx);
				ui::Log(octxt);
			
			}


			if (wParam >= VK_NUMPAD0&&wParam <= VK_NUMPAD7)
			{
				ui::cOctave = wParam - VK_NUMPAD0;
				char octx[10];
				_itoa(ui::cOctave, octx, 10);
				char octxt[32];
				strcpy(octxt, "Octave changed to ");
				strcat(octxt, octx);
				ui::Log(octxt);
		
			}
		
		}

		if (CmdDesc[cmd].Routine == Note&&ui::isKeyDown(VK_CONTROL))
		{
			switch (wParam)
			{
				case VK_OEM_4:
				{
					if (ui::cVolume > 0)
					{
						ui::cVolume--;
						char octx[10];
						_itoa(ui::cVolume, octx, 10);
						char octxt[32];
						strcpy(octxt, "Volume changed to ");
						strcat(octxt, octx);
						ui::Log(octxt);
					}
		
				}
				break;

				case VK_OEM_6:
				{
					if (ui::cVolume < 256)
					{
						ui::cVolume++;
						char octx[10];
						_itoa(ui::cVolume, octx, 10);
						char octxt[32];
						strcpy(octxt, "Volume changed to ");
						strcat(octxt, octx);
						ui::Log(octxt);
					}
			
				}
				break;
			}
	
		}

		if (!ui::isKeyDown(VK_CONTROL) && 
			!ui::isKeyDown(VK_SHIFT) && 
			!ui::isKeyDown(VK_MENU)&& 
			!ui::isKeyDown(VK_DELETE) && 
			!ui::isKeyDown(VK_SPACE) &&
			CmdDesc[cmd].Routine == Note)
		{
			int p = -1;
			BYTE cOctave = ui::cOctave;

			switch (wParam)
			{
				case VK_RETURN: p = 0; break;
				case 'Z':p = 0 + cOctave * 12 + 1; break;//C
				case 'S':p = 1 + cOctave * 12 + 1; break;//C#
				case 'X':p = 2 + cOctave * 12 + 1; break;//D
				case 'D':p = 3 + cOctave * 12 + 1; break;//D#
				case 'C':p = 4 + cOctave * 12 + 1; break;//E
				case 'V':p = 5 + cOctave * 12 + 1; break;//F
				case 'G':p = 6 + cOctave * 12 + 1; break;//F#
				case 'B':p = 7 + cOctave * 12 + 1; break;//G
				case 'H':p = 8 + cOctave * 12 + 1; break;//G#
				case 'N':p = 9 + cOctave * 12 + 1; break;//A
				case 'J':p = 10 + cOctave * 12 + 1; break;//A#
				case 'M':p = 11 + cOctave * 12 + 1; break;//B
				case 'Q':p = 0 + cOctave * 12 + 1 + 12; break;//C
				case '2':p = 1 + cOctave * 12 + 1 + 12; break;//C#
				case 'W':p = 2 + cOctave * 12 + 1 + 12; break;//D
				case '3':p = 3 + cOctave * 12 + 1 + 12; break;//D#
				case 'E':p = 4 + cOctave * 12 + 1 + 12; break;//E
				case 'R':p = 5 + cOctave * 12 + 1 + 12; break;//F
				case '5':p = 6 + cOctave * 12 + 1 + 12; break;//F#
				case 'T':p = 7 + cOctave * 12 + 1 + 12; break;//G
				case '6':p = 8 + cOctave * 12 + 1 + 12; break;//G#
				case 'Y':p = 9 + cOctave * 12 + 1 + 12; break;//A
				case '7':p = 10 + cOctave * 12 + 1 + 12; break;//A#
				case 'U':p = 11 + cOctave * 12 + 1 + 12; break;//B

				case VK_OEM_4: 
				{
					if (ui::cOctave > 0)
					{
						ui::cOctave--;
						char octx[10];
						_itoa(ui::cOctave, octx, 10);
						char octxt[32];
						strcpy(octxt, "Octave changed to ");
						strcat(octxt, octx);
						ui::Log(octxt);
					}
				}
					break;

				case VK_OEM_6: 
				{
					if (ui::cOctave < 9)
					{
						ui::cOctave++;
						char octx[10];
						_itoa(ui::cOctave, octx, 10);
						char octxt[32];
						strcpy(octxt, "Octave changed to ");
						strcat(octxt, octx);
						ui::Log(octxt);
					}
				}
					break;

//				case VK_INSERT:
//					
//					if (ui::autoNextNote) ui::Log("autoNextNote on"); else ui::Log("autoNextNote off");
//					break;

				case VK_RIGHT:
				{
					BYTE* nn = stack::data + ui::CurrentPos + CmdDesc[*(stack::data + ui::CurrentPos)].Size;
					if (*(nn) != 0 && CmdDesc[*nn].Routine == Note)
					{
						ui::CurrentPos += CmdDesc[*(stack::data + ui::CurrentPos)].Size;
						ui::removeSelection();
						ui::SetCurSelect();
						ui::ShowParam();
					}
				}
					break;

				case VK_LEFT:
					{
						int x = 0; 
						while (x < ui::CurrentPos)
						{
							if (CmdDesc[*(stack::data + x)].Routine == Note)
							{
								if ((x+ CmdDesc[*(stack::data + x)].Size) == ui::CurrentPos)
								{
									ui::CurrentPos = x;
								}
							}
							x += CmdDesc[*(stack::data + x)].Size;
						}
						ui::removeSelection();
						ui::SetCurSelect();
						ui::ShowParam();
						
					break;
					}

			}

			if (p >= 0)
			{
				BYTE pp = *(stack::data + ui::CurrentPos + 1);
				*(stack::data + ui::CurrentPos + 1) = (BYTE)p;

				if (ui::VolumeOverwrite) *(stack::data + ui::CurrentPos + 2) = (BYTE)ui::cVolume;
				
				int playPos = ui::CurrentPos;
				
					for (int g = 0; g < ui::autoNextNote; g++)
					{
						ui::CurrentPos += CmdDesc[*(stack::data + ui::CurrentPos)].Size;
					}

				ui::removeSelection();
				ui::SetCurSelect();
				ui::ShowParam();
				
				if (pp != p)
					{
						io::WriteUndoStage();
					}

				if (playonenoteFlag == false&&playmode==0)
				{
					PlayOneNote(playPos);
					playonenoteFlag = true;
					playnoteKey = wParam;
		
				}
				
			}
		
		}
		else
		switch (wParam)
		{
		case VK_SPACE:
			{
			if (Commands::playmode == 0)
			{
				Commands::PlayWave((int)Commands::StartPosition);
			}
			else
			{
				Commands::StartPosition = Commands::cursorF;// +(-60.f*100.f)*ui::timelinePos.x / ui::timelineScale;
				if (Commands::StartPosition < 0) Commands::StartPosition = 0;
				Commands::StopWave();
				Commands::playmode = 0;				
			}
			
			break;
			}
		case VK_DELETE:
		{
			ui::removeSelected();
			ui::SetCurSelect();
			io::WriteUndoStage();
			ui::SetCurSelect();
			ui::Select(0);
			ui::ShowParam();
			ui::Log("deleted");
			break;
		}
		case 'C':
			if (ui::isKeyDown(VK_CONTROL))
			{
				ui::copySelected();
				ui::removeSelection();
				ui::SetCurSelect();
				ui::Log("copied");
			}
			break;
		case 'V':
			if (ui::isKeyDown(VK_CONTROL))
			{
				if (ui::paste())
				{
					ui::removeSelection();
					ui::SetCurSelect();
					io::WriteUndoStage();
					ui::Log("pasted");
				}
				else
				{
					ui::Log("incorrect type combination for paste");
				}
			}
			break;
		case 'X':
			if (ui::isKeyDown(VK_CONTROL))
			{
				ui::copySelected();
				ui::removeSelected();
				ui::removeSelection();
				ui::SetCurSelect();
				ui::Select(0);
				ui::ShowParam();
				io::WriteUndoStage();
				ui::Log("cutted");
			}
			break;

		case 'Z':
			if (ui::isKeyDown(VK_CONTROL)&& ui::isKeyDown(VK_SHIFT))
			{
				io::Redo();
				ui::removeSelection();
				ui::SetCurSelect();
				ui::Select(0);
				ui::ShowParam();

				ui::Log("redo complete");
			}
			else 
			if (ui::isKeyDown(VK_CONTROL))
			{
				io::Undo();
				ui::removeSelection();
				ui::SetCurSelect();
				ui::Select(0);
				ui::ShowParam();

				ui::Log("undo complete");

			}

			break;
		}

		break;
	}

	case WM_KEYUP:
	{
		if (!ui::isKeyDown(playnoteKey)&&playonenoteFlag)
		{
			playonenoteFlag = false;
			Commands::playmode = 0;
			Commands::StopWave();
			
		}
		break;
	}

    case WM_CLOSE:
		 PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}


