typedef void(*PVFN)();//func pointer type for commands

//PVFN CurrentCmd0, CurrentCmd1, CurrentCmd2;



typedef struct { float color[3]; } color3;
typedef struct { float color[4]; } color4;

typedef struct
{
	PVFN Routine;
	signed char Level;
	int Size;
	int	 ParamOffset[MAXPARAM];
	BYTE ParamType[MAXPARAM];
#ifdef EditMode
	char Name[MAXPARAMNAMELEN];
	int	 Category;
	char ParamName[MAXPARAM][MAXPARAMNAMELEN];
	int  ParamMin[MAXPARAM];
	int  ParamMax[MAXPARAM];
	int  ParamDefault[MAXPARAM];
	char ParamEnum[MAXPARAM][MAXPARAMENUM][MAXPARAMNAMELEN];
	bool ParamEnvControlled[MAXPARAM];
	XMFLOAT4 Color;
	int posType;
	int switchersNum;
	char switchersText[MAXSWITCHERS];
#endif

} Cmd;

Cmd CmdDesc[256];//limit for 1-byte encoding