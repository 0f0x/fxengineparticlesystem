namespace ui
{
	
	int cursorpos = 0;
	double cursorposF = 0;
	int cOctave = 1;
	int cVolume = 255;
	bool VolumeOverwrite = true;
	int autoNextNote = 0;

	typedef struct { float x; float y; } point2d;
	point2d viewPos, oldviewPos, oldCursor;
	point2d timelinePos, oldtimelinePos, oldtimelineCursor;

	point2d itemViewPos, itemOldCursor;
	point2d* itemOldviewPos;

	point2d stackItemViewPos, stackItemOldCursor;
	point2d* stackItemOldviewPos;

	point2d cameraPos, oldcameraPos, oldcameraCursor;

	bool SelectGhost = false;

	float timelineScale = 1.0;
	int dragFlag = 0;

	int itemDragFlag = 0;
	int stackItemDragFlag = 0;

	int timelinedragFlag = 0;

	int cameradragFlag = 0;

	bool LButtonDown = false;

	HWND ToolBox;
	HWND PEditor;
	HWND TEditor;
	int peName = 2000;
	int peButton = 3000;
	int peEditBox = 4000;
	
	int teEditBox = 5000;
	int teOk = 5001;
	int teCancel = 5002;

	BYTE* TextEditorActivePtr = NULL;
	int TextEditorActiveMaxSize = 0;
	BYTE TextEditorActiveCmd = 0;
	BYTE TextEditorActiveShader = 0;

	HWND TE_edit, TE_ok, TE_cancel;

	#define PESTEP 22

	int CurrentPos = 0;
	int lastSelectedI = 0;

	typedef struct {
		HWND Text;
		HWND Edit;
		HWND Button;
	} paramElements;

	paramElements paramH[MAXPARAM];

	int ToolBoxCtrlStartIndex = 200;
	int ToolBoxCtrlCounter = 0;

	double logTimer;

	bool isKeyDown(int key)
	{
		return GetAsyncKeyState(key) & 0x8000;
	}

	void Log(char* text)
	{
		logTimer = timer::GetCounter();
		SetWindowText(hWnd, text);
	}

	void NLog(int n)
	{
		logTimer = timer::GetCounter();
		char sss[22];
		_itoa(n, sss, 10);
		SetWindowText(hWnd, sss);
	}


	void MainMenu()
	{
		RECT rect; POINT pt;
		GetWindowRect(GetDlgItem(ToolBox, ToolBoxCtrlStartIndex), &rect);
		pt.x = rect.left + 22;
		pt.y = rect.top - 2;

		HMENU hMenu;
		hMenu = CreatePopupMenu();
		int j = 0;
		AppendMenu(hMenu, MF_STRING, 2000 + j++, "New");
		AppendMenu(hMenu, MF_STRING, 2000 + j++, "Load");
		AppendMenu(hMenu, MF_STRING, 2000 + j++, "Save");
		AppendMenu(hMenu, MF_STRING, 2000 + j++, "Save as...");
		AppendMenu(hMenu, MF_STRING, 2000 + j++, "ReCalc");
		int c = TrackPopupMenu(hMenu, TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x + 32, pt.y, 0, ToolBox, NULL);

		if (c != 0)
		{
			int d = c - 2000;
			switch (d)
			{
			case 0: {Commands::loopPoint = stack::data; io::LoadTemplate(); ui::logTimer = 0; break;	}
			case 1: {Commands::loopPoint = stack::data; io::Load(); ui::logTimer = 0; break; }
			case 2: {io::Save(); ui::Log("saved"); break; }
			case 3: {io::SaveAs(); ui::Log("saved with new filename"); break; }
			case 4: {Commands::loopPoint=stack::data; break; }
			}
		}

		DestroyMenu(hMenu);
	}

	void SetCurSelect()
	{
		*(stack::data + CurrentPos + CmdDesc[*(stack::data + CurrentPos)].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
	}
	void removeSelection();
	void Select(int mode);
	void ShowParam();

	void InsertCmd(int n)
	{
		removeSelection();
		BYTE CurCmd = *(stack::data + CurrentPos);
		int back = 0;
		if (((CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Envelope)||
			CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Point) && (CmdDesc[n].Routine != Point))
		{
			int cp = CurrentPos;
			BYTE c = *(stack::data + cp);

				while ((CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Envelope ||
					    CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Point))
				{
					c = *(stack::data + CurrentPos);
					CurrentPos += CmdDesc[c].Size;
					back= CmdDesc[c].Size;
				}
				CurrentPos -= back;
			CurCmd = *(stack::data + CurrentPos);
		}

		
		if (((CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Clip) ||
			CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Note) && (CmdDesc[n].Routine != Note))
		{
			
			int cp = CurrentPos;
			BYTE c = *(stack::data + cp);

			while ((CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Clip ||
				CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Note))
			{
				c = *(stack::data + CurrentPos);
				CurrentPos += CmdDesc[c].Size;
				back = CmdDesc[c].Size;
			}
			CurrentPos -= back;
			CurCmd = *(stack::data + CurrentPos);
		}

		

		int LastPos = CurrentPos;
		BYTE LastCmd = *(BYTE*)(stack::data + CurrentPos);
		int LastSize = CmdDesc[LastCmd].Size;

		int CurCmdSize = CmdDesc[CurCmd].Size;
		CurrentPos += CurCmdSize;
		int sz = CmdDesc[n].Size;
		memmove(stack::data + CurrentPos + sz, stack::data + CurrentPos, MAXDATASIZE - CurrentPos - sz);
		ZeroMemory(stack::data + CurrentPos, sz);
		*(stack::data + CurrentPos) = (BYTE)n;

		if (CmdDesc[n].Routine == Envelope)
		{
			*(BYTE*)(stack::data + CurrentPos + CmdDesc[n].Size - EditorInfoOffset + EditorInfoOffsetMode) =(BYTE)1;
		}


		if (CmdDesc[n].Routine == Point)
		{
			signed short pX = (signed short)( *(signed short*)(stack::data + LastPos + 1)+200.0f/ui::timelineScale);
			signed short pY = 0;
				if (CmdDesc[*(BYTE*)(stack::data + CurrentPos + CmdDesc[n].Size)].Routine == Point)
				{
					signed short x = *(signed short*)(stack::data + LastPos + 1);
					signed short y = *(signed short*)(stack::data + LastPos + 3);
					signed short x1 = *(signed short*)(stack::data + CurrentPos + CmdDesc[n].Size + 1);
					signed short y1 = *(signed short*)(stack::data + CurrentPos + CmdDesc[n].Size + 3);
					pX = (signed short)((x + x1)*.5f);
					pY = (signed short)((y + y1)*.5f);
				}
			*(signed short*)(stack::data + CurrentPos + 1)=pX;
			*(signed short*)(stack::data + CurrentPos + 3) = pY;
		}


		lastSelectedI++;
		SetCurSelect();

		ui::Select(0);

			ui::ShowParam();
		//	SetFocus(hWnd);
		ui::LButtonDown = true;
	}


	void SelectCmd(WORD category)
	{
		RECT rect; POINT pt;
		GetWindowRect(GetDlgItem(ToolBox, category), &rect);
		pt.x = rect.left + 22;
		pt.y = rect.top - 2;

		HMENU hMenu;
		hMenu = CreatePopupMenu();
		for (int j = 1; j < stack::CommandsCount; j++)
		{
			if (category - ToolBoxCtrlStartIndex == CmdDesc[j].Category) AppendMenu(hMenu, MF_STRING, 2000 + j, CmdDesc[j].Name);
		}
		int c = TrackPopupMenu(hMenu, TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x + 32, pt.y, 0, ToolBox, NULL);

		if (c != 0)
		{
			int d = c - 2000;

			InsertCmd(d);
			io::WriteUndoStage();
		}

		DestroyMenu(hMenu);
	}

	int EnumMenu(int cmd,int pNum)
	{

		RECT rect; POINT pt;
		GetWindowRect(paramH[pNum].Button, &rect);
		pt.x = rect.left + 2;
		pt.y = rect.top + PESTEP-2;

		HMENU hMenu;
		hMenu = CreatePopupMenu();

		int i = 0;

		while ((BYTE*)*CmdDesc[cmd].ParamEnum[pNum][i] != 0)
		{
			AppendMenu(hMenu, MF_STRING, 2000 + i, CmdDesc[cmd].ParamEnum[pNum][i]);
			i++;
		}
		
		return TrackPopupMenu(hMenu, TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x , pt.y, 0, PEditor, NULL)-2000;
	}

	int EnumMenuAssign(int cmd, int pNum)
	{

		RECT rect; POINT pt;
		GetWindowRect(paramH[pNum].Button, &rect);
		pt.x = rect.left + 2;
		pt.y = rect.top + PESTEP - 2;

		HMENU hMenu;
		hMenu = CreatePopupMenu();

		int i = 0;

		while ((BYTE*)*CmdDesc[cmd].ParamName[i] != 0)
		{
			char complex[255];
			strcpy(complex, CmdDesc[cmd].Name);
			strcat(complex, ": ");
			strcat(complex, CmdDesc[cmd].ParamName[i]);
			if (CmdDesc[cmd].ParamEnvControlled[i] == true)
				{
					AppendMenu(hMenu, MF_STRING, 2000 + i, complex);
				}
				else
				{
					AppendMenu(hMenu, MF_STRING|MF_DISABLED, 2000 + i, complex);
				}
			i++;
		}

		return TrackPopupMenu(hMenu, TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, 0, PEditor, NULL) - 2000;
	}

	BOOL CALLBACK ToolProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		if (LOWORD(wParam) == ToolBoxCtrlStartIndex + 0)
		{
			MainMenu();
		//	SetFocus(hWnd);
			return 0;
		}

		if ((LOWORD(wParam) > ToolBoxCtrlStartIndex) && (LOWORD(wParam) < (ToolBoxCtrlStartIndex + ToolBoxCtrlCounter)))
		{
			SelectCmd(LOWORD(wParam));
		//	SetFocus(hWnd);
			return 0;
		}
		
		return 0;
	}



	void AdaptiveTESize()
	{
		RECT r;
		GetWindowRect(TEditor, &r);
		SetWindowPos(TE_ok, HWND_TOP, 0, r.bottom - r.top - 65-32, (r.right - r.left - 15) / 1, 25+32, SWP_SHOWWINDOW);
		//SetWindowPos(TE_cancel, HWND_TOP, (r.right - r.left - 5) / 2, r.bottom - r.top - 65, (r.right - r.left - 15) / 2, 25, SWP_SHOWWINDOW);
		SetWindowPos(TE_edit, HWND_TOP, 5, 5, r.right - r.left - 20, r.bottom - r.top - 75-32, SWP_SHOWWINDOW);
	}

	BOOL CALLBACK EditBoxProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_COMMAND:
			{
				int control = LOWORD(wParam);

				if (control == teOk)
				{
					if (dx::pErrorBlob != NULL)
					{
						char* p1 = NULL;
						p1 = strstr((char*)dx::pErrorBlob->GetBufferPointer(), "(");
						if (p1)
						{
							p1++;
							int s = atoi(p1);
							char* p2 = (char*)TextEditorActivePtr;
							int i = 1;
							while (i < s)
							{
								p2 = strstr(p2, "\n"); p2 += 2;
								i++;
							}
							
							char* p3= strstr(p2, "\n");

							SendDlgItemMessage(hDlg, teEditBox, EM_SETSEL, p2 - (char*)TextEditorActivePtr-1, p3 - (char*)TextEditorActivePtr);;
							SendDlgItemMessage(hDlg, teEditBox, EM_SCROLLCARET, 0, 0);
						}
					
					}
				}

				if (control == teEditBox)
				{
					if (HIWORD(wParam) == EN_CHANGE)
					{
						
						ZeroMemory(TextEditorActivePtr, TextEditorActiveMaxSize);
						GetDlgItemText(hDlg, control, (LPSTR)TextEditorActivePtr, TextEditorActiveMaxSize-1);

						if (CmdDesc[TextEditorActiveCmd].Routine == CreateShader)
						{
	
							dx::CompileShader(TextEditorActiveShader, (char*)TextEditorActivePtr, (char*)TextEditorActivePtr);
							if (dx::pErrorBlob != NULL)
							{
								char* s = strstr((char*)dx::pErrorBlob->GetBufferPointer(), "error");
								if (s) SetWindowText(TE_ok, s); else
								SetWindowText(TE_ok, "Shader compiled");
							}
							else
							{
								SetWindowText(TE_ok, "Shader compiled");
							}
						}
					}
				}
				break;
			}

			case WM_SIZING:
			{
				AdaptiveTESize();
				break; 
			}

			case WM_CLOSE:
			{
				//EnableWindow(hWnd, TRUE);
				ShowWindow(TEditor, SW_HIDE);
			break;
			}
		}

		return 0;
	}

	int LastShownCmd = -1;

	char* GetShaderName(BYTE shaderID);
	char* GetShaderPtr(BYTE shaderID);

	void ShowParam()
	{
		HWND pf = GetFocus();

		RECT r;
		GetWindowRect(hWnd, &r);

		BYTE* pP = stack::data + CurrentPos;

		BYTE cmd = *pP;
		SetWindowText(PEditor, CmdDesc[cmd].Name);
		int x = 0;

		//change name for replace it from shader constant buffer
		if (CmdDesc[cmd].Routine == Quad)
		{
			BYTE shaderID = *(BYTE*)(pP + CmdDesc[cmd].ParamOffset[0]);
			char* text = GetShaderPtr(shaderID);
			char* ptr1 = text;
			char* ptr2 = text;
			char* end = text;
			//strstr(text, "float4 _");
			text = strstr(text, "{");
			end = strstr(text, "}");
			
			int pc = 1;
			while (text < end&&text != NULL&&ptr2!=NULL)
			{
				strcpy(CmdDesc[cmd].ParamName[pc], "");
				text = strstr(text, "float4 _"); 
				if (text != NULL)
				{
					text += 8;
					ptr2 = strstr(text, ";");
					if (ptr2 != NULL)
					{
						if (text != NULL && ptr2 != NULL)
						{
							strncpy(CmdDesc[cmd].ParamName[pc], text, min(ptr2 - text, MAXPARAMNAMELEN - 1));
							CmdDesc[cmd].ParamName[pc][min(ptr2 - text, MAXPARAMNAMELEN - 1)] = 0;
							CmdDesc[cmd].ParamType[pc] = PT_SWORD;
						}
					}

					//enum
					char* cr = strstr(ptr2, "\n");
					ptr1 = ptr2;
					int enumN = 0;

					for (int t = 0; t < MAXPARAMENUM;t++)CmdDesc[cmd].ParamEnum[pc][t][0] = 0;

					while (ptr1 != NULL&&ptr1<cr)
					{
						ptr1 = strstr(ptr1, "//")+2;
						if (ptr1 != NULL&&ptr1<cr)
						{
							char* ptr3 = strstr(ptr1, "//");
							if (ptr3 == NULL||(ptr3 != NULL&&ptr3>cr))
							{
								strncpy(CmdDesc[cmd].ParamEnum[pc][enumN], ptr1, min(cr - ptr1, MAXPARAMNAMELEN-1));
								CmdDesc[cmd].ParamEnum[pc][enumN][min(cr - ptr1, MAXPARAMNAMELEN-1)] = 0;
								ptr1 = cr;
							}
							else
							{
								strncpy(CmdDesc[cmd].ParamEnum[pc][enumN], ptr1, min(ptr3 - ptr1, MAXPARAMNAMELEN-1));
								CmdDesc[cmd].ParamEnum[pc][enumN][min(ptr3 - ptr1, MAXPARAMNAMELEN-1)] = 0;
		
							}

							CmdDesc[cmd].ParamType[pc] = PT_ENUM;
							enumN++;
						}
					}
				}
				
				pc++;
			}
			//strncpy(CmdDesc[cmd].ParamName[1], "ppp", 32);
		}
		//


		while ((BYTE*)*CmdDesc[*pP].ParamName[x] != 0)
		{
			int offset = CmdDesc[cmd].ParamOffset[x];
			char* name = CmdDesc[cmd].ParamName[x];
			BYTE type = CmdDesc[cmd].ParamType[x];
			//

			//custom enum
			if (!strcmp(name, "shader"))
			{
				type = PT_ENUM;
				CmdDesc[cmd].ParamType[x] = type;
				
				BYTE* sp = stack::data;
				int shaderCounter = 0;
				while (*sp != 0 )//check shader creation area
				{
					if (CmdDesc[*sp].Routine == CreateShader && shaderCounter<MAXPARAMENUM)
					{
							for (int _x = 0; _x<MAXPARAM; _x++)
							{
								if (!strcmp(CmdDesc[*sp].ParamName[_x], "name"))
								{
									strcpy(CmdDesc[cmd].ParamEnum[x][shaderCounter], (char*)(sp + CmdDesc[*sp].ParamOffset[_x]));
								}
							}
						

						shaderCounter++;
					}

					sp += CmdDesc[*sp].Size;
				}

			}

			if (!strcmp(name, "texture"))
			{
				type = PT_ENUM;
				CmdDesc[cmd].ParamType[x] = type;

				BYTE* sp = stack::data;
				int textureCounter = 0;
				while (*sp != 0 )//check texture creation area
				{
					if (CmdDesc[*sp].Routine == CreateTexture&& textureCounter<MAXPARAMENUM)
					{
						for (int _x = 0; _x<MAXPARAM; _x++)
						{
							if (!strcmp(CmdDesc[*sp].ParamName[_x], "name"))
							{
								char* tn = (char*)(sp + CmdDesc[*sp].ParamOffset[_x]);
								strcpy(CmdDesc[cmd].ParamEnum[x][textureCounter], tn);
							}
						}

						textureCounter++;
					}
					sp += CmdDesc[*sp].Size;
				}

			}

			if (!strcmp(name, "scene"))
			{
				type = PT_ENUM;
				CmdDesc[cmd].ParamType[x] = type;

				BYTE* sp = stack::data;
				int sceneCounter = 0;
				while (*sp != 0 )//check scene creation area
				{
					if (CmdDesc[*sp].Routine == Scene && sceneCounter<MAXPARAMENUM)
					{
						for (int _x = 0; _x<MAXPARAM; _x++)
						{
							if (!strcmp(CmdDesc[*sp].ParamName[_x], "name"))
							{
								char* tn = (char*)(sp + CmdDesc[*sp].ParamOffset[_x]);
								strcpy(CmdDesc[cmd].ParamEnum[x][sceneCounter], tn);
							}
						}

						sceneCounter++;
					}
					sp += CmdDesc[*sp].Size;
				}

			}

			if (!strcmp(name, "vb"))
			{
			}

			if (name)
			{
				SetWindowText(paramH[x].Text, name);
				switch (type)
				{
				case PT_BYTE:
				{
					BYTE v;
					v = *(pP + offset);
					SetDlgItemInt(PEditor, peEditBox + x, v, false);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_SBYTE:
				{
					signed char v;
					v = *(signed char*)(pP + offset);
					SetDlgItemInt(PEditor, peEditBox + x, v, true);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_SWORD:
				{
					signed short v;
					signed short* s=(signed short*)(pP + offset);
					//v = *(pP + offset);
					v = *s;
					SetDlgItemInt(PEditor, peEditBox + x, v, true);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_INT:
				{
					INT32 v;
					INT32* s = (INT32*)(pP + offset);
					//v = *(pP + offset);
					v = *s;
					SetDlgItemInt(PEditor, peEditBox + x, v, true);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_ENUMASSIGN:
				{
					//search target
					BYTE* i = stack::data + CurrentPos; i += CmdDesc[cmd].Size;
					while (*i != 0 && (CmdDesc[*i].Routine == Envelope || CmdDesc[*i].Routine == Point))
					{
						i += CmdDesc[*i].Size;
					}

					EnableWindow(paramH[x].Edit, false);
					BYTE v = *(pP + offset);

					char* enumText = CmdDesc[*i].ParamName[v];
					SetWindowText(paramH[x].Edit, enumText);

					ShowWindow(paramH[x].Button, SW_SHOW | SW_SHOWNOACTIVATE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Button, 0, 120 - 66, step * x, h, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140-66, step * x, 55+66, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110-66, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_ENUM:
				{
					EnableWindow(paramH[x].Edit, false);
					BYTE i = *(pP + offset);

					char* enumText = CmdDesc[cmd].ParamEnum[x][i];
					SetWindowText(paramH[x].Edit, enumText);

					ShowWindow(paramH[x].Button, SW_SHOW|SW_SHOWNOACTIVATE);
					int step = PESTEP; int h = 20;
					int offset = 0;
					if (!strcmp(CmdDesc[cmd].ParamName[x], "shader")|| 
						!strcmp(CmdDesc[cmd].ParamName[x], "scene")||
						!strcmp(CmdDesc[cmd].ParamName[x], "texture") ) offset = 50;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Button, 0, 120-offset, step * x, h, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140-offset, step * x, 55+offset, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110-offset, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_LABEL:
				{
					EnableWindow(paramH[x].Edit, true);
					BYTE* Text = pP + offset;
					SetWindowText(paramH[x].Edit, (char*)Text);
					ShowWindow(paramH[x].Button, SW_HIDE );

					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140-100, step * x, 55+100, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 25, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					
					break;
				}
				case PT_TEXT:
					EnableWindow(paramH[x].Edit, false);
					ShowWindow(paramH[x].Button, SW_SHOW|SW_SHOWNOACTIVATE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_HIDEWINDOW );
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW );

					break;
				}
			}
			x++;
		}

		if (x == 0)
		{
			ShowWindow(PEditor, SW_HIDE ); return;
			SetFocus(pf);
		}
		int headerh = GetSystemMetrics(SM_CYCAPTION) - 7;
		if (cmd != LastShownCmd)
		{
			SetWindowPos(PEditor, HWND_TOP, r.right - 210, 32, 210, (x + 1)*PESTEP + headerh, SWP_SHOWWINDOW | SWP_NOMOVE );
		}
		else
		{
			SetWindowPos(PEditor, HWND_TOP, r.right - 210, 32, 210, (x + 1)*PESTEP + headerh, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOACTIVATE);
		}

		SetFocus(pf);

		LastShownCmd = cmd;
	}

	void WriteValue(int pNum,int value)
	{
		BYTE cmd = *(stack::data + CurrentPos);
		int type = CmdDesc[cmd].ParamType[pNum];
		int offset = CmdDesc[cmd].ParamOffset[pNum];
		switch (type)
		{
		case PT_BYTE:
			*(stack::data + CurrentPos+offset) = (BYTE)value;
			break;
		case PT_SBYTE:
			*(signed char*)(stack::data + CurrentPos + offset) = (signed char)value;
			break;

		case PT_SWORD:
		{
			//signed short* p = (signed short*)(stack::data + CurrentPos + offset);
			//*(stack::data + CurrentPos + offset) = (signed short)value;
			signed short* p = (signed short*)(stack::data + CurrentPos + offset);
			*p = (signed short)value;
//				*p = LOWORD(value);
//				*(p+1) = HIWORD(value);
			break;
		}
		case PT_INT:
		{
			//signed short* p = (signed short*)(stack::data + CurrentPos + offset);
			//*(stack::data + CurrentPos + offset) = (signed short)value;
			INT32* p = (INT32*)(stack::data + CurrentPos + offset);
			*p = (INT32)value;
			//				*p = LOWORD(value);
			//				*(p+1) = HIWORD(value);
			break;
		}
		case PT_TEXT:
			//todo*(stack::data + CurrentPos) = (BYTE)value;
			break;
		case PT_ENUM:
			*(stack::data + CurrentPos + offset) = (BYTE)value;
			break;
		case PT_ENUMASSIGN:
			*(stack::data + CurrentPos+offset) = (BYTE)value;
			break;
		}
	}

	BOOL CALLBACK PEditProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
	{

		if (isKeyDown(VK_MENU)&&GetFocus()!=hWnd) 
		{
			//SetFocus(hWnd); 
			return 0; 
		}

		switch (msg)
		{
			case WM_MOUSEWHEEL:
			{
				WORD fwKeys = GET_KEYSTATE_WPARAM(wParam);
				int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);

				if (zDelta != 0)
				{
					int i = 0;
					while (i < MAXPARAM&&GetFocus() != paramH[i].Edit) i++;
					BOOL valid;
					int v = GetDlgItemInt(hDlg, i + peEditBox, &valid, true);
					
					if (valid)
					{
						int mul = 1;

						if (isKeyDown(VK_CONTROL)) mul *= 10;
						if (isKeyDown(VK_SHIFT)) mul *= 100;

						int sign = 0;
						if (zDelta > 0) sign = 1;
						if (zDelta < 0) sign = -1;
						v += mul * sign;
						WriteValue(i, v);
						ShowParam();
					}
				}

			break;
			}

			case WM_COMMAND:
			{
				int control = LOWORD(wParam);
				//enum button
				if (control >= peButton && control <= peButton + MAXPARAM)
				{
					BYTE cmd = *(stack::data + CurrentPos);

					if (CmdDesc[cmd].ParamType[control - peButton] == PT_ENUMASSIGN)
					{
						//search target
						BYTE* i = stack::data + CurrentPos; i += CmdDesc[cmd].Size;
						while (*i != 0 && (CmdDesc[*i].Routine == Envelope || CmdDesc[*i].Routine == Point))
						{

							i += CmdDesc[*i].Size;
						}
						//get params
						if (*i != 0 && stack::data + CurrentPos != i)
						{
							//process menu
							int value = EnumMenuAssign(*(BYTE*)i, control - peButton);
							//write value
							if (value >= 0)
							{
								WriteValue(control - peButton, value);
							}
						}
					}

					if (CmdDesc[cmd].ParamType[control - peButton] == PT_ENUM)
					{
						int i=0;
						int value = 0;

						while ((BYTE*)*CmdDesc[cmd].ParamEnum[control - peButton][i] != 0)
						{
							i++;
						}

						if (i > 1)
						{

							if (i < 3)
							{
								int type = CmdDesc[cmd].ParamType[control - peButton];
								int offset = CmdDesc[cmd].ParamOffset[control - peButton];
								if (type == PT_ENUM)
								{
									value = *(stack::data + CurrentPos + offset);
									value = !value;
								}
							}
							else
							{
								value = EnumMenu(cmd, control - peButton);
							}

							if (value >= 0)
							{
								WriteValue(control - peButton, value);
							}
						}
					}

					if (CmdDesc[cmd].ParamType[control - peButton] == PT_TEXT)
					{

						TextEditorActivePtr = stack::data + CurrentPos+ CmdDesc[cmd].ParamOffset[control - peButton];
						TextEditorActiveMaxSize = TypeSizeTable[CmdDesc[cmd].ParamType[control - peButton]]-1;
						TextEditorActiveCmd = cmd;

						if (CmdDesc[cmd].Routine == Commands::CreateShader)
						{
							//BYTE n = *(stack::data + CurrentPos + CmdDesc[cmd].ParamOffset[1]);

							BYTE* sptr = stack::data; int shCounter = 0;
							while (*sptr != 0&&sptr<stack::data+CurrentPos)
							{
								if (CmdDesc[*sptr].Routine == Commands::CreateShader)
								{
									shCounter++;
								}
								sptr += CmdDesc[*sptr].Size;
							}


							TextEditorActiveShader = shCounter;
							//TextEditorActiveShader = n;
						}

						ShowWindow(TEditor, SW_SHOW);

						SetWindowText(TE_edit,(LPCSTR)TextEditorActivePtr);

					//	EnableWindow(hWnd, FALSE);
						/*int value = EnumMenu(cmd, control - peButton);
						if (value >= 0)
						{
							WriteValue(control - peButton, value);
						}*/
					}

					ShowParam();
				}

				if (control >= peEditBox && control <= peEditBox + MAXPARAM)
				{
					if (HIWORD(wParam) == EN_CHANGE)
					{
						BYTE cmd = *(stack::data + CurrentPos);
						int type = CmdDesc[cmd].ParamType[control - peEditBox];
						switch (type)
						{
							case PT_BYTE:
							case PT_SBYTE:
							{
								BOOL valid;
								int value=GetDlgItemInt(hDlg, control, &valid, true);
								if (valid) WriteValue(control - peEditBox, value);
								break;
							}
							case PT_INT:
							case PT_SWORD:
							{
								BOOL valid;
								int value = GetDlgItemInt(hDlg, control, &valid, true);
								if (valid) WriteValue(control - peEditBox, value);
								break;
							}
							case PT_LABEL:
							{
								int pnum = control - peEditBox;
								BYTE* strptr = stack::data + CurrentPos + CmdDesc[cmd].ParamOffset[pnum];

								GetDlgItemText(hDlg, control, (LPSTR)strptr, TypeSizeTable[type]-1);
								//WriteValue(control - peEditBox, value);
								break;
							}
						}
					}
				}
				break;
			}
				

		}

		return 0;
	}

	HWND hWndCat[50];
	void CreateButton(char* text, int x, int y, int w, int h)
	{
		hWndCat[ToolBoxCtrlCounter] = CreateWindow("BUTTON", text, WS_VISIBLE | WS_CHILD, x, y, w, h, ToolBox, (HMENU)(ToolBoxCtrlStartIndex + ToolBoxCtrlCounter), (HINSTANCE)GetWindowLong(ToolBox, GWL_HINSTANCE), NULL);
		SendMessage(hWndCat[ToolBoxCtrlCounter], WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);

		ToolBoxCtrlCounter++;

	}

	LPDLGTEMPLATE lpdt;
	void DialogTemplate()
	{
		HGLOBAL hgbl;
		LPWORD lpw;
		hgbl = GlobalAlloc(GMEM_ZEROINIT, 1024);
		lpdt = (LPDLGTEMPLATE)GlobalLock(hgbl);
		lpdt->style = WS_CAPTION | WS_POPUP;
		lpdt->cdit = 0;         // Number of controls
		lpdt->x = 0;  lpdt->y = 0;
		lpdt->cx = 0; lpdt->cy = 0;
		lpw = (LPWORD)(lpdt + 1);
		*lpw++ = 0;             // No menu
		*lpw++ = 0;
	}

	void initToolBox()
	{
		static DLGPROC lpfnDlgToolBox;
		lpfnDlgToolBox = (DLGPROC)MakeProcInstance((FARPROC)ToolProc, hInst);

		ToolBox = CreateDialogIndirect(hInst, lpdt, hWnd, lpfnDlgToolBox);

		ToolBoxCtrlCounter = 0;
		int h = 20; int y = 0; int hw = 2; int w = 50;
		CreateButton("Project", 0, y, w, h); y += h + hw;
		CreateButton("Scene", 0, y, w, h); y += h + hw;
		CreateButton("Cmd", 0, y, w, h); y += h + hw;
		CreateButton("->", 0, y, w, h); y += h + hw;
		CreateButton("3D", 0, y, w, h); y += h + hw;
		CreateButton("Particle", 0, y, w, h); y += h + hw;
		CreateButton("Sound", 0, y, w, h); y += h + hw;

		SetWindowPos(ToolBox, HWND_TOP, 0, 30, 65, 300, SWP_SHOWWINDOW );
		
		ShowWindow(ToolBox, SW_SHOW);
		UpdateWindow(ToolBox);
	}

	

	void initTextEditor()
	{

		static DLGPROC lpfnDlgTextEditor;
		lpfnDlgTextEditor = (DLGPROC)MakeProcInstance((FARPROC)EditBoxProc, hInst);
		lpdt->style = WS_CAPTION | WS_SYSMENU | WS_SIZEBOX;
		TEditor = CreateDialogIndirect(hInst, lpdt, hWnd, lpfnDlgTextEditor);

		TE_edit = CreateWindow("EDIT", "", WS_VISIBLE | WS_CHILD | ES_MULTILINE | WS_VSCROLL | ES_NOHIDESEL, 0,0, 0, 0, TEditor, (HMENU)(teEditBox), (HINSTANCE)GetWindowLong(TEditor, GWL_HINSTANCE), NULL);
		SendMessage(TE_edit, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);

		HFONT hFont = CreateFont(16, 6, 0, 0, FW_LIGHT, 0, 0, 0, 0, 0, 0, 0, 0, TEXT("Tahoma"));
		SendMessage(TE_edit, WM_SETFONT, (WPARAM)hFont, 0);

		TE_ok = CreateWindow("BUTTON", "", WS_VISIBLE | WS_CHILD| BS_MULTILINE, 0, 0, 0, 0, TEditor, (HMENU)(teOk), (HINSTANCE)GetWindowLong(TEditor, GWL_HINSTANCE), NULL);
		SendMessage(TE_ok, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);

	//	TE_cancel = CreateWindow("BUTTON", "CANCEL", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, TEditor, (HMENU)(teCancel), (HINSTANCE)GetWindowLong(TEditor, GWL_HINSTANCE), NULL);
	//	SendMessage(TE_cancel, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);

		SetWindowPos(TEditor, HWND_TOP, 70, 30, 400, 690, SWP_HIDEWINDOW);

		AdaptiveTESize();

		ShowWindow(TEditor, SW_HIDE);
		UpdateWindow(TEditor);
	}

	void initPEditor()
	{
		RECT r;
		GetWindowRect(hWnd, &r);
		static DLGPROC lpfnDlgPEditor;
		lpfnDlgPEditor = (DLGPROC)MakeProcInstance((FARPROC)PEditProc, hInst);

		PEditor = CreateDialogIndirect(hInst, lpdt, hWnd, lpfnDlgPEditor);

		int step = PESTEP; int h = 20;
		for (int x = 0; x < MAXPARAM; x++)
		{
			paramH[x].Text = CreateWindow("STATIC", "", WS_VISIBLE | WS_CHILD, 5, x*step+2, 110,h, PEditor, (HMENU)peName+x, (HINSTANCE)GetWindowLong(PEditor, GWL_HINSTANCE), NULL);
			paramH[x].Button = CreateWindow("BUTTON", "...", WS_VISIBLE | WS_CHILD, 120, x * step, 20, h, PEditor, (HMENU)(peButton+x), (HINSTANCE)GetWindowLong(PEditor, GWL_HINSTANCE), NULL);
			paramH[x].Edit = CreateWindowEx(WS_EX_CLIENTEDGE ,"EDIT", NULL, WS_VISIBLE | WS_CHILD | WS_BORDER |WS_TABSTOP, 140, step * x, 55, h, PEditor, (HMENU)(peEditBox + x), (HINSTANCE)GetWindowLong(PEditor, GWL_HINSTANCE), NULL);
				
			SendMessage(paramH[x].Text, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);
			SendMessage(paramH[x].Button, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);
			SendMessage(paramH[x].Edit, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);
		//	SendMessage(paramH[x].Edit, EM_SETREADONLY, false,NULL);
		}
		SetWindowText(PEditor, "Parameters");
		
		SetWindowPos(PEditor, HWND_TOP, r.right - 210, 32, 0, 0, SWP_NOSIZE);
		UpdateWindow(PEditor);
	}

	void init()
	{
		itemOldviewPos = new point2d[MAXDATASIZE];
		stackItemOldviewPos = new point2d[MAXDATASIZE];

		DialogTemplate();

		initToolBox();
		initPEditor();
		initTextEditor();//changes template!

		dxui::LoadFont();
		dxui::SetTextureOptions();
		dxui::SetBlendStates();
		dxui::ShadersInit();

		dxui::CreateConstBuf();
		dxui::VBInit();

		dxui::CreateConstBufTL();
		dxui::TimelineVBInit();

		dxui::CreateConstBufL();
		dxui::lineVBInit();
	}

	typedef struct {
		float x; float y; float w; float h;
	} bbox;

	bbox bPos[2][10000];//0 base stack 1 timeline ghost

	int copiedBytes = 0;

	void copySelected()
	{
		BYTE* i = stack::data;
		BYTE* j = stack::data2;
		int counter = 0;

		while (*i != 0)
		{
			BYTE cmd = *i;
			BYTE selFlag = *(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected);

			if (selFlag == 1)
			{
				if (CmdDesc[cmd].Routine == Envelope)
				{
					BYTE* f = i+CmdDesc[cmd].Size;
					while (*f != 0 && CmdDesc[*f].Routine == Point)
					{
						*(f + CmdDesc[*f].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
						f+= CmdDesc[*f].Size;
					}
				}

				if (CmdDesc[cmd].Routine == Clip)
				{
					BYTE cmd2 = *i;
					BYTE* f = i + CmdDesc[cmd].Size;
					BYTE cmd3 = *(BYTE*)f;
					while (*f != 0 && CmdDesc[*f].Routine == Note)
					{
						*(f + CmdDesc[*f].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
						f += CmdDesc[*f].Size;
					}
				}

			}

			if (selFlag == 1)
			{
				memcpy(j, i, CmdDesc[cmd].Size);
				j += CmdDesc[cmd].Size;
			}

			i += CmdDesc[cmd].Size;
		}
		copiedBytes=j- stack::data2;
	}

	bool paste()
	{
		Commands::loopPoint = stack::data;

		BYTE* i = stack::data;
		BYTE* j = stack::data2;
		bool pflag = false;
		bool nflag = false;

		if (CmdDesc[*(BYTE*)(i + CurrentPos)].Routine == Point)
		{
			int n = 0; 
			while (n<copiedBytes)
			{
				if (CmdDesc[*(BYTE*)(j + n)].Routine != Point) pflag = true;

				n += CmdDesc[*(BYTE*)(j + n)].Size;
			}
		}

		if (CmdDesc[*(BYTE*)(i + CurrentPos)].Routine == Note)
		{
			int n = 0;
			while (n<copiedBytes)
			{
				if (CmdDesc[*(BYTE*)(j + n)].Routine != Note) nflag = true;

				n += CmdDesc[*(BYTE*)(j + n)].Size;
			}
		}


		if (pflag||nflag)
		{
			return false;
		}

		memmove(i + copiedBytes+CurrentPos, i+CurrentPos, MAXDATASIZE  -CurrentPos- copiedBytes);
		memcpy(i + CurrentPos, j, copiedBytes);
		return true;
	}

	void removeSelection()
	{
		BYTE* i = stack::data;
		while (*i != 0)
		{
			BYTE cmd = *i;
			*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)0;
			i += CmdDesc[cmd].Size;
		}
		
	}

	void removeCmd(BYTE* i)
	{	
		memmove(i, i + CmdDesc[*i].Size, MAXDATASIZE + stack::data - i - CmdDesc[*i].Size);
		Commands::loopPoint = stack::data;
	}

	void removeSelected()
	{
		Commands::loopPoint = stack::data;
		BYTE* i = stack::data;
		int counter = 0;

		while (*i != 0)
		{
			BYTE cmd = *i;

			BYTE selFlag = *(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected);

			if ((CmdDesc[cmd].Routine == Envelope)&& (selFlag == 1))
			{
				BYTE* j = i+CmdDesc[cmd].Size;
				while (*j != 0 && CmdDesc[*j].Routine == Point)
				{
					*(j + CmdDesc[*j].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
					j += CmdDesc[*j].Size;
				}
			}

			if ((CmdDesc[cmd].Routine == Clip)&& (selFlag == 1))
			{
				BYTE* j = i + CmdDesc[cmd].Size;
				while (*j != 0 && CmdDesc[*j].Routine == Note)
				{
					*(j + CmdDesc[*j].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
					j += CmdDesc[*j].Size;
				}
			}



			if (selFlag == 1)
			{
				removeCmd(i);
				CurrentPos = min(i - stack::data,CurrentPos);
				lastSelectedI = min(counter,lastSelectedI);
			}
			else 
			{
				i += CmdDesc[cmd].Size;
			}

			counter++;
		}

		*(stack::data + CurrentPos +CmdDesc[*(stack::data + CurrentPos)].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
	}

	void SelectRange(int i1,int i2)
	{
		int start = min(i1, i2);
		int end = max(i1, i2);

		BYTE* i = stack::data;
		int counter = 0;
		while (*i != 0)
		{
			BYTE cmd = *i;
			if (counter >= start&&counter <=end)
			{
				*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE) 1;
			}
			i += CmdDesc[cmd].Size;
			counter++;
		}
	}

	int GetScenePosById(int id)
	{
		BYTE* ptr = stack::data;
		int c = 0;

		while (*ptr != 0)
		{
			if (CmdDesc[*ptr].Routine == Scene)
			{
				if (c == id) 
				{
					return ptr - stack::data;
				}
				
				c++;
			}

			ptr += CmdDesc[*ptr].Size;
		}

		return ui::CurrentPos;
	}

	void Select(int mode)
	{
		POINT p;
		GetCursorPos(&p);
		ScreenToClient(hWnd, &p);
		float x, y;
		x = p.x / float(dx::width) * 2 - 1;
		y = -(p.y / float(dx::height) * 2 - 1);
		x *= 1 / dx::aspect;

		BYTE* i = stack::data;
		int counter = 0;

		while (*i !=(BYTE) 0)
		{
			BYTE cmd = *i;

			float bx = bPos[0][counter].x;
			float by = bPos[0][counter].y;
			float bw = bPos[0][counter].w;
			float bh = bPos[0][counter].h;

			float bx1 = bPos[1][counter].x;
			float by1 = bPos[1][counter].y;
			float bw1 = bPos[1][counter].w;
			float bh1 = bPos[1][counter].h;

			if (((x > bx) && (x < (bx + bw)) && (y < by) && (y > (by - bh))) ||
				((x > bx1) && (x < (bx1 + bw1)) && (y < by1) && (y >(by1 - bh1))))
			{
				BYTE selectFlag = *(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected);

				if (mode == 0)
				{
					removeSelection();
					selectFlag=1;
					lastSelectedI = counter;
					CurrentPos=i- stack::data;
					*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)selectFlag;
					return;
				/*	BYTE ggg = *(stack::data + CurrentPos);
					int ggg2 = CmdDesc[ggg].Category;
					int a = 0;*/
				}

				if (mode == 1)//toggle
				{
					selectFlag = 1 - selectFlag;
					lastSelectedI = counter;
					CurrentPos = i - stack::data;
				}

				if (mode == 2)//add
				{
					selectFlag = 1;
					SelectRange(counter, lastSelectedI);
					lastSelectedI = counter;
					CurrentPos = i - stack::data;
				}

				*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)selectFlag;
			}

			if ((x > bx) && (x < (bx + bw)) && (y < by) && (y >(by - bh)))
			{
				SelectGhost = false;
			}

			if ((x > bx1) && (x < (bx1 + bw1)) && (y < by1) && (y > (by1 - bh1)))
			{
				SelectGhost = true;
			}


			i += CmdDesc[cmd].Size;
			counter++;
		}

	}

	bool GetTimeLineStatus(BYTE* i)
	{
		int cmd = *i;
		int x = 0;

		if (CmdDesc[cmd].Routine == Clip) return true;
		if (CmdDesc[cmd].Routine == Note) return true;

		if (CmdDesc[cmd].Routine == Envelope) return true;
		if (CmdDesc[cmd].Routine == Point) return true;

		if (CmdDesc[cmd].Routine == TimeLoop) return true;


		while ((BYTE*)*CmdDesc[cmd].ParamName[x] != 0)
		{
			int offset = CmdDesc[cmd].ParamOffset[x];
			char* name = CmdDesc[cmd].ParamName[x];

			if (!strcmp(name, "timeline"))
			{
				if (*(i + offset) == 1)
				{
					return true;
				}
			}
			x++;
		}

		return false;
	}

	int notePosX=0;
	float notePosY=0;

	char noteText[4]="   ";

	void CreateNoteText(BYTE p)
	{
		char* table = "C-0C#0D-0D#0E-0F-0F#0G-0G#0A-0A#0B-0";

		strcpy(noteText, " - ");
		if (p == 0) return;

		if (p < 12 * 8 + 1) //in range
		{
			p--;//skip nonote
			int octave = p / 12;
			char _octave[10];
			_itoa(octave, _octave, 10);
			int note = p - octave * 12;


			noteText[0] = table[note*3];
			noteText[1] = table[note * 3+1];
			noteText[2] = _octave[0];
		}

		//special notes
	}

	int npm=0;
	float clipw = 0;
	int cliprep = 0;


	float envelopePosX = 0;

	XMFLOAT4 GetXYWR(BYTE* i,int iter)//xyw in signed int format!
	{
		int cmd = *i;
		int x = 0;
		float tx, ty, tw, tr;
		tx = 0; ty = 0; tw = 0; tr = 0;
		if (npm == 0) npm = 1;
		int intNPM = (int)(3600 / (int)npm);

		if (CmdDesc[cmd].Routine == Note)
		{
			float h = .05f;
			float stepY = h*1.1f;

			tw = (float)intNPM;
			tx = (float)notePosX;
			ty = notePosY;
			tx *= .01f / 60.f; ty *= .01f / 60.f; tw *= .01f / 60.f;
			ty -= stepY;

			notePosX += intNPM;// *.01f / 60.f;;

			return XMFLOAT4(tx, ty, tw,0);
		}

		x = 0;
		while ((BYTE*)*CmdDesc[cmd].ParamName[x] != 0)
		{
			int offset = CmdDesc[cmd].ParamOffset[x];
			char* name = CmdDesc[cmd].ParamName[x];
			int s_ = 0;
			if (CmdDesc[cmd].ParamType[x] == PT_INT)
			{
				INT32* p = (INT32*)(i + offset);
				INT32 s = *p;
				s_ = s;
			}
			if (CmdDesc[cmd].ParamType[x] == PT_SWORD)
			{
				signed short* p = (signed short*)(i + offset);
				signed short s = *p;
				s_ = s;
			}
			if (CmdDesc[cmd].ParamType[x] == PT_BYTE)
			{
				BYTE* p = (BYTE*)(i + offset);
				BYTE s = *p;
				s_ = s;
			}

			if (!strcmp(name, "x"))	tx = float(s_);
			if (!strcmp(name, "y"))	ty = float(s_);
			if (!strcmp(name, "length") && CmdDesc[cmd].Routine == Clip) { tw = (float)(s_ * intNPM); clipw = tw * .01f / 60.f; }
			if (!strcmp(name, "length") && CmdDesc[cmd].Routine != Clip) { tw = (float)s_; }
			if (!strcmp(name, "npm")) npm = s_*4;
			if (!strcmp(name, "repeat")) { tr = float(s_); cliprep = s_;}
			x++;
		}

		if (CmdDesc[cmd].Routine == Clip)
		{
			notePosX = (int)tx;
			notePosY = ty;
		}

		if (CmdDesc[cmd].Routine == Envelope)
		{
			BYTE link = *(BYTE*)(i + CmdDesc[*i].ParamOffset[7]);

			if (link == 0)
				envelopePosX = tx;
			else
				envelopePosX = 0;

			
			tw = 300;
		}

		if (CmdDesc[cmd].Routine == Point)
		{
			tx += envelopePosX;
			tw = 300;
		}

		tx *= .01f / 60.f; ty *= .01f / 60.f; tw *= .01f / 60.f;



		return XMFLOAT4(tx, ty, tw, tr);

	}

	void CameraMovement()
	{
		POINT p;
		GetCursorPos(&p);
		ScreenToClient(hWnd, &p);
		float x, y;
		x = p.x / float(dx::width) * 2 - 1;
		y = -(p.y / float(dx::height) * 2 - 1);
		x *= 1 / dx::aspect;
		

		if (isKeyDown(VK_CONTROL)&&(isKeyDown(VK_LBUTTON)||isKeyDown(VK_RBUTTON)))
		{

			if (ui::cameradragFlag == 0)
			{
				ui::cameradragFlag = 1;
				
				dx::tempView = dx::View;
				
				if (isKeyDown(VK_LBUTTON)) ui::oldcameraCursor.x = x;
				if (isKeyDown(VK_RBUTTON)) ui::oldcameraCursor.y = y;
			}

			float xangle = 0;
			float yangle = 0;

			if (isKeyDown(VK_LBUTTON)) xangle =  x - ui::oldcameraCursor.x;
			if (isKeyDown(VK_RBUTTON)) yangle =  y - ui::oldcameraCursor.y;
			  
			 XMMATRIX rotationY = XMMatrixRotationY(xangle);
			 XMMATRIX rotationX = XMMatrixRotationX(yangle);
			 XMMATRIX t = XMMatrixTranslation(0, 0, -1);
			 XMMATRIX t2 = XMMatrixTranslation(0, 0, 1);
			 XMMATRIX temp = XMMatrixMultiply(dx::tempView, t);

			 XMMATRIX temp2 = XMMatrixMultiply(temp, rotationY);
			 XMMATRIX temp3 = XMMatrixMultiply(temp2, rotationX);


			 dx::View = XMMatrixMultiply(temp3, t2);
		}
		else
		{
			ui::cameradragFlag = 0;

		}
	}

	
	void StackMovement()
	{
		POINT p;
		GetCursorPos(&p);
		ScreenToClient(hWnd, &p);
		float x, y;
		x = p.x / float(dx::width) * 2 - 1;
		y = -(p.y / float(dx::height) * 2 - 1);
		x *= 1 / dx::aspect;
		//ui::NLog(Commands::cursor);

		if (isKeyDown(VK_CONTROL)) return;

		if (isKeyDown(VK_LBUTTON)&& isKeyDown(VK_MENU))
		{
			Commands::StartPosition = -playmode*(timer::GetCounter() - timer::StartTime) / (1.f / 60.f * 1000.f) + (600.0 / 618.6 * 12 * 1000 * (2 * (p.x / float(dx::width)) - 1) / timelineScale + (-60.f*100.f)*ui::timelinePos.x / ui::timelineScale);

			if (Commands::playmode == 1)
			{

				Commands::StopWave();
				Commands::PlayWave((int)Commands::StartPosition);
				timer::StartTime = timer::GetCounter();
			}
		}

		if (isKeyDown(VK_RBUTTON))
		{
			if (isKeyDown(VK_MENU))
			{
				if (ui::timelinedragFlag == 0)
				{
					ui::timelinedragFlag = 1;
					{
						ui::oldtimelinePos.x = ui::timelinePos.x;
						ui::oldtimelinePos.y = ui::timelinePos.y;
					}
					ui::oldtimelineCursor.x = x;
					ui::oldtimelineCursor.y = y;
				}
				ui::dragFlag = 0;
				ui::oldCursor.x = x;
				ui::oldCursor.y = y;
				ui::oldviewPos.x = ui::viewPos.x;
				ui::oldviewPos.y = ui::viewPos.y;

				ui::timelinePos.x = ui::oldtimelinePos.x + x - ui::oldtimelineCursor.x;
				ui::timelinePos.y = ui::oldtimelinePos.y + y - ui::oldtimelineCursor.y;


			}
			else
			{
				if (ui::dragFlag == 0)
				{
					ui::dragFlag = 1;
					ui::oldviewPos.x = ui::viewPos.x;
					ui::oldviewPos.y = ui::viewPos.y;
					ui::oldCursor.x = x;
					ui::oldCursor.y = y;
				}
				ui::timelinedragFlag = 1;
				ui::oldtimelineCursor.x = x;
				ui::oldtimelineCursor.y = y;
				ui::oldtimelinePos.x = ui::timelinePos.x;
				ui::oldtimelinePos.y = ui::timelinePos.y;

				ui::viewPos.x = ui::oldviewPos.x + x - ui::oldCursor.x;
				ui::viewPos.y = ui::oldviewPos.y + y - ui::oldCursor.y;
			}
		}
		else
		{
			ui::timelinedragFlag = 0;
			ui::dragFlag = 0;
		}
	}
	
	float currentEnvRange = 0;

	void ItemMovement()
	{
		if (isKeyDown(VK_MENU)) return;
		if (isKeyDown(VK_CONTROL)) return;

		POINT p;
		GetCursorPos(&p);
		ScreenToClient(hWnd, &p);
		float x, y;
		x = p.x / float(dx::width) * 2 - 1;
		y = -(p.y / float(dx::height) * 2 - 1);
		x *= 1 / dx::aspect;

		if (SelectGhost == false) return;

		if (
			(isKeyDown(VK_LBUTTON) && GetFocus() == hWnd&&ui::LButtonDown==true)&&
			(
				(CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Point)||
				(CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == Envelope)
			)
		   )
		{
			if (ui::itemDragFlag == 0)
			{
				ui::itemDragFlag = 1;

				BYTE* j = stack::data; int m = 0;

				if (CmdDesc[*(stack::data + CurrentPos)].Routine == Envelope)
				{
					ui::itemOldviewPos[0].x = *(short int*)(stack::data + CurrentPos + 1);
					ui::itemOldviewPos[0].y = *(short int*)(stack::data + CurrentPos + 3);
					ui::itemOldCursor.x = x;
					ui::itemOldCursor.y = y;
				}
				else
				{
					while (*(j) != 0)
					{
						if (CmdDesc[*j].Routine == Point&&*(BYTE*)(j + CmdDesc[*j].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
						{
							ui::itemOldviewPos[m].x = *(short int*)(j + 1);
							ui::itemOldviewPos[m].y = *(short int*)(j + 3);
							ui::itemOldCursor.x = x;
							ui::itemOldCursor.y = y;
						}
						j += CmdDesc[*j].Size; m++;
					}
				}
			}
				//ui::NLog(ui::itemOldviewPos.x);

				int i = 0;

				if (CmdDesc[*(stack::data + CurrentPos)].Routine == Envelope)
				{
					int _min = *(signed short*)(stack::data + CurrentPos + Envelope_min);
					int _max = *(signed short*)(stack::data + CurrentPos + Envelope_max);
					currentEnvRange = (float)(_max - _min);


				}
				else
				{
					while (*(BYTE*)(stack::data + i) != 0 && i < ui::CurrentPos)
					{
						BYTE cmd = *(BYTE*)(stack::data + i);
						if (CmdDesc[cmd].Routine == Envelope)
						{
							int _min = *(signed short*)(stack::data + i + Envelope_min);
							int _max = *(signed short*)(stack::data + i + Envelope_max);

							currentEnvRange =(float)( _max - _min);
						}
						i += CmdDesc[*(BYTE*)(stack::data + i)].Size;
					}
				}
		

		//	ui::itemViewPos.x = ui::itemOldviewPos.x + x - ui::itemOldCursor.x;
		//	ui::itemViewPos.y = ui::itemOldviewPos.y + y - ui::itemOldCursor.y;

			if (isKeyDown(VK_SHIFT))
			{
				if (fabs(x - ui::itemOldCursor.x) > fabs(y - ui::itemOldCursor.y))
				{
					BYTE* i = stack::data; int m = 0;

					if (CmdDesc[*(stack::data + CurrentPos)].Routine == Envelope)
					{
						*(short int*)(stack::data +CurrentPos + 1) = (short int)(ui::itemOldviewPos[0].x + (x - ui::itemOldCursor.x)*4.0*dx::aspect*float(dx::width) / ui::timelineScale * 2 * 1.065);
					}
					else
					{
						while (*i != 0)
						{
							if (CmdDesc[*i].Routine == Point&&*(BYTE*)(i + CmdDesc[*i].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
							{
								*(short int*)(i + 1) = (short int)(ui::itemOldviewPos[m].x + (x - ui::itemOldCursor.x)*4.0*dx::aspect*float(dx::width) / ui::timelineScale * 2 * 1.065);
								//*(short int*)(i + 3) = ui::itemOldviewPos[m].y + (y - ui::itemOldCursor.y)*currentEnvRange * 2;
							}
							i += CmdDesc[*i].Size; m++;
						}
					}
				}
				else
				{
					BYTE* i = stack::data; int m = 0;
					if (CmdDesc[*(stack::data + CurrentPos)].Routine == Envelope)
					{
						*(short int*)(stack::data+CurrentPos + 3) = (short int)(ui::itemOldviewPos[0].y + (y - ui::itemOldCursor.y)*currentEnvRange * 2);
					}
					else
					{
						while (*i != 0)
						{
							if (CmdDesc[*i].Routine == Point&&*(BYTE*)(i + CmdDesc[*i].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
							{
								//*(short int*)(i + 1) = ui::itemOldviewPos[m].x + (x - ui::itemOldCursor.x)*4.0*dx::aspect*float(dx::width) / ui::timelineScale * 2 * 1.065;
								*(short int*)(i + 3) = (short int)(ui::itemOldviewPos[m].y + (y - ui::itemOldCursor.y)*currentEnvRange * 2);
							}
							i += CmdDesc[*i].Size; m++;
						}
					}
				}
			}
			else
			{
				BYTE* i = stack::data; int m = 0;
				if (CmdDesc[*(stack::data + CurrentPos)].Routine == Envelope)
				{
					*(short int*)(stack::data + CurrentPos + 1) = (short int)(ui::itemOldviewPos[0].x + (x - ui::itemOldCursor.x)*4.0*dx::aspect*float(dx::width) / ui::timelineScale * 2 * 1.065);
					*(short int*)(stack::data + CurrentPos + 3) = (short int)(ui::itemOldviewPos[0].y + (y - ui::itemOldCursor.y)*currentEnvRange * 2);

				}
				else
				{
					while (*i != 0)
					{
						if (CmdDesc[*i].Routine == Point&&*(BYTE*)(i + CmdDesc[*i].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
						{
							*(short int*)(i + 1) = (short int)(ui::itemOldviewPos[m].x + (x - ui::itemOldCursor.x)*4.0f*dx::aspect*float(dx::width) / ui::timelineScale * 2 * 1.065f);
							*(short int*)(i + 3) = (short int)(ui::itemOldviewPos[m].y + (y - ui::itemOldCursor.y)*currentEnvRange * 2.f);
						}
						i += CmdDesc[*i].Size; m++;
					}
				}
			}

			if (fabs(x - ui::itemOldCursor.x) > 0 || fabs(y - ui::itemOldCursor.y) > 0)
			{
				ui::ShowParam();
			}
				//SetFocus(hWnd);
		}
		else
		{
		/*	if (ui::itemDragFlag == 1)
			{
				ui::ShowParam();
				SetFocus(hWnd);
			}*/
			ui::itemDragFlag = 0;
			//ui::ShowParam();
		}

		
		
	}

	void StackItemMovement()
	{
		if (isKeyDown(VK_MENU)) return;
		if (isKeyDown(VK_CONTROL)) return;

		POINT p;
		GetCursorPos(&p);
		ScreenToClient(hWnd, &p);
		float x, y;
		x = p.x / float(dx::width) ;
		y = -(p.y / float(dx::height));
		//NLog(x * 100);
		//x *= 1 / dx::aspect;

		if (
				(isKeyDown(VK_LBUTTON) && GetFocus() == hWnd&&ui::LButtonDown == true) &&
				(
					(CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == ColorPoint) ||
					(CmdDesc[*(BYTE*)(stack::data + CurrentPos)].Routine == fxPoint)
				)
			)
		{
			if (ui::stackItemDragFlag == 0)
			{
				ui::stackItemDragFlag = 1;
				BYTE* j = stack::data; int m = 0;
	
					while (*(j) != 0)
					{
						if (CmdDesc[*j].Routine == fxPoint&&*(BYTE*)(j + CmdDesc[*j].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
						{
							ui::stackItemOldviewPos[m].x = *(BYTE*)(j + 1);
							ui::stackItemOldviewPos[m].y = *(BYTE*)(j + 2);
							ui::stackItemOldCursor.x = x;
							ui::stackItemOldCursor.y = y;
						}

						if (CmdDesc[*j].Routine == ColorPoint&&*(BYTE*)(j + CmdDesc[*j].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
						{
							ui::stackItemOldviewPos[m].x = *(BYTE*)(j + 1);
							//ui::stackItemOldviewPos[m].y = *(BYTE*)(j + 2);
							ui::stackItemOldCursor.x = x;
							//ui::stackItemOldCursor.y = y;
						}


						j += CmdDesc[*j].Size; m++;
					}
				
			}

			

			{
				BYTE* i = stack::data; int m = 0;
				{
					while (*i != 0)
					{
						if (CmdDesc[*i].Routine == fxPoint&&*(BYTE*)(i + CmdDesc[*i].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
						{
							*(BYTE*)(i + 1) = (BYTE)clamp(ui::stackItemOldviewPos[m].x + 255.0f*(x - ui::stackItemOldCursor.x)*3.0f*2,0,100);
							*(BYTE*)(i + 2) = (BYTE)clamp(ui::stackItemOldviewPos[m].y + 255.0f*(y - ui::stackItemOldCursor.y)*3.0f*2,0,100);
						}

						if (CmdDesc[*i].Routine == ColorPoint&&*(BYTE*)(i + CmdDesc[*i].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
						{
							*(BYTE*)(i + 1) = (BYTE)clamp(ui::stackItemOldviewPos[m].x + 255.0f*(x - ui::stackItemOldCursor.x)*3.0f * 2.f, 0, 100);
						}

						i += CmdDesc[*i].Size; m++;
					}
				}
			}

			if (fabs(x - ui::stackItemOldCursor.x) > 0 || fabs(y - ui::stackItemOldCursor.y) > 0)
			{
				ui::ShowParam();
			}
		
		}
		else
		{
			ui::stackItemDragFlag = 0;
		}



	}

	float ui_clipleft = 0;
	int clipgrid = 0;
	float channelPeak[MAXCHANNELS];

	dxui::gradient grad1, grad2, grad3;
	int colorpointnum;
	XMFLOAT4 ContorCurveScreenPos;

	float channelYstart = .75;

	char* GetShaderName(BYTE shaderID)
	{
		int shaderCounter = 0;
		BYTE* p = stack::data;

		while (*p != 0 )
		{
			if (CmdDesc[*p].Routine == CreateShader)
			{
				if (shaderID == shaderCounter)
				{
					for (int x=0;x<MAXPARAM;x++)
					{
						if (!strcmp(CmdDesc[*p].ParamName[x], "name"))
						{
							return (char*)(p + CmdDesc[*p].ParamOffset[x]);
						}
					}
				}

				shaderCounter++;
			}

			p += CmdDesc[*p].Size;
		}

		return NULL;
	}


	char* GetResourceName(BYTE id,PVFN CreationRoutine,char* name)
	{
		int counter = 0;
		BYTE* p = stack::data;

		while (*p != 0 )
		{
			if (CmdDesc[*p].Routine == CreationRoutine)
			{
				if (id == counter)
				{
					for (int x = 0; x<MAXPARAM; x++)
					{
						if (!strcmp(CmdDesc[*p].ParamName[x], name))
						{
							return (char*)(p + CmdDesc[*p].ParamOffset[x]);
						}
					}
				}

				counter++;
			}

			p += CmdDesc[*p].Size;
		}

		return NULL;
	}

	char* GetShaderPtr(BYTE shaderID)
	{
		int shaderCounter = 0;
		BYTE* p = stack::data;

		while (*p != 0 )
		{
			if (CmdDesc[*p].Routine == CreateShader)
			{
				if (shaderID == shaderCounter)
				{
					for (int x = 0; x<MAXPARAM; x++)
					{
						if (!strcmp(CmdDesc[*p].ParamName[x], "data"))
						{
							return (char*)(p + CmdDesc[*p].ParamOffset[x]);
						}
					}
				}

				shaderCounter++;
			}

			p += CmdDesc[*p].Size;
		}

		return NULL;
	}

	float stepY;
	float LevelStepRight;
	int envelopeLineCount;
	float h = .05f;
	int counter;
	float bx ;
	float by ;
	float bw ;
	float bh ;

	void DrawControlCurve(BYTE* i)
	{
		BYTE cmd = *i;
		BYTE* p = i + CmdDesc[cmd].Size;
		int cnt = 0;
		while (*p != 0 && CmdDesc[*p].Routine == ColorPoint && cnt<GradientMaxPoints)
		{
			grad1.GradientColor[cnt].x = *(BYTE*)(p + CmdDesc[*p].ParamOffset[1]) / 255.0f;
			grad1.GradientColor[cnt].y = *(BYTE*)(p + CmdDesc[*p].ParamOffset[2]) / 255.0f;
			grad1.GradientColor[cnt].z = *(BYTE*)(p + CmdDesc[*p].ParamOffset[3]) / 255.0f;
			grad1.GradientColor[cnt].w = 1.;
			grad1.GradientPos[cnt].x = *(BYTE*)(p + CmdDesc[*p].ParamOffset[0]) / 100.0f;

			grad2.GradientColor[cnt].x = 1.;
			grad2.GradientColor[cnt].y = 1.;
			grad2.GradientColor[cnt].z = 1.;
			grad2.GradientColor[cnt].w = *(BYTE*)(p + CmdDesc[*p].ParamOffset[4]) / 255.0f;
			grad2.GradientPos[cnt].x = *(BYTE*)(p + CmdDesc[*p].ParamOffset[0]) / 100.0f;

			grad3.GradientColor[cnt].x = *(BYTE*)(p + CmdDesc[*p].ParamOffset[1]) / 255.0f;
			grad3.GradientColor[cnt].y = *(BYTE*)(p + CmdDesc[*p].ParamOffset[2]) / 255.0f;
			grad3.GradientColor[cnt].z = *(BYTE*)(p + CmdDesc[*p].ParamOffset[3]) / 255.0f;
			grad3.GradientColor[cnt].w = *(BYTE*)(p + CmdDesc[*p].ParamOffset[4]) / 255.0f;
			grad3.GradientPos[cnt].x = *(BYTE*)(p + CmdDesc[*p].ParamOffset[0]) / 100.0f;

			p += CmdDesc[*p].Size;
			cnt++;
		}
		if (cnt > 0)
		{
			grad1.GradientPosCount = (float)cnt;
			grad2.GradientPosCount = (float)cnt;
			grad3.GradientPosCount = (float)cnt;
			float w = .125f / 10.f;
			dxui::Box(bx + w, by - stepY * 2.f, 1.f / 3.f, bh, XMFLOAT4(1, 1, 1, 1), .001f, &grad1);
			dxui::Box(bx + w, by - stepY * 3.f, 1.f / 3.f, bh, XMFLOAT4(1, 1, 1, 1), .001f, &grad2);
			dxui::Box(bx + w, by - stepY * 4.f, 1.f / 3.f, bh, XMFLOAT4(1, 1, 1, 1), .001f, &grad3);
		}

		colorpointnum = 0;
		ContorCurveScreenPos.x = bx;
		ContorCurveScreenPos.y = by;
		ContorCurveScreenPos.z = bw;
		ContorCurveScreenPos.w = bh;

		cnt = 0;
		p = i + CmdDesc[cmd].Size;

		ui::point2d points[5];

		while (*p != 0 && cnt<5 && (CmdDesc[*p].Routine == fxPoint || (CmdDesc[*p].Routine == ColorPoint)))
		{
			if (CmdDesc[*p].Routine == fxPoint)
			{
				points[cnt].x = *(p + CmdDesc[*p].ParamOffset[0]);
				points[cnt].y = *(p + CmdDesc[*p].ParamOffset[1]);
				cnt++;
			}
			p += CmdDesc[*p].Size;
		}


		for (int n = 0; n<cnt - 1; n++)
		{

			float _bx = ContorCurveScreenPos.x + points[n].x / 100.0f / 3.f;
			float _by = ContorCurveScreenPos.y - stepY * 7 - bh / 4.f - (1 - points[n].y) / 100.f / 8.0f;
			float _bx1 = ContorCurveScreenPos.x + points[n + 1].x / 100.0f / 3.f;
			float _by1 = ContorCurveScreenPos.y - stepY * 7 - bh / 4.f - (1 - points[n + 1].y) / 100.f / 8.0f;

			dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(_bx + .0125f, _by - .0125f, 0.0f);
			dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(_bx1 + .0125f, _by1 - .0125f, 0.0f);

		}

	}

	void DrawColorPoint(BYTE* i)
	{
		BYTE cmd = *i;
		int cnt = colorpointnum;
		XMFLOAT4 c = grad1.GradientColor[cnt];
		colorpointnum++;

		BYTE x = *(i + CmdDesc[cmd].ParamOffset[0]);
		float w = ContorCurveScreenPos.z;

		bx = ContorCurveScreenPos.x + x / 100.0f / 3.f;
		by = ContorCurveScreenPos.y - stepY - bh / 4.f;
		bw = .025f;
		bh = .025f;
		bPos[0][counter].x = bx; bPos[0][counter].y = by; bPos[0][counter].w = bw; bPos[0][counter].h = bh;
		bPos[1][counter].x = bx; bPos[1][counter].y = by; bPos[1][counter].w = bw; bPos[1][counter].h = bh;

		dxui::Box(bx + bw / 2.f - .002f, by + bh * 1.5f / 7.5f, 0.004f, stepY * 4.f, colorScheme.colorpoint, .001f);

		if (CmdDesc[*(stack::data + CurrentPos)].Routine == ColorPoint &&
			(i == stack::data + CurrentPos))
		{
			dxui::Box(bx - bw * 1.5f / 7.5f, by + bh * 1.5f / 7.5f, bw * 1.5f, bh * 1.5f, colorScheme.colorpoint, .25f);
		}
	}

	void DrawFxPoint(BYTE* i)
	{
		BYTE cmd = *i;
		BYTE x = *(i + CmdDesc[cmd].ParamOffset[0]);
		BYTE y = *(i + CmdDesc[cmd].ParamOffset[1]);

		float w = ContorCurveScreenPos.z;

		bx = ContorCurveScreenPos.x + x / 100.0f / 3.f;
		by = ContorCurveScreenPos.y - stepY * 7.f - bh / 4.f - (1 - y) / 100.0f / 8.f;
		bw = .025f;
		bh = .025f;
		bPos[0][counter].x = bx; bPos[0][counter].y = by; bPos[0][counter].w = bw; bPos[0][counter].h = bh;
		bPos[1][counter].x = bx; bPos[1][counter].y = by; bPos[1][counter].w = bw; bPos[1][counter].h = bh;


		if (CmdDesc[*(stack::data + CurrentPos)].Routine == fxPoint &&
			(i == stack::data + CurrentPos))
		{
			dxui::Box(bx - bw * 1.5f / 7.5f, by + bh * 1.5f / 7.5f, bw * 1.5f, bh * 1.5f, colorScheme.fxpoint, .001f);
		}
	}

	//DANGER ZONE. TODO: rewrite complitely
	void ShowStack()
	{
		stepY = h * 1.1f;
		LevelStepRight = stepY / 2.;
	
		envelopeLineCount = 0;
		point2d envelopePrevPoint;
		BYTE* envelopePrevPointPosInStack;
		float envelopeLoopLenght = 0;
		colorpointnum = 0;

		float channelY = channelYstart;
		float channelStep = 2.3f;
		float envH = .125f;

		for (int ch = 0; ch < MAXCHANNELS; ch++)
		{
			sound::channelVisible[ch] = false;
		}

		POINT p;
		GetCursorPos(&p);
		ScreenToClient(hWnd, &p);
		float x, y;
		x = p.x / float(dx::width) * 2.f - 1;
		y = -(p.y / float(dx::height) * 2.f - 1);
		x *= 1.f / dx::aspect;

		StackMovement();
		ItemMovement();
		StackItemMovement();
		CameraMovement();

		dxui::SetupDrawerTimeline();
		dxui::Linelist(timelinePos.x, 0.0f, timelineScale, 1.f, colorScheme.timelinelines);	

		dxui::SetupDrawerBox();

		float tlc = (float)((Commands::cursorF)*ui::timelineScale) / (-60.f*100.f) - timelinePos.x;

		if (isKeyDown(VK_MENU)||playmode == 1)
			dxui::Box(0.f-tlc, 1.f, .005f, 2.f, colorScheme.cursor,.001f);
		else
			dxui::Box(0.f-tlc, -.95f, .005f, .15f, colorScheme.cursor,.001f);


		float tx, ty;
		tx = ui::viewPos.x;
		ty = ui::viewPos.y;
		BYTE* i=stack::data; 
		

		float offsetX = h/3.f;
		float offsety = (stepY-h)/4.f;
	    counter = 0;
		int currentChannel = 0;
		int currentChannel_ = 0;
		bool MusicCmd=false;
		bool StayOnEnv = false;
		bool MusicContainerStarted = false;
		bool AnyMusicCmd = false;
		int notecounter = 0;

		signed short env_min = 0;
		signed short env_max = 0;

		bool envOpen = false;
		bool AnyEnvOpen = false;
		BYTE link;
		int textureCounter = 0;


		//boxes
		while (*i != (BYTE)0)
		{
			BYTE cmd = *i;

			if (CmdDesc[cmd].Category != CAT_SOUND) tx = ui::viewPos.x + CmdDesc[cmd].Level*LevelStepRight;

			float w = dxui::StringLen(.05f, CmdDesc[cmd].Name);
			XMFLOAT4 c = CmdDesc[cmd].Color;
			XMFLOAT4 cR = CmdDesc[cmd].Color;
			bx = tx - offsetX / 4.f;
			by = ty;
			bw = w*2.f + offsetX;
			bh = .05f;

			for (int bc = 0; bc < 2; bc++)
			{
				bPos[bc][counter].x = bx; bPos[bc][counter].y = by; bPos[bc][counter].w = bw; bPos[bc][counter].h = bh;
			}

			if (*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
			{
				c = colorScheme.selectedbox;
			}

			if (CmdDesc[cmd].Routine == ControlCurve)
			{
				DrawControlCurve(i);
			}

			if (CmdDesc[cmd].Routine == ColorPoint)
			{
				c = grad1.GradientColor[colorpointnum];
				DrawColorPoint(i);
			}

			if (CmdDesc[cmd].Routine == fxPoint)
			{
				DrawFxPoint(i);
			}

			if (CmdDesc[cmd].Category == CAT_SOUND)
			{
				MusicCmd = true; AnyMusicCmd = true;
			}
			else
			{
				if (CmdDesc[cmd].Level<=0 && CmdDesc[cmd].Routine!=Envelope)
				{
					MusicCmd = false;
				}
			}

			if (CmdDesc[cmd].Routine == Channel)
			{
				currentChannel_ = *(BYTE*)(i + 1);
				BYTE c = *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
				sound::channelVisible[currentChannel_] = c;
				MusicContainerStarted = true;

				if (sound::channelVisible[currentChannel_] == true)
				{
					channelY -= stepY * channelStep;
			
				}
			}

			if (CmdDesc[cmd].Routine == Mixer)
			{
				currentChannel_ = MAXCHANNELS-1;
				BYTE c = *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
				sound::channelVisible[currentChannel_] = c;
				MusicContainerStarted = true;
			}

			if (CmdDesc[cmd].Level == 1 || 
				(MusicContainerStarted == true && 
					sound::channelVisible[currentChannel_] == false && 
					MusicCmd == true && 
					CmdDesc[cmd].Routine != Channel &&
					CmdDesc[cmd].Routine != Mixer))
			{
				bPos[0][counter].x = 0; bPos[0][counter].y = 0; bPos[0][counter].w = 0; bPos[0][counter].h = 0;	
			}
			else
			{
				if (MusicCmd == true)
				{
					if (sound::channelVisible[currentChannel_] == true && CmdDesc[cmd].Routine != Channel && CmdDesc[cmd].Routine != Mixer)
					{
						bPos[0][counter].x += .05f;
						bx += .05f;
					}
					if (CmdDesc[cmd].Routine == Channel)
					{
						float amp = Commands::channelVolume[currentChannel_];
						if (amp == 0) 
						{
							if (*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
							{
								dxui::Box(bx, by, bw, bh, colorScheme.selectedbox);
							}
							else
							{
								dxui::Box(bx, by, bw, bh, colorScheme.mutedchannel);
							}
						}
						else
						{
							if (*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
							{

								dxui::Box(bx, by, bw, bh, colorScheme.selectedbox);
							}
							else
							{
								dxui::Box(bx, by, bw, bh, colorScheme.activechannel);
							}
						}

						float v = (float)fabs((float)*(signed short*)sound::channel[currentChannel_]+rand()%Commands::framelen*4);
						float v2 = (float)fabs((float)*(signed short*)(sound::channel[currentChannel_]+2+rand() % Commands::framelen * 4));
						v = (v+v2)*.5f;
						channelPeak[currentChannel_] = lerp(channelPeak[currentChannel_], v, .1f);
						if (playmode == 1)
						{
							dxui::Box(bx, by, bw*channelPeak[currentChannel_] / 32768.0f, bh, colorScheme.peakmeter, .01f);
						}
					}
					else
					{
			
						dxui::Box(bx, by, bw, bh, c);
					
					}

					ty -= stepY;
				}
				else
				{
					if (CmdDesc[cmd].Routine == ColorPoint|| CmdDesc[cmd].Routine == fxPoint  )
					{
						dxui::Box(bx, by, bw, bh, c ,.25);
					}
					else
					{
						dxui::Box(bx, by, bw, bh, c);

						if (CmdDesc[cmd].Routine==Commands::CreateTexture)
						{
							dxui::SetupDrawerTextureView();
							dx::g_pImmediateContext->PSSetShaderResources(0, 1, &dx::TextureResView[textureCounter]);
							dxui::Box(bx - .12, by, .1, .1, XMFLOAT4(0, 0, 0, 0));
							dxui::SetupDrawerBox();
							ty -= stepY;
							textureCounter++;
						}

						ty -= stepY;
					}
					
					if (CmdDesc[cmd].Routine ==ControlCurve) ty -= stepY*7;

				}
			}
			
			//timeline ghost

			if (GetTimeLineStatus(i) == true)
			{
				XMFLOAT4 xyw = GetXYWR(i, 0);
				xyw.x *= timelineScale;
				xyw.x += timelinePos.x;

				float clikpos = (float)( -playmode*(timer::GetCounter() - timer::StartTime) / (1.f / 60.f * 1000.f) + (600.0f / 618.6f * 12.f * 1000.f * (2.f * (p.x / float(dx::width)) - 1.0f) / timelineScale + (-60.f*100.f)*ui::timelinePos.x / ui::timelineScale));

				if ((sound::channelVisible[currentChannel_] == true && MusicCmd ==true ) || (CmdDesc[cmd].Category != CAT_SOUND))
				{

					bPos[1][counter].x = xyw.x;
					bPos[1][counter].y = xyw.y;
					bPos[1][counter].w = xyw.z*timelineScale;
					bPos[1][counter].h = bh;

					if (CmdDesc[cmd].Routine == Envelope||CmdDesc[cmd].Routine == Point)
					{
						bPos[1][counter].x = 0;
						bPos[1][counter].y = 0;
						bPos[1][counter].w = 0;
						bPos[1][counter].h = 0;
					}

					if (CmdDesc[cmd].Routine == Envelope)
					{
						envOpen = *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
						if (envOpen) AnyEnvOpen = true;
						
					}

					if (!(CmdDesc[cmd].Routine == Envelope || CmdDesc[cmd].Routine == Point || CmdDesc[cmd].Routine == TimeLoop))
					{

						if (CmdDesc[cmd].Routine == Clip) 
						{ 
							xyw.z *= xyw.w; bPos[1][counter].w *= xyw.w;
						}

						if (CmdDesc[cmd].Routine == Note)
						{
							XMFLOAT4 cN = c;
							float highlight = 1;
							if (clipgrid >= 1)
							{
								highlight =(float)( notecounter%clipgrid);
								if (highlight == 0)
								{
									if (*(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected)!=1)
									{
										cN = colorScheme.notestrong;
									}
								}
							}
							notecounter++;

							xyw.y=	channelY - stepY;
							bPos[1][counter].y = xyw.y;

							dxui::Box(xyw.x, xyw.y, xyw.z*timelineScale - 0.005f, bh, cN, .001f);
							float h = bh*((float)(*(BYTE*)(i+CmdDesc[cmd].ParamOffset[1]))) / 255.0f;
							float pan = -((float)(*(signed char*)(i + CmdDesc[cmd].ParamOffset[3]))) / 127.0f/2.f;
							dxui::Box(xyw.x, xyw.y+h, xyw.z*timelineScale - 0.005f, h, colorScheme.notevolume, .001f);
							dxui::Box(xyw.x - ((pan+.5f)*xyw.z*timelineScale) + xyw.z*timelineScale - 0.005f, xyw.y + h, .005f, h, colorScheme.notepan, .001f);
						}
						else
						{
							xyw.y = channelY;
							bPos[1][counter].y = channelY;

							dxui::Box(xyw.x, xyw.y, xyw.z*timelineScale - 0.005f, bh, c, .2f);
						}

					}

					if (CmdDesc[cmd].Routine == TimeLoop)
						{
						BYTE on = *(BYTE*)(i + CmdDesc[*i].ParamOffset[0]);

							bPos[1][counter].y -= .95f;
							bPos[1][counter].h /= 2.0f;
							if (on == 0) dxui::Box(xyw.x , bPos[1][counter].y, xyw.z*timelineScale - 0.005f, bPos[1][counter].h, colorScheme.loopoff,.001f);
							if (on == 1) dxui::Box(xyw.x, bPos[1][counter].y, xyw.z*timelineScale - 0.005f, bPos[1][counter].h, colorScheme.loopon, .001f);
						}

					if (CmdDesc[cmd].Routine == Clip)
					{
						xyw.y = channelY;
						bPos[1][counter].y= channelY;

						notecounter = 0;
						ui_clipleft = xyw.x- timelinePos.x;
						clipgrid = *(BYTE*)(i + CmdDesc[cmd].ParamOffset[5]);
						for (int rep = 1; rep <= xyw.w; rep++)
						{
						//	dxui::Box(xyw.x + (xyw.z*timelineScale)*rep, xyw.y, xyw.z*timelineScale - 0.005f, bh, colorScheme.clipcopy,.001);
						}
					}

					if (CmdDesc[cmd].Routine == Note)
					{
						for (int rep = 1; rep <= cliprep; rep++)
						{
						//	dxui::Box(xyw.x + (clipw*timelineScale)*rep, xyw.y, xyw.z*timelineScale - 0.005f, bh, colorScheme.clipcopy, 0.001);
						}
					}

					if ((CmdDesc[*(stack::data + CurrentPos)].Routine == Point) || (CmdDesc[*(stack::data + CurrentPos)].Routine == Envelope))
					{
						StayOnEnv = true;

					}

					if ((CmdDesc[cmd].Routine == Envelope&&envOpen && MusicCmd == false)||
						(CmdDesc[cmd].Routine == Envelope&&envOpen && MusicCmd==true && sound::channelVisible[currentChannel_] ==true))
					{
						link = *(BYTE*)(i + CmdDesc[*i].ParamOffset[8]);
						
						if (link == 1)
							xyw.x += ui_clipleft;

						bPos[1][counter].x = xyw.x;
						bPos[1][counter].y = xyw.y;
						bPos[1][counter].w = xyw.z*timelineScale;
						bPos[1][counter].h = bh;

						env_min = *(signed short*)(i + Envelope_min);
						env_max = *(signed short*)(i + Envelope_max);
						signed short _vy = *(signed short*)(i + Envelope_y);
						float y = (float)(_vy - env_min);
						float range = (float)(env_max - env_min);
						if (range != 0) 
						{
							y /= range;
							y -= .5f;
							y *= 2.f;
							y *= envH;
						}
						float envY = channelY - stepY * 2.1f-envH;
						xyw.y = y+envY;
						
						bPos[1][counter].y = xyw.y;
						
						dxui::Box(xyw.x - (xyw.z - 0.005f) / 4.f, xyw.y + bh / 4.f, (xyw.z - 0.005f) / 2.f, bh / 2.f, c,.25f);
						bPos[1][counter].w = xyw.z;

						bPos[1][counter].x -= (xyw.z - 0.005f) / 4.f;
						bPos[1][counter].y += bh / 4.f;
						bPos[1][counter].w /= 2.;
						bPos[1][counter].h /= 2.;
					}


					if ((CmdDesc[cmd].Routine == Point&&envOpen && MusicCmd==false) ||
						(CmdDesc[cmd].Routine == Point&&envOpen && MusicCmd == true && sound::channelVisible[currentChannel_] == true))
					{
						if (link == 1)
						{
							xyw.x += ui_clipleft;
						}
						bPos[1][counter].x = xyw.x;
						bPos[1][counter].y = xyw.y;
						bPos[1][counter].w = xyw.z*timelineScale;
						bPos[1][counter].h = bh;

						signed short _vy = *(signed short*)(i + Point_y);
						float y = (float)(_vy - env_min);
						float range =(float)( env_max - env_min);
						if (range != 0)
						{
							y /= range;
							y -= .5f;
							y *= 2.f;
							y *= envH;
						}
					
						float envY = channelY - stepY * 2.1f-envH;
						xyw.y = y +envY;
						
						bPos[1][counter].y = xyw.y;
						dxui::Box(xyw.x -(xyw.z - 0.005f) / 4.f, xyw.y+bh/4.f, (xyw.z - 0.005f)/2.f, bh/2.f, c,.25f);
						bPos[1][counter].w = xyw.z;

						bPos[1][counter].x -= (xyw.z - 0.005f) / 4.f;
						bPos[1][counter].y += bh / 4.f;
						bPos[1][counter].w /= 2.;
						bPos[1][counter].h /= 2.;
					}

					
					if ((CmdDesc[cmd].Routine == Envelope&&envOpen && MusicCmd == false) ||
						(CmdDesc[cmd].Routine == Envelope&&envOpen && MusicCmd == true && sound::channelVisible[currentChannel_] == true))

					{

							float envY = channelY - stepY * 2.1f;
							dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(-2, envY, 0.0);
							dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(2,  envY, 0.0);
							dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(-2, envY - envH*2, 0.0);
							dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(2,  envY - envH*2, 0.0);
							dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(-2, envY - envH, 0.0);
							dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(2,  envY - envH, 0.0);
							

						envelopePrevPoint.x = xyw.x;
						envelopePrevPoint.y = xyw.y;
						envelopePrevPointPosInStack = i;

						BYTE* ptr = i;
						ptr += CmdDesc[cmd].Size;
						
						float pX = xyw.x ;
						while (CmdDesc[*ptr].Routine == Point)
						{
							BYTE pcmd = *ptr;
							int k = 0;
							while ((BYTE*)*CmdDesc[pcmd].ParamName[k] != 0)
							{
								int offset = CmdDesc[pcmd].ParamOffset[k];
								char* name = CmdDesc[pcmd].ParamName[k];
								int s_ = 0;
								if (CmdDesc[pcmd].ParamType[k] == PT_INT)
								{
									INT32* pt = (INT32*)(ptr + offset);
									INT32 s = *pt;
									s_ = s;
								}
								if (CmdDesc[pcmd].ParamType[k] == PT_SWORD)
								{
									signed short* pt = (signed short*)(ptr + offset);
									signed short s = *pt;
									s_ = s;
								}
								if (CmdDesc[pcmd].ParamType[k] == PT_BYTE)
								{
									BYTE* pt = (BYTE*)(ptr + offset);
									BYTE s = *pt;
									s_ = s;
								}

								if (!strcmp(name, "x"))	envelopeLoopLenght = float(s_) * .01f / 60.f;;
								k++;
							}

							ptr += CmdDesc[*ptr].Size;
						}

					}

					if ((CmdDesc[cmd].Routine == Point&&envOpen && MusicCmd == false) ||
						(CmdDesc[cmd].Routine == Point&&envOpen && MusicCmd == true && sound::channelVisible[currentChannel_] == true))

					{

						{
							float lp = envelopeLoopLenght*0;
							lp *= timelineScale;

							if (!((envelopePrevPoint.x + lp > 1. / dx::aspect)||
								(xyw.x + lp < -1. / dx::aspect)))
							{
								dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(envelopePrevPoint.x + lp, envelopePrevPoint.y, 0.0);
								dxui::envelopeVertices[envelopeLineCount++].Pos = XMFLOAT3(xyw.x + lp, xyw.y, 0);
							}
						}

						if (envelopePrevPoint.x > xyw.x )
						{

							if (i == stack::data + CurrentPos)
							{
								signed short prevP = *(signed short*)(envelopePrevPointPosInStack + Point_x);
								*(signed short*)(i + Point_x) = prevP;
							}

							if (envelopePrevPointPosInStack == stack::data + CurrentPos)
							{
								signed short prevP = *(signed short*)(i + Point_x);
								*(signed short*)(envelopePrevPointPosInStack + Point_x) = prevP;
							}

							ui::ShowParam();
							
						}

						envelopePrevPoint.x = xyw.x;
						envelopePrevPoint.y = xyw.y;
						envelopePrevPointPosInStack = i;

						if (*(i + CmdDesc[*i].Size) != 0 && CmdDesc[*(i + CmdDesc[*i].Size)].Routine != Point)
						{
							channelY -= envH * 2.1f;
						}
					}
				}

			}

			if (CmdDesc[*i].Routine == Scene && *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode) == 0)
			{
				i += CmdDesc[*i].Size;
				//ty -= stepY;
				counter++;
				while (*(i) != 0 &&
					CmdDesc[*(i)].Routine != EndScene &&
					CmdDesc[*(i)].Routine != Scene)
				{
					char *cn = CmdDesc[*i].Name;
					bPos[0][counter].x = 0;
					bPos[0][counter].y = 0;
					bPos[0][counter].w = 0;
					bPos[0][counter].h = 0;
					bPos[1][counter].x = 0;
					bPos[1][counter].y = 0;
					bPos[1][counter].w = 0;
					bPos[1][counter].h = 0;

					i += CmdDesc[*i].Size;
					counter++;
					//ty -= stepY;
				}
				if (CmdDesc[*(i)].Routine == EndScene)
				{
					bPos[0][counter].x = 0;
					bPos[0][counter].y = 0;
					bPos[0][counter].w = 0;
					bPos[0][counter].h = 0;
					bPos[1][counter].x = 0;
					bPos[1][counter].y = 0;
					bPos[1][counter].w = 0;
					bPos[1][counter].h = 0;
				}

				cmd = *i;
				char *cn = CmdDesc[*i].Name;
				int a = 0;
			}

			if (CmdDesc[cmd].Routine == EndDraw) ty-=stepY;

			i += CmdDesc[cmd].Size;
			counter++;

		}

		dxui::SetupDrawer();
		 i = stack::data;
		 tx = (float)ui::viewPos.x;
		 ty = (float)ui::viewPos.y;

		 MusicCmd = false;
		 envOpen = false;
		 AnyEnvOpen = false;

		 for (int ch = 0; ch < MAXCHANNELS; ch++)
		 {
			 sound::channelVisible[ch] = false;
		 }
		 currentChannel_ = 0;
		 MusicContainerStarted = false;

		 channelY = channelYstart;
		 BYTE* chname;
		 int tcounter = 0;

		 //text
		while (*i != (BYTE)0)
		{
			BYTE cmd = *i;

			if (CmdDesc[cmd].Category!=CAT_SOUND) tx = ui::viewPos.x + CmdDesc[cmd].Level*LevelStepRight;

			XMFLOAT4 c = colorScheme.font;
			if (*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
			{
				c = colorScheme.selectedfont;
			}

			if (CmdDesc[cmd].Category == CAT_SOUND )
			{
				MusicCmd = true;
			}
			else
			{
				if (CmdDesc[cmd].Level == 0 && CmdDesc[cmd].Routine != Envelope)
				{
					MusicCmd = false;
				}
			}

			if (CmdDesc[cmd].Routine == Channel)
			{
				currentChannel_ = *(BYTE*)(i + 1);
				//MusicCmd = true;
				BYTE c = *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
				sound::channelVisible[currentChannel_] = c;
				MusicContainerStarted = true;

				if (sound::channelVisible[currentChannel_] == true)  channelY -= stepY*channelStep;
			}

			if (CmdDesc[cmd].Routine == Mixer)
			{
				currentChannel_ = MAXCHANNELS-1;
				//MusicCmd = true;
				BYTE c = *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
				sound::channelVisible[currentChannel_] = c;
				MusicContainerStarted = true;
			}

			if ((CmdDesc[cmd].Level == 1) ||(CmdDesc[cmd].Routine==ColorPoint)|| (CmdDesc[cmd].Routine == fxPoint)||
				(MusicContainerStarted == true && 
					sound::channelVisible[currentChannel_] == false && 
					MusicCmd == true && 
					CmdDesc[cmd].Routine != Channel &&
					CmdDesc[cmd].Routine != Mixer
					))
			{

			}
			else
			{
				if (MusicCmd==true)
					{
					float ofs = 0.0;
					if (sound::channelVisible[currentChannel_] == true &&
						CmdDesc[cmd].Routine != Channel&&
						CmdDesc[cmd].Routine != Mixer)
						{
						ofs = .05f;
						}

						dxui::String(tx + ofs, ty, .05f, .05f, c, CmdDesc[cmd].Name);
						
						if (CmdDesc[cmd].Routine == Envelope)
						{
							float toofs = dxui::StringLen(.05f, CmdDesc[cmd].Name)*2.3f;

							BYTE* j = i + CmdDesc[cmd].Size;
							while (*j != 0 && (CmdDesc[*j].Routine == Envelope || CmdDesc[*j].Routine == Point))
							{
								j += CmdDesc[*j].Size;
							}
							int offset = CmdDesc[cmd].ParamOffset[4];
							BYTE v = *(i + offset);

							char* enumText = CmdDesc[*j].ParamName[v];
							char nt[128];
							strcpy(nt, CmdDesc[*j].Name);
							strcat(nt, "::");
							strcat(nt, enumText);

							dxui::String(tx + ofs+toofs, ty, .05f, .05f, c, nt);

							if (*(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode) == 1)
							{
								dxui::String(bPos[1][tcounter].x - dxui::StringLen(.05f, nt)*2.3f, bPos[1][tcounter].y, .05f, .05f, c, nt);
							}
						}


						int pp = 0;
						while (*CmdDesc[cmd].ParamName[pp] != 0)
						{
							if (!strcmp(CmdDesc[cmd].ParamName[pp], "name"))
							{
								BYTE* label = i+CmdDesc[cmd].ParamOffset[pp];
								chname = label;
								dxui::String(tx + ofs + dxui::StringLen(.05f,CmdDesc[cmd].Name)*2.3f, ty, .05f, .05f, c, (char*)label);
							}
							pp++;
						}

						ty -= stepY;

					}
				else
					{
						char* Label = NULL;
						char cname[128];

						for (int n=0;n<MAXPARAM;n++)
						{ 
							if (!strcmp(CmdDesc[cmd].ParamName[n], "name") && Label==NULL)
							{
								Label = (char*)(CmdDesc[cmd].ParamOffset[n]+i);
							
								strcpy(cname, CmdDesc[cmd].Name);
								strcat(cname, " ");
								strcat(cname, Label);
							}
						}

						if (CmdDesc[cmd].Routine == Quad)
						{
							Label = GetShaderName(*(BYTE*)(i+ CmdDesc[cmd].ParamOffset[0]));
							if (Label != NULL)
							{
								strcpy(cname, CmdDesc[cmd].Name);
								strcat(cname, " ");
								strcat(cname, Label);
							}
						}

						if (CmdDesc[cmd].Routine == ShowScene)
						{
							Label = GetResourceName(*(BYTE*)(i + CmdDesc[cmd].ParamOffset[0]),Scene,"name");
							if (Label != NULL)
							{
								strcpy(cname, CmdDesc[cmd].Name);
								strcat(cname, " ");
								strcat(cname, Label);
							}

						}

						if (Label==NULL) dxui::String(tx, ty, .05f, .05f, c, CmdDesc[cmd].Name); else dxui::String(tx, ty, .05f, .05f, c, cname);
					
						//dxui::String(tx, ty, .05f, .05f, c, CmdDesc[cmd].Name);

						ty -= stepY;
						if (CmdDesc[cmd].Routine == ControlCurve) ty -= stepY * 7;

						if (CmdDesc[cmd].Routine == Commands::CreateTexture) ty -= stepY;


						if (CmdDesc[cmd].Routine==Scene&&*(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode) == 0)
						{
							i += CmdDesc[*i].Size;
							//ty -= stepY;
							while (*(i) != 0 &&
								CmdDesc[*(i )].Routine != EndScene &&
								CmdDesc[*(i )].Routine != Scene)
							{
								char *cn = CmdDesc[*i].Name;
								i += CmdDesc[*i].Size;
								tcounter++;
								//ty -= stepY;
							}
							cmd = *i;
							char *cn = CmdDesc[*i].Name;
							int a = 0;
						}

					}
				
			}

			if (CmdDesc[cmd].Routine == EndDraw) ty -= stepY;
			
			if ((sound::channelVisible[currentChannel_] == true && CmdDesc[cmd].Category == CAT_SOUND)|| (CmdDesc[cmd].Category != CAT_SOUND))
			{

				//timeline ghost
				float NoteTextTreshold = 15.5f;
				float ClipTextTreshold = 1.5f;
				if (GetTimeLineStatus(i) == true)
				{
					XMFLOAT4 xyw = GetXYWR(i, 0);
					xyw.x *= timelineScale;
					xyw.x += (float)timelinePos.x;

					if (CmdDesc[cmd].Routine == Clip)
					{
						xyw.y = channelY ;
					}

					if (CmdDesc[cmd].Routine == Envelope)
					{
						envOpen = *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
						if (envOpen) AnyEnvOpen = true;
					}

					if ((CmdDesc[cmd].Routine == Envelope && envOpen && MusicCmd == false) ||
						(CmdDesc[cmd].Routine == Envelope && envOpen && MusicCmd == true && sound::channelVisible[currentChannel_] == true))
					{

						channelY -= envH * 2.1f;
					}

					if (CmdDesc[cmd].Routine != Note)
					{
						if (ui::timelineScale > ClipTextTreshold)
						{
							if ((CmdDesc[cmd].Routine != Point) && (CmdDesc[cmd].Routine != Envelope) && (CmdDesc[cmd].Routine != TimeLoop))
							{
								if (CmdDesc[cmd].Routine == Clip)
								{
									char cname[64];
									strcpy(cname, CmdDesc[cmd].Name);
									strcat(cname, "::");
									strcat(cname, (char*)chname);
									dxui::String(xyw.x, xyw.y, .05f, .05f, c, cname);
								}
								else
								{
									dxui::String(xyw.x, xyw.y, .05f, .05f, c, CmdDesc[cmd].Name);
								}
							}
						}
					}
					else
					{
						xyw.y = channelY-stepY;

						float noteLen = dxui::StringLen(.05f, "C#0");
						if (xyw.z*timelineScale  > noteLen*1.7)
						{
							CreateNoteText(*(i + 1));
							dxui::String(xyw.x, xyw.y, .05f/1.3f, .05f, c, noteText);
						}
					}

					if (CmdDesc[cmd].Routine == Note)
					{
						float noteLen = dxui::StringLen(.05f, "C#0");
						if (xyw.z*timelineScale  > noteLen*1.7)
						{
							
							for (int rep = 1; rep <= cliprep; rep++)
							{
								dxui::String(xyw.x + (clipw*timelineScale)*rep, xyw.y, .05f/1.3f, .05f, c, noteText);
							}
						}
					}
				}

			}

			i += CmdDesc[cmd].Size;
			tcounter++;

		}

		for (int j = 0; j < timedividercount; j += 10)
		{
			char t[12]; char t2[12];
			_itoa(j % 60, t, 10);
			_itoa(j / 60, t2, 10);
			strcat(t2, ":");
			strcat(t2, t);
			if (j % 60 == 0) strcat(t2, "0");
			if (j % 60==0|| timelineScale>.75)

			dxui::String(j*.01f*timelineScale + (float)timelinePos.x-.015f, -0.92f, .04f, .04f, colorScheme.font, t2);
		}


		cursorposF = (-60.f*100.f*timelinePos.x / timelineScale);
		cursorpos = int(cursorposF);
		
		float ch = 0;
		
		if (cursorpos>=0 && cursorpos<maxlines)
		{
			//find clip&note, set height
			i = stack::data;
			while (*i != (BYTE)0)
			{
				BYTE cmd = *i;

				if (CmdDesc[cmd].Routine == Clip)
				{
					signed short start,len,repeat;
					signed short* p1 = (signed short*)(i + CmdDesc[cmd].ParamOffset[0]);
					signed short* p2 = (signed short*)(i + CmdDesc[cmd].ParamOffset[2]);
					signed short* p3 = (signed short*)(i + CmdDesc[cmd].ParamOffset[3]);
					signed short* p4 = (signed short*)(i + CmdDesc[cmd].ParamOffset[6]);
					start= *p1;
					len = *p2; 
					signed short _npm = (*p4)*4;
					float _len = (float)(int(((float)len)* (int)(60.0*60.0/int(_npm))));

					repeat = *p3;
					_len *= repeat+1;
					int _end = (int)(start + _len);

					i += CmdDesc[cmd].Size;
		
				}
				else
				{
					i += CmdDesc[cmd].Size;
				}
			}



		}

		if (cursorM >= 0&&AnyMusicCmd==true)
		{

			int outchannel = MAXCHANNELS - 1;
			float amp = 8.f;
			float ofs = .015f;
			float width = .75f;

			float y = .8f;

			dxui::SetupDrawerBox();
			dxui::Box(width*(.0f - 1.f) - ofs, 1.f/amp+y, width , 2.f / amp, colorScheme.masterwave);
			dxui::Box(width*(.0f + 0.f) + ofs, 1.f / amp+y, width, 2.f / amp, colorScheme.masterwave);

			for (int w = 0; w < 734; w++)
			{
				int _framelen =2* 2* 44100 / 60;
				signed short smp1 = *(short*)(sound::channel[outchannel] + Commands::cursorM * _framelen + w * 2 * 2);
				signed short smp2 = *(short*)(sound::channel[outchannel] + Commands::cursorM * _framelen + (w+1) * 2 * 2);

				dxui::lineVertices[w * 2].Pos.x = width*(w/ 735.0f-1.f)-ofs;
				dxui::lineVertices[w * 2].Pos.y = smp1 / 32767.0f / amp+y;
				dxui::lineVertices[w * 2].Pos.z = 0.0f;

				dxui::lineVertices[w * 2 + 1].Pos.x = width*((w+1) / 735.0f-1)-ofs;
				dxui::lineVertices[w * 2 + 1].Pos.y = smp2 / 32767.0f / amp+y;
				dxui::lineVertices[w * 2 + 1].Pos.z = 0.0f;
			}

			for (int w = 0; w < 734; w++)
			{
				int _framelen = 2 * 2 * 44100 / 60;
				signed short smp1 = *(short*)(sound::channel[outchannel] + Commands::cursorM * _framelen + w * 2 * 2+2);
				signed short smp2 = *(short*)(sound::channel[outchannel] + Commands::cursorM * _framelen + (w + 1) * 2 * 2+2);

				int w2 = w +734 * 2;

				dxui::lineVertices[w2 * 2].Pos.x = width*w / 735.0f+ofs;
				dxui::lineVertices[w2 * 2].Pos.y = smp1 / 32767.0f / amp+y;
				dxui::lineVertices[w2 * 2].Pos.z = 0.0f;

				dxui::lineVertices[w2 * 2 + 1].Pos.x = width*(w + 1) / 735.0f+ofs;
				dxui::lineVertices[w2 * 2 + 1].Pos.y = smp2 / 32767.0f / amp+y;
				dxui::lineVertices[w2 * 2 + 1].Pos.z = 0.0f;
			}

			dxui::lineVBSet();
			dxui::SetupDrawerline();
			dxui::UpdateLineVb(734 * 2 * 2);

			dxui::Line(0.0, 0.f, 1, 1.f, colorScheme.masterwaveline, 734 * 2 * 2);	
		}

		dxui::lineVBSet();
		dxui::SetupDrawerline();

		dxui::UpdateLineEnvelopeVb(envelopeLineCount/2);
		dxui::Line(0.0, 0.f, 1., 1.f, colorScheme.envelopeline, envelopeLineCount/2);

	}
	
	

}